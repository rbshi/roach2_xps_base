#!/bin/bash

# This script is used to generate bof file from the bit file.
# Autor: Runbin Shi
# Date:  21/11/2016

# Run this bash with the output file name, like, ./gen_bof.sh system.bof

./top/XPS_ROACH2_base/mkbof_64 -o ./top/bit_files/$1 -s ./top/XPS_ROACH2_base/core_info.tab -t 3 ./top/XPS_ROACH2_base/implementation/system.bin
