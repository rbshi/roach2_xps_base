//-----------------------------------------------------------------------------
// Title         : Data Bypass Module
// Project       : ROACH2
//-----------------------------------------------------------------------------
// File          : data_bypass.v
// Author        : Runbin Shi
// Created       : 21.11.2016
// Last modified : 21.11.2016
//-----------------------------------------------------------------------------
// Description : This is a simple example module which connects the Matlab & 
//		 XPS Project.
//-----------------------------------------------------------------------------

module data_bypass(
                           input wire         clk,
                           input wire         ce,
                           input wire         rst_n,
                           input wire         en,
                           input wire [127:0] data_in,

                           output reg [127:0] data_out,
                           output reg         data_out_vld
                           );


   always @(posedge clk) begin
      case (en)
        1: begin
           data_out <= data_in;
           data_out_vld <= 1'b1;
        end
        default: begin
           data_out <= 128'b0;
           data_out_vld <= 1'b0;
        end
      endcase // case (en)
   end // always @ (posedge clk)

endmodule // data_bypass


