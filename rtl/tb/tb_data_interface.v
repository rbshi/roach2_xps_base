//-----------------------------------------------------------------------------
// Title         : Testbench for Cross Domain Clk Data Interface
// Project       : ROACH2
//-----------------------------------------------------------------------------
// File          : tb_data_interface.v
// Author        : Runbin Shi
// Created       : 24.11.2016
// Last modified : 24.11.2016
//-----------------------------------------------------------------------------
// Description :
//
//-----------------------------------------------------------------------------

`timescale 1 ns / 1 ns
module tb_data_interface();

   // Input Signal
   reg clk_250;
   reg clk_125;
   reg rst_n;

   reg [127:0] data_in;
   reg    data_in_vld;

   // Output Signal
   wire [127:0] data_out;
   wire         data_out_vld;

   // Wires
   wire [255:0] data_out_freqdown;
   wire         data_out_vld_freqdown;

   integer                          k,l;
   integer                          tmp;

   integer                          in_file, out_file;

   integer                          input_data[0:100];

   initial begin
      $vcdpluson;
   end

   initial begin
      for(l=0;l<100;l=l+1) begin
         input_data[l] = l;
      end
   end


   initial begin
      clk_250 = 0;
      forever #2 clk_250 = ~clk_250;
   end

   initial begin
      #2 clk_125 = 0;
      forever #4 clk_125 = ~clk_125;
   end

   initial begin
      rst_n = 1;
      #4 rst_n = 0;
      #8 rst_n = 1;
   end



   integer t;
   initial begin
      #60;
      for(t=0; t<100; t=t+1) begin
         #4 data_in = input_data[t];
         data_in_vld = 1;
      end
      $finish;
   end


   clk250_to_clk125 u_clk_freqdown(
                                   .clk_250(clk_250),
                                   .clk_125(clk_125),
                                   .rst_n(rst_n),
                                   .data_in(data_in),
                                   .data_in_vld(data_in_vld),
                                   .data_out(data_out_freqdown),
                                   .data_out_vld(data_out_vld_freqdown)
                                   );

   clk125_to_clk250 u_clk_frequp(
                                   .clk_250(clk_250),
                                   .clk_125(clk_125),
                                   .rst_n(rst_n),
                                   .data_in(data_out_freqdown),
                                   .data_in_vld(data_out_vld_freqdown),
                                   .data_out(data_out),
                                   .data_out_vld(data_out_vld)
                                   );


endmodule // tb_interleave_sampling
