# Begin_DVE_Session_Save_Info
# DVE view(Wave.1 ) session
# Saved on Thu Nov 24 22:08:01 2016
# Toplevel windows open: 2
# 	TopLevel.1
# 	TopLevel.2
#   Wave.1: 34 signals
# End_DVE_Session_Save_Info

# DVE version: I-2014.03
# DVE build date: Feb 27 2014 20:56:47


#<Session mode="View" path="/home/rbshi/roach/roach2_xps_base/rtl/vcs_workspace/session.vcdplus.vpd.tcl" type="Debug">

#<Database>

gui_set_time_units 1ns
#</Database>

# DVE View/pane content session: 

# Begin_DVE_Session_Save_Info (Wave.1)
# DVE wave signals session
# Saved on Thu Nov 24 22:08:01 2016
# 34 signals
# End_DVE_Session_Save_Info

# DVE version: I-2014.03
# DVE build date: Feb 27 2014 20:56:47


#Add ncecessay scopes
gui_load_child_values {tb_data_interface}
gui_load_child_values {tb_data_interface.u_clk_freqdown}
gui_load_child_values {tb_data_interface.u_clk_frequp}

gui_set_time_units 1ns

set _wave_session_group_10 Group1
if {[gui_sg_is_group -name "$_wave_session_group_10"]} {
    set _wave_session_group_10 [gui_sg_generate_new_name]
}
set Group1 "$_wave_session_group_10"

gui_sg_addsignal -group "$_wave_session_group_10" { {V1:tb_data_interface.clk_250} {V1:tb_data_interface.clk_125} {V1:tb_data_interface.rst_n} {V1:tb_data_interface.data_in} {V1:tb_data_interface.data_in_vld} {V1:tb_data_interface.data_out} {V1:tb_data_interface.data_out_vld} {V1:tb_data_interface.data_out_freqdown} {V1:tb_data_interface.data_out_vld_freqdown} }

set _wave_session_group_11 freqdown
if {[gui_sg_is_group -name "$_wave_session_group_11"]} {
    set _wave_session_group_11 [gui_sg_generate_new_name]
}
set Group2 "$_wave_session_group_11"

gui_sg_addsignal -group "$_wave_session_group_11" { {V1:tb_data_interface.u_clk_freqdown.toggle_flag_d1} {V1:tb_data_interface.u_clk_freqdown.toggle_flag} {V1:tb_data_interface.u_clk_freqdown.rst_n} {V1:tb_data_interface.u_clk_freqdown.data_out_vld_d1} {V1:tb_data_interface.u_clk_freqdown.data_out_vld_d0} {V1:tb_data_interface.u_clk_freqdown.data_out_vld} {V1:tb_data_interface.u_clk_freqdown.data_out_l128} {V1:tb_data_interface.u_clk_freqdown.data_out_h128} {V1:tb_data_interface.u_clk_freqdown.data_out_d1} {V1:tb_data_interface.u_clk_freqdown.data_out_d0} {V1:tb_data_interface.u_clk_freqdown.data_out} {V1:tb_data_interface.u_clk_freqdown.data_in_vld} {V1:tb_data_interface.u_clk_freqdown.data_in} {V1:tb_data_interface.u_clk_freqdown.clk_250} {V1:tb_data_interface.u_clk_freqdown.clk_125} }

set _wave_session_group_12 frequp
if {[gui_sg_is_group -name "$_wave_session_group_12"]} {
    set _wave_session_group_12 [gui_sg_generate_new_name]
}
set Group3 "$_wave_session_group_12"

gui_sg_addsignal -group "$_wave_session_group_12" { {V1:tb_data_interface.u_clk_frequp.toggle_flag} {V1:tb_data_interface.u_clk_frequp.rst_n} {V1:tb_data_interface.u_clk_frequp.data_out_vld} {V1:tb_data_interface.u_clk_frequp.data_out} {V1:tb_data_interface.u_clk_frequp.data_in_vld_d1} {V1:tb_data_interface.u_clk_frequp.data_in_vld} {V1:tb_data_interface.u_clk_frequp.data_in_d1} {V1:tb_data_interface.u_clk_frequp.data_in} {V1:tb_data_interface.u_clk_frequp.clk_250} {V1:tb_data_interface.u_clk_frequp.clk_125} }
if {![info exists useOldWindow]} { 
	set useOldWindow true
}
if {$useOldWindow && [string first "Wave" [gui_get_current_window -view]]==0} { 
	set Wave.1 [gui_get_current_window -view] 
} else {
	gui_open_window Wave
set Wave.1 [ gui_get_current_window -view ]
}
set groupExD [gui_get_pref_value -category Wave -key exclusiveSG]
gui_set_pref_value -category Wave -key exclusiveSG -value {false}
set origWaveHeight [gui_get_pref_value -category Wave -key waveRowHeight]
gui_list_set_height -id Wave -height 25
set origGroupCreationState [gui_list_create_group_when_add -wave]
gui_list_create_group_when_add -wave -disable
gui_marker_set_ref -id ${Wave.1}  C1
gui_wv_zoom_timerange -id ${Wave.1} 32 159
gui_list_add_group -id ${Wave.1} -after {New Group} [list ${Group1}]
gui_list_add_group -id ${Wave.1} -after {New Group} [list ${Group2}]
gui_list_add_group -id ${Wave.1} -after {New Group} [list ${Group3}]
gui_seek_criteria -id ${Wave.1} {Any Edge}


gui_set_pref_value -category Wave -key exclusiveSG -value $groupExD
gui_list_set_height -id Wave -height $origWaveHeight
if {$origGroupCreationState} {
	gui_list_create_group_when_add -wave -enable
}
if { $groupExD } {
 gui_msg_report -code DVWW028
}
gui_list_set_filter -id ${Wave.1} -list { {Buffer 1} {Input 1} {Others 1} {Linkage 1} {Output 1} {Parameter 1} {All 1} {Aggregate 1} {LibBaseMember 1} {Event 1} {Assertion 1} {Constant 1} {Interface 1} {BaseMembers 1} {Signal 1} {$unit 1} {Inout 1} {Variable 1} }
gui_list_set_filter -id ${Wave.1} -text {*}
gui_list_set_insertion_bar  -id ${Wave.1} -group ${Group3}  -position in

gui_marker_move -id ${Wave.1} {C1} 66
gui_view_scroll -id ${Wave.1} -vertical -set 0
gui_show_grid -id ${Wave.1} -enable false
#</Session>

