# Begin_DVE_Session_Save_Info
# DVE reload session
# Saved on Thu Nov 24 22:08:05 2016
# Designs open: 1
#   V1: /home/rbshi/roach/roach2_xps_base/rtl/vcs_workspace/vcdplus.vpd
# Toplevel windows open: 1
# 	TopLevel.1
#   Source.1: tb_data_interface
#   Group count = 6
#   Group tb signal count = 9
#   Group freq_down signal count = 0
#   Group freq_up signal count = 25
#   Group Group1 signal count = 9
#   Group freqdown signal count = 15
#   Group frequp signal count = 10
# End_DVE_Session_Save_Info

# DVE version: I-2014.03
# DVE build date: Feb 27 2014 20:56:47


#<Session mode="Reload" path="/home/rbshi/roach/roach2_xps_base/rtl/vcs_workspace/DVEfiles/session.tcl" type="Debug">

gui_set_loading_session_type Reload
gui_continuetime_set

# Close design
if { [gui_sim_state -check active] } {
    gui_sim_terminate
}
gui_close_db -all
gui_expr_clear_all
gui_clear_window -type Wave
gui_clear_window -type List

# Application preferences
gui_set_pref_value -key app_default_font -value {Helvetica,10,-1,5,50,0,0,0,0,0}
gui_src_preferences -tabstop 8 -maxbits 24 -windownumber 1
#<WindowLayout>

# DVE top-level session


# Create and position top-level window: TopLevel.1

set TopLevel.1 TopLevel.1

# Docked window settings
set HSPane.1 HSPane.1
set Hier.1 Hier.1
set DLPane.1 DLPane.1
set Data.1 Data.1
set Console.1 Console.1
set DriverLoad.1 DriverLoad.1
gui_sync_global -id ${TopLevel.1} -option true

# MDI window settings
set Source.1 Source.1
gui_update_layout -id ${Source.1} {{show_state maximized} {dock_state undocked} {dock_on_new_line false}}

# End MDI window settings


#</WindowLayout>

#<Database>

# DVE Open design session: 

if { ![gui_is_db_opened -db {/home/rbshi/roach/roach2_xps_base/rtl/vcs_workspace/vcdplus.vpd}] } {
	gui_open_db -design V1 -file /home/rbshi/roach/roach2_xps_base/rtl/vcs_workspace/vcdplus.vpd -nosource
}
gui_set_precision 1ns
gui_set_time_units 1ns
#</Database>

# DVE Global setting session: 


# Global: Bus

# Global: Expressions

# Global: Signal Time Shift

# Global: Signal Compare

# Global: Signal Groups
gui_load_child_values {tb_data_interface}
gui_load_child_values {tb_data_interface.u_clk_freqdown}
gui_load_child_values {tb_data_interface.u_clk_frequp}


set _session_group_10 tb
gui_sg_create "$_session_group_10"
set tb "$_session_group_10"

gui_sg_addsignal -group "$_session_group_10" { tb_data_interface.clk_250 tb_data_interface.clk_125 tb_data_interface.rst_n tb_data_interface.data_in tb_data_interface.data_in_vld tb_data_interface.data_out tb_data_interface.data_out_vld tb_data_interface.data_out_freqdown tb_data_interface.data_out_vld_freqdown }

set _session_group_11 freq_down
gui_sg_create "$_session_group_11"
set freq_down "$_session_group_11"


set _session_group_12 freq_up
gui_sg_create "$_session_group_12"
set freq_up "$_session_group_12"

gui_sg_addsignal -group "$_session_group_12" { tb_data_interface.u_clk_frequp.toggle_flag tb_data_interface.u_clk_frequp.rst_n tb_data_interface.u_clk_frequp.data_out_vld tb_data_interface.u_clk_frequp.data_out tb_data_interface.u_clk_frequp.data_in_vld_d1 tb_data_interface.u_clk_frequp.data_in_vld tb_data_interface.u_clk_frequp.data_in_d1 tb_data_interface.u_clk_frequp.data_in tb_data_interface.u_clk_frequp.clk_250 tb_data_interface.u_clk_frequp.clk_125 tb_data_interface.u_clk_freqdown.toggle_flag_d1 tb_data_interface.u_clk_freqdown.toggle_flag tb_data_interface.u_clk_freqdown.rst_n tb_data_interface.u_clk_freqdown.data_out_vld_d1 tb_data_interface.u_clk_freqdown.data_out_vld_d0 tb_data_interface.u_clk_freqdown.data_out_vld tb_data_interface.u_clk_freqdown.data_out_l128 tb_data_interface.u_clk_freqdown.data_out_h128 tb_data_interface.u_clk_freqdown.data_out_d1 tb_data_interface.u_clk_freqdown.data_out_d0 tb_data_interface.u_clk_freqdown.data_out tb_data_interface.u_clk_freqdown.data_in_vld tb_data_interface.u_clk_freqdown.data_in tb_data_interface.u_clk_freqdown.clk_250 tb_data_interface.u_clk_freqdown.clk_125 }

set _session_group_13 Group1
gui_sg_create "$_session_group_13"
set Group1 "$_session_group_13"

gui_sg_addsignal -group "$_session_group_13" { tb_data_interface.clk_250 tb_data_interface.clk_125 tb_data_interface.rst_n tb_data_interface.data_in tb_data_interface.data_in_vld tb_data_interface.data_out tb_data_interface.data_out_vld tb_data_interface.data_out_freqdown tb_data_interface.data_out_vld_freqdown }

set _session_group_14 freqdown
gui_sg_create "$_session_group_14"
set freqdown "$_session_group_14"

gui_sg_addsignal -group "$_session_group_14" { tb_data_interface.u_clk_freqdown.toggle_flag_d1 tb_data_interface.u_clk_freqdown.toggle_flag tb_data_interface.u_clk_freqdown.rst_n tb_data_interface.u_clk_freqdown.data_out_vld_d1 tb_data_interface.u_clk_freqdown.data_out_vld_d0 tb_data_interface.u_clk_freqdown.data_out_vld tb_data_interface.u_clk_freqdown.data_out_l128 tb_data_interface.u_clk_freqdown.data_out_h128 tb_data_interface.u_clk_freqdown.data_out_d1 tb_data_interface.u_clk_freqdown.data_out_d0 tb_data_interface.u_clk_freqdown.data_out tb_data_interface.u_clk_freqdown.data_in_vld tb_data_interface.u_clk_freqdown.data_in tb_data_interface.u_clk_freqdown.clk_250 tb_data_interface.u_clk_freqdown.clk_125 }

set _session_group_15 frequp
gui_sg_create "$_session_group_15"
set frequp "$_session_group_15"

gui_sg_addsignal -group "$_session_group_15" { tb_data_interface.u_clk_frequp.toggle_flag tb_data_interface.u_clk_frequp.rst_n tb_data_interface.u_clk_frequp.data_out_vld tb_data_interface.u_clk_frequp.data_out tb_data_interface.u_clk_frequp.data_in_vld_d1 tb_data_interface.u_clk_frequp.data_in_vld tb_data_interface.u_clk_frequp.data_in_d1 tb_data_interface.u_clk_frequp.data_in tb_data_interface.u_clk_frequp.clk_250 tb_data_interface.u_clk_frequp.clk_125 }

# Global: Highlighting

# Global: Stack
gui_change_stack_mode -mode list

# Post database loading setting...

# Restore C1 time
gui_set_time -C1_only 66



# Save global setting...

# Wave/List view global setting
gui_list_create_group_when_add -wave -enable
gui_cov_show_value -switch false

# Close all empty TopLevel windows
foreach __top [gui_ekki_get_window_ids -type TopLevel] {
    if { [llength [gui_ekki_get_window_ids -parent $__top]] == 0} {
        gui_close_window -window $__top
    }
}
gui_set_loading_session_type noSession
# DVE View/pane content session: 


# Hier 'Hier.1'
gui_show_window -window ${Hier.1}
gui_list_set_filter -id ${Hier.1} -list { {Package 1} {All 0} {Process 1} {VirtPowSwitch 0} {UnnamedProcess 1} {UDP 0} {Function 1} {Block 1} {OVA Unit 1} {LeafScCell 1} {LeafVlgCell 1} {Interface 1} {LeafVhdCell 1} {$unit 1} {NamedBlock 1} {Task 1} {VlgPackage 1} {ClassDef 1} {VirtIsoCell 0} }
gui_list_set_filter -id ${Hier.1} -text {*} -force
gui_change_design -id ${Hier.1} -design V1
catch {gui_list_expand -id ${Hier.1} tb_data_interface}
catch {gui_list_select -id ${Hier.1} {tb_data_interface.u_clk_frequp}}
gui_view_scroll -id ${Hier.1} -vertical -set 0
gui_view_scroll -id ${Hier.1} -horizontal -set 0

# Data 'Data.1'
gui_list_set_filter -id ${Data.1} -list { {Buffer 1} {Input 1} {Others 1} {Linkage 1} {Output 1} {LowPower 1} {Parameter 1} {All 1} {Aggregate 1} {LibBaseMember 1} {Event 1} {Assertion 1} {Constant 1} {Interface 1} {BaseMembers 1} {Signal 1} {$unit 1} {Inout 1} {Variable 1} }
gui_list_set_filter -id ${Data.1} -text {*}
gui_list_show_data -id ${Data.1} {tb_data_interface.u_clk_frequp}
gui_show_window -window ${Data.1}
catch { gui_list_select -id ${Data.1} {tb_data_interface.u_clk_frequp.toggle_flag tb_data_interface.u_clk_frequp.rst_n tb_data_interface.u_clk_frequp.data_out_vld tb_data_interface.u_clk_frequp.data_out tb_data_interface.u_clk_frequp.data_in_vld_d1 tb_data_interface.u_clk_frequp.data_in_vld tb_data_interface.u_clk_frequp.data_in_d1 tb_data_interface.u_clk_frequp.data_in tb_data_interface.u_clk_frequp.clk_250 tb_data_interface.u_clk_frequp.clk_125 }}
gui_view_scroll -id ${Data.1} -vertical -set 0
gui_view_scroll -id ${Data.1} -horizontal -set 0
gui_view_scroll -id ${Hier.1} -vertical -set 0
gui_view_scroll -id ${Hier.1} -horizontal -set 0

# Source 'Source.1'
gui_src_value_annotate -id ${Source.1} -switch false
gui_set_env TOGGLE::VALUEANNOTATE 0
gui_open_source -id ${Source.1}  -replace -active tb_data_interface /home/rbshi/roach/roach2_xps_base/rtl/vcs_workspace/../tb/tb_data_interface.v
gui_view_scroll -id ${Source.1} -vertical -set 224
gui_src_set_reusable -id ${Source.1}

# DriverLoad 'DriverLoad.1'
# Restore toplevel window zorder
# The toplevel window could be closed if it has no view/pane
if {[gui_exist_window -window ${TopLevel.1}]} {
	gui_set_active_window -window ${TopLevel.1}
	gui_set_active_window -window ${Source.1}
	gui_set_active_window -window ${DLPane.1}
}
#</Session>

