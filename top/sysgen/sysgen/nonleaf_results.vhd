library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/adc/bus0_0"

entity bus0_0_entity_5e787f795e is
  port (
    in1: in std_logic_vector(7 downto 0); 
    in10: in std_logic_vector(7 downto 0); 
    in11: in std_logic_vector(7 downto 0); 
    in12: in std_logic_vector(7 downto 0); 
    in13: in std_logic_vector(7 downto 0); 
    in14: in std_logic_vector(7 downto 0); 
    in15: in std_logic_vector(7 downto 0); 
    in16: in std_logic_vector(7 downto 0); 
    in2: in std_logic_vector(7 downto 0); 
    in3: in std_logic_vector(7 downto 0); 
    in4: in std_logic_vector(7 downto 0); 
    in5: in std_logic_vector(7 downto 0); 
    in6: in std_logic_vector(7 downto 0); 
    in7: in std_logic_vector(7 downto 0); 
    in8: in std_logic_vector(7 downto 0); 
    in9: in std_logic_vector(7 downto 0); 
    bus_out: out std_logic_vector(127 downto 0)
  );
end bus0_0_entity_5e787f795e;

architecture structural of bus0_0_entity_5e787f795e is
  signal concatenate_y_net_x0: std_logic_vector(127 downto 0);
  signal reinterpret10_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret11_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret12_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret13_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret14_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret15_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret16_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret1_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret2_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret3_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret4_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret5_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret6_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret7_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret8_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret9_output_port_net: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x15: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x16: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x17: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x18: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x19: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x20: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x21: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x22: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x23: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x24: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x25: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x26: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x27: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x28: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x29: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x30: std_logic_vector(7 downto 0);

begin
  reinterpret_output_port_net_x15 <= in1;
  reinterpret_output_port_net_x30 <= in10;
  reinterpret_output_port_net_x17 <= in11;
  reinterpret_output_port_net_x18 <= in12;
  reinterpret_output_port_net_x19 <= in13;
  reinterpret_output_port_net_x20 <= in14;
  reinterpret_output_port_net_x21 <= in15;
  reinterpret_output_port_net_x22 <= in16;
  reinterpret_output_port_net_x16 <= in2;
  reinterpret_output_port_net_x23 <= in3;
  reinterpret_output_port_net_x24 <= in4;
  reinterpret_output_port_net_x25 <= in5;
  reinterpret_output_port_net_x26 <= in6;
  reinterpret_output_port_net_x27 <= in7;
  reinterpret_output_port_net_x28 <= in8;
  reinterpret_output_port_net_x29 <= in9;
  bus_out <= concatenate_y_net_x0;

  concatenate: entity work.concat_96b2f1cb93
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0 => reinterpret1_output_port_net,
      in1 => reinterpret2_output_port_net,
      in10 => reinterpret11_output_port_net,
      in11 => reinterpret12_output_port_net,
      in12 => reinterpret13_output_port_net,
      in13 => reinterpret14_output_port_net,
      in14 => reinterpret15_output_port_net,
      in15 => reinterpret16_output_port_net,
      in2 => reinterpret3_output_port_net,
      in3 => reinterpret4_output_port_net,
      in4 => reinterpret5_output_port_net,
      in5 => reinterpret6_output_port_net,
      in6 => reinterpret7_output_port_net,
      in7 => reinterpret8_output_port_net,
      in8 => reinterpret9_output_port_net,
      in9 => reinterpret10_output_port_net,
      y => concatenate_y_net_x0
    );

  reinterpret1: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x15,
      output_port => reinterpret1_output_port_net
    );

  reinterpret10: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x30,
      output_port => reinterpret10_output_port_net
    );

  reinterpret11: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x17,
      output_port => reinterpret11_output_port_net
    );

  reinterpret12: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x18,
      output_port => reinterpret12_output_port_net
    );

  reinterpret13: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x19,
      output_port => reinterpret13_output_port_net
    );

  reinterpret14: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x20,
      output_port => reinterpret14_output_port_net
    );

  reinterpret15: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x21,
      output_port => reinterpret15_output_port_net
    );

  reinterpret16: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x22,
      output_port => reinterpret16_output_port_net
    );

  reinterpret2: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x16,
      output_port => reinterpret2_output_port_net
    );

  reinterpret3: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x23,
      output_port => reinterpret3_output_port_net
    );

  reinterpret4: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x24,
      output_port => reinterpret4_output_port_net
    );

  reinterpret5: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x25,
      output_port => reinterpret5_output_port_net
    );

  reinterpret6: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x26,
      output_port => reinterpret6_output_port_net
    );

  reinterpret7: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x27,
      output_port => reinterpret7_output_port_net
    );

  reinterpret8: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x28,
      output_port => reinterpret8_output_port_net
    );

  reinterpret9: entity work.reinterpret_d51df7ac30
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret_output_port_net_x29,
      output_port => reinterpret9_output_port_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/adc/conv_obbus_1/conv_ob0"

entity conv_ob0_entity_3d0ed41ee2 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    i: in std_logic_vector(7 downto 0); 
    q: out std_logic_vector(7 downto 0)
  );
end conv_ob0_entity_3d0ed41ee2;

architecture structural of conv_ob0_entity_3d0ed41ee2 is
  signal ce_1_sg_x0: std_logic;
  signal clk_1_sg_x0: std_logic;
  signal concat_y_net: std_logic_vector(7 downto 0);
  signal inverter_op_net: std_logic;
  signal reinterpret_output_port_net_x16: std_logic_vector(7 downto 0);
  signal slice1_y_net: std_logic_vector(6 downto 0);
  signal slice_y_net: std_logic;
  signal top_adc_adc_user_data_i0_net_x0: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x0 <= ce_1;
  clk_1_sg_x0 <= clk_1;
  top_adc_adc_user_data_i0_net_x0 <= i;
  q <= reinterpret_output_port_net_x16;

  concat: entity work.concat_83e473517e
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0(0) => inverter_op_net,
      in1 => slice1_y_net,
      y => concat_y_net
    );

  inverter: entity work.inverter_e2b989a05e
    port map (
      ce => ce_1_sg_x0,
      clk => clk_1_sg_x0,
      clr => '0',
      ip(0) => slice_y_net,
      op(0) => inverter_op_net
    );

  reinterpret: entity work.reinterpret_4389dc89bf
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => concat_y_net,
      output_port => reinterpret_output_port_net_x16
    );

  slice: entity work.xlslice
    generic map (
      new_lsb => 7,
      new_msb => 7,
      x_width => 8,
      y_width => 1
    )
    port map (
      x => top_adc_adc_user_data_i0_net_x0,
      y(0) => slice_y_net
    );

  slice1: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 6,
      x_width => 8,
      y_width => 7
    )
    port map (
      x => top_adc_adc_user_data_i0_net_x0,
      y => slice1_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/adc/conv_obbus_1"

entity conv_obbus_1_entity_f0371e6e3e is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    i0: in std_logic_vector(7 downto 0); 
    i1: in std_logic_vector(7 downto 0); 
    i10: in std_logic_vector(7 downto 0); 
    i11: in std_logic_vector(7 downto 0); 
    i12: in std_logic_vector(7 downto 0); 
    i13: in std_logic_vector(7 downto 0); 
    i14: in std_logic_vector(7 downto 0); 
    i15: in std_logic_vector(7 downto 0); 
    i2: in std_logic_vector(7 downto 0); 
    i3: in std_logic_vector(7 downto 0); 
    i4: in std_logic_vector(7 downto 0); 
    i5: in std_logic_vector(7 downto 0); 
    i6: in std_logic_vector(7 downto 0); 
    i7: in std_logic_vector(7 downto 0); 
    i8: in std_logic_vector(7 downto 0); 
    i9: in std_logic_vector(7 downto 0); 
    q0: out std_logic_vector(7 downto 0); 
    q1: out std_logic_vector(7 downto 0); 
    q10: out std_logic_vector(7 downto 0); 
    q11: out std_logic_vector(7 downto 0); 
    q12: out std_logic_vector(7 downto 0); 
    q13: out std_logic_vector(7 downto 0); 
    q14: out std_logic_vector(7 downto 0); 
    q15: out std_logic_vector(7 downto 0); 
    q2: out std_logic_vector(7 downto 0); 
    q3: out std_logic_vector(7 downto 0); 
    q4: out std_logic_vector(7 downto 0); 
    q5: out std_logic_vector(7 downto 0); 
    q6: out std_logic_vector(7 downto 0); 
    q7: out std_logic_vector(7 downto 0); 
    q8: out std_logic_vector(7 downto 0); 
    q9: out std_logic_vector(7 downto 0)
  );
end conv_obbus_1_entity_f0371e6e3e;

architecture structural of conv_obbus_1_entity_f0371e6e3e is
  signal ce_1_sg_x16: std_logic;
  signal clk_1_sg_x16: std_logic;
  signal reinterpret_output_port_net_x32: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x33: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x34: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x35: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x36: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x37: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x38: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x39: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x40: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x41: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x42: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x43: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x44: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x45: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x46: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x47: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i0_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i1_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i2_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i3_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i4_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i5_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i6_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i7_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q0_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q1_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q2_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q3_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q4_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q5_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q6_net_x1: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q7_net_x1: std_logic_vector(7 downto 0);

begin
  ce_1_sg_x16 <= ce_1;
  clk_1_sg_x16 <= clk_1;
  top_adc_adc_user_data_i0_net_x1 <= i0;
  top_adc_adc_user_data_i1_net_x1 <= i1;
  top_adc_adc_user_data_q2_net_x1 <= i10;
  top_adc_adc_user_data_q3_net_x1 <= i11;
  top_adc_adc_user_data_q4_net_x1 <= i12;
  top_adc_adc_user_data_q5_net_x1 <= i13;
  top_adc_adc_user_data_q6_net_x1 <= i14;
  top_adc_adc_user_data_q7_net_x1 <= i15;
  top_adc_adc_user_data_i2_net_x1 <= i2;
  top_adc_adc_user_data_i3_net_x1 <= i3;
  top_adc_adc_user_data_i4_net_x1 <= i4;
  top_adc_adc_user_data_i5_net_x1 <= i5;
  top_adc_adc_user_data_i6_net_x1 <= i6;
  top_adc_adc_user_data_i7_net_x1 <= i7;
  top_adc_adc_user_data_q0_net_x1 <= i8;
  top_adc_adc_user_data_q1_net_x1 <= i9;
  q0 <= reinterpret_output_port_net_x32;
  q1 <= reinterpret_output_port_net_x33;
  q10 <= reinterpret_output_port_net_x34;
  q11 <= reinterpret_output_port_net_x35;
  q12 <= reinterpret_output_port_net_x36;
  q13 <= reinterpret_output_port_net_x37;
  q14 <= reinterpret_output_port_net_x38;
  q15 <= reinterpret_output_port_net_x39;
  q2 <= reinterpret_output_port_net_x40;
  q3 <= reinterpret_output_port_net_x41;
  q4 <= reinterpret_output_port_net_x42;
  q5 <= reinterpret_output_port_net_x43;
  q6 <= reinterpret_output_port_net_x44;
  q7 <= reinterpret_output_port_net_x45;
  q8 <= reinterpret_output_port_net_x46;
  q9 <= reinterpret_output_port_net_x47;

  conv_ob0_3d0ed41ee2: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_i0_net_x1,
      q => reinterpret_output_port_net_x32
    );

  conv_ob10_84f3bb3908: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_q2_net_x1,
      q => reinterpret_output_port_net_x34
    );

  conv_ob11_94a7c50bbd: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_q3_net_x1,
      q => reinterpret_output_port_net_x35
    );

  conv_ob12_465d953af6: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_q4_net_x1,
      q => reinterpret_output_port_net_x36
    );

  conv_ob13_00a9b776cc: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_q5_net_x1,
      q => reinterpret_output_port_net_x37
    );

  conv_ob14_0b049980b8: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_q6_net_x1,
      q => reinterpret_output_port_net_x38
    );

  conv_ob15_1e383ea766: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_q7_net_x1,
      q => reinterpret_output_port_net_x39
    );

  conv_ob1_3bd4728504: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_i1_net_x1,
      q => reinterpret_output_port_net_x33
    );

  conv_ob2_1c3a9f9587: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_i2_net_x1,
      q => reinterpret_output_port_net_x40
    );

  conv_ob3_b2483072bb: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_i3_net_x1,
      q => reinterpret_output_port_net_x41
    );

  conv_ob4_53f7e3dcfe: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_i4_net_x1,
      q => reinterpret_output_port_net_x42
    );

  conv_ob5_8c109ea807: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_i5_net_x1,
      q => reinterpret_output_port_net_x43
    );

  conv_ob6_61a8de1a81: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_i6_net_x1,
      q => reinterpret_output_port_net_x44
    );

  conv_ob7_b113533efd: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_i7_net_x1,
      q => reinterpret_output_port_net_x45
    );

  conv_ob8_40db51fb64: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_q0_net_x1,
      q => reinterpret_output_port_net_x46
    );

  conv_ob9_6b8617e398: entity work.conv_ob0_entity_3d0ed41ee2
    port map (
      ce_1 => ce_1_sg_x16,
      clk_1 => clk_1_sg_x16,
      i => top_adc_adc_user_data_q1_net_x1,
      q => reinterpret_output_port_net_x47
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/adc/enable"

entity enable_entity_fb3258a4ab is
  port (
    top_adc_enable_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end enable_entity_fb3258a4ab;

architecture structural of enable_entity_fb3258a4ab is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_adc_enable_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_adc_enable_user_data_out_net_x0 <= top_adc_enable_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_adc_enable_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/adc/pipeline"

entity pipeline_entity_3f7fa1cc1f is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    d: in std_logic_vector(127 downto 0); 
    q: out std_logic_vector(127 downto 0)
  );
end pipeline_entity_3f7fa1cc1f;

architecture structural of pipeline_entity_3f7fa1cc1f is
  signal ce_1_sg_x17: std_logic;
  signal clk_1_sg_x17: std_logic;
  signal concatenate_y_net_x1: std_logic_vector(127 downto 0);
  signal register0_q_net: std_logic_vector(127 downto 0);
  signal register1_q_net: std_logic_vector(127 downto 0);
  signal register2_q_net_x0: std_logic_vector(127 downto 0);

begin
  ce_1_sg_x17 <= ce_1;
  clk_1_sg_x17 <= clk_1;
  concatenate_y_net_x1 <= d;
  q <= register2_q_net_x0;

  register0: entity work.xlregister
    generic map (
      d_width => 128,
      init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    )
    port map (
      ce => ce_1_sg_x17,
      clk => clk_1_sg_x17,
      d => concatenate_y_net_x1,
      en => "1",
      rst => "0",
      q => register0_q_net
    );

  register1: entity work.xlregister
    generic map (
      d_width => 128,
      init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    )
    port map (
      ce => ce_1_sg_x17,
      clk => clk_1_sg_x17,
      d => register0_q_net,
      en => "1",
      rst => "0",
      q => register1_q_net
    );

  register2: entity work.xlregister
    generic map (
      d_width => 128,
      init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    )
    port map (
      ce => ce_1_sg_x17,
      clk => clk_1_sg_x17,
      d => register1_q_net,
      en => "1",
      rst => "0",
      q => register2_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/adc"

entity adc_entity_1abe01bdeb is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    top_adc_adc_user_data_i0: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i1: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i2: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i3: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i4: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i5: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i6: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i7: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q0: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q1: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q2: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q3: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q4: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q5: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q6: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q7: in std_logic_vector(7 downto 0); 
    top_adc_enable_user_data_out: in std_logic_vector(31 downto 0); 
    data: out std_logic_vector(127 downto 0); 
    we: out std_logic
  );
end adc_entity_1abe01bdeb;

architecture structural of adc_entity_1abe01bdeb is
  signal ce_1_sg_x18: std_logic;
  signal clk_1_sg_x18: std_logic;
  signal concatenate_y_net_x1: std_logic_vector(127 downto 0);
  signal enableslice_y_net: std_logic;
  signal register2_q_net_x1: std_logic_vector(127 downto 0);
  signal register_q_net_x0: std_logic;
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal reinterpret_output_port_net_x32: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x33: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x34: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x35: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x36: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x37: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x38: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x39: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x40: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x41: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x42: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x43: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x44: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x45: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x46: std_logic_vector(7 downto 0);
  signal reinterpret_output_port_net_x47: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i0_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i1_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i2_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i3_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i4_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i5_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i6_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i7_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q0_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q1_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q2_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q3_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q4_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q5_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q6_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q7_net_x2: std_logic_vector(7 downto 0);
  signal top_adc_enable_user_data_out_net_x1: std_logic_vector(31 downto 0);

begin
  ce_1_sg_x18 <= ce_1;
  clk_1_sg_x18 <= clk_1;
  top_adc_adc_user_data_i0_net_x2 <= top_adc_adc_user_data_i0;
  top_adc_adc_user_data_i1_net_x2 <= top_adc_adc_user_data_i1;
  top_adc_adc_user_data_i2_net_x2 <= top_adc_adc_user_data_i2;
  top_adc_adc_user_data_i3_net_x2 <= top_adc_adc_user_data_i3;
  top_adc_adc_user_data_i4_net_x2 <= top_adc_adc_user_data_i4;
  top_adc_adc_user_data_i5_net_x2 <= top_adc_adc_user_data_i5;
  top_adc_adc_user_data_i6_net_x2 <= top_adc_adc_user_data_i6;
  top_adc_adc_user_data_i7_net_x2 <= top_adc_adc_user_data_i7;
  top_adc_adc_user_data_q0_net_x2 <= top_adc_adc_user_data_q0;
  top_adc_adc_user_data_q1_net_x2 <= top_adc_adc_user_data_q1;
  top_adc_adc_user_data_q2_net_x2 <= top_adc_adc_user_data_q2;
  top_adc_adc_user_data_q3_net_x2 <= top_adc_adc_user_data_q3;
  top_adc_adc_user_data_q4_net_x2 <= top_adc_adc_user_data_q4;
  top_adc_adc_user_data_q5_net_x2 <= top_adc_adc_user_data_q5;
  top_adc_adc_user_data_q6_net_x2 <= top_adc_adc_user_data_q6;
  top_adc_adc_user_data_q7_net_x2 <= top_adc_adc_user_data_q7;
  top_adc_enable_user_data_out_net_x1 <= top_adc_enable_user_data_out;
  data <= register2_q_net_x1;
  we <= register_q_net_x0;

  bus0_0_5e787f795e: entity work.bus0_0_entity_5e787f795e
    port map (
      in1 => reinterpret_output_port_net_x32,
      in10 => reinterpret_output_port_net_x47,
      in11 => reinterpret_output_port_net_x34,
      in12 => reinterpret_output_port_net_x35,
      in13 => reinterpret_output_port_net_x36,
      in14 => reinterpret_output_port_net_x37,
      in15 => reinterpret_output_port_net_x38,
      in16 => reinterpret_output_port_net_x39,
      in2 => reinterpret_output_port_net_x33,
      in3 => reinterpret_output_port_net_x40,
      in4 => reinterpret_output_port_net_x41,
      in5 => reinterpret_output_port_net_x42,
      in6 => reinterpret_output_port_net_x43,
      in7 => reinterpret_output_port_net_x44,
      in8 => reinterpret_output_port_net_x45,
      in9 => reinterpret_output_port_net_x46,
      bus_out => concatenate_y_net_x1
    );

  conv_obbus_1_f0371e6e3e: entity work.conv_obbus_1_entity_f0371e6e3e
    port map (
      ce_1 => ce_1_sg_x18,
      clk_1 => clk_1_sg_x18,
      i0 => top_adc_adc_user_data_i0_net_x2,
      i1 => top_adc_adc_user_data_i1_net_x2,
      i10 => top_adc_adc_user_data_q2_net_x2,
      i11 => top_adc_adc_user_data_q3_net_x2,
      i12 => top_adc_adc_user_data_q4_net_x2,
      i13 => top_adc_adc_user_data_q5_net_x2,
      i14 => top_adc_adc_user_data_q6_net_x2,
      i15 => top_adc_adc_user_data_q7_net_x2,
      i2 => top_adc_adc_user_data_i2_net_x2,
      i3 => top_adc_adc_user_data_i3_net_x2,
      i4 => top_adc_adc_user_data_i4_net_x2,
      i5 => top_adc_adc_user_data_i5_net_x2,
      i6 => top_adc_adc_user_data_i6_net_x2,
      i7 => top_adc_adc_user_data_i7_net_x2,
      i8 => top_adc_adc_user_data_q0_net_x2,
      i9 => top_adc_adc_user_data_q1_net_x2,
      q0 => reinterpret_output_port_net_x32,
      q1 => reinterpret_output_port_net_x33,
      q10 => reinterpret_output_port_net_x34,
      q11 => reinterpret_output_port_net_x35,
      q12 => reinterpret_output_port_net_x36,
      q13 => reinterpret_output_port_net_x37,
      q14 => reinterpret_output_port_net_x38,
      q15 => reinterpret_output_port_net_x39,
      q2 => reinterpret_output_port_net_x40,
      q3 => reinterpret_output_port_net_x41,
      q4 => reinterpret_output_port_net_x42,
      q5 => reinterpret_output_port_net_x43,
      q6 => reinterpret_output_port_net_x44,
      q7 => reinterpret_output_port_net_x45,
      q8 => reinterpret_output_port_net_x46,
      q9 => reinterpret_output_port_net_x47
    );

  enable_fb3258a4ab: entity work.enable_entity_fb3258a4ab
    port map (
      top_adc_enable_user_data_out => top_adc_enable_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x0
    );

  enableslice: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 0,
      x_width => 32,
      y_width => 1
    )
    port map (
      x => reint1_output_port_net_x0,
      y(0) => enableslice_y_net
    );

  pipeline_3f7fa1cc1f: entity work.pipeline_entity_3f7fa1cc1f
    port map (
      ce_1 => ce_1_sg_x18,
      clk_1 => clk_1_sg_x18,
      d => concatenate_y_net_x1,
      q => register2_q_net_x1
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x18,
      clk => clk_1_sg_x18,
      d(0) => enableslice_y_net,
      en => "1",
      rst => "0",
      q(0) => register_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/data_bypass_topin"

entity data_bypass_topin_entity_fb713132b6 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    data_in: in std_logic_vector(127 downto 0); 
    en: in std_logic; 
    rst_n: in std_logic; 
    convert1_x0: out std_logic_vector(127 downto 0); 
    convert2_x0: out std_logic; 
    convert3_x0: out std_logic
  );
end data_bypass_topin_entity_fb713132b6;

architecture structural of data_bypass_topin_entity_fb713132b6 is
  signal ce_1_sg_x19: std_logic;
  signal clk_1_sg_x19: std_logic;
  signal convert1_dout_net_x0: std_logic_vector(127 downto 0);
  signal convert2_dout_net_x0: std_logic;
  signal convert3_dout_net_x0: std_logic;
  signal reinterpret1_output_port_net: std_logic_vector(127 downto 0);
  signal reinterpret2_output_port_net: std_logic;
  signal reinterpret3_output_port_net: std_logic;
  signal relational2_op_net_x0: std_logic;
  signal top_data_bypass_topout_data_out_net_x0: std_logic_vector(127 downto 0);
  signal top_data_bypass_topout_data_out_vld_net_x0: std_logic;

begin
  ce_1_sg_x19 <= ce_1;
  clk_1_sg_x19 <= clk_1;
  top_data_bypass_topout_data_out_net_x0 <= data_in;
  top_data_bypass_topout_data_out_vld_net_x0 <= en;
  relational2_op_net_x0 <= rst_n;
  convert1_x0 <= convert1_dout_net_x0;
  convert2_x0 <= convert2_dout_net_x0;
  convert3_x0 <= convert3_dout_net_x0;

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 128,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 128,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x19,
      clk => clk_1_sg_x19,
      clr => '0',
      din => reinterpret1_output_port_net,
      en => "1",
      dout => convert1_dout_net_x0
    );

  convert2: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x19,
      clk => clk_1_sg_x19,
      clr => '0',
      din(0) => reinterpret2_output_port_net,
      en => "1",
      dout(0) => convert2_dout_net_x0
    );

  convert3: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x19,
      clk => clk_1_sg_x19,
      clr => '0',
      din(0) => reinterpret3_output_port_net,
      en => "1",
      dout(0) => convert3_dout_net_x0
    );

  reinterpret1: entity work.reinterpret_28b9ecc6fc
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => top_data_bypass_topout_data_out_net_x0,
      output_port => reinterpret1_output_port_net
    );

  reinterpret2: entity work.reinterpret_81130c7f2d
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port(0) => top_data_bypass_topout_data_out_vld_net_x0,
      output_port(0) => reinterpret2_output_port_net
    );

  reinterpret3: entity work.reinterpret_112d91c147
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port(0) => relational2_op_net_x0,
      output_port(0) => reinterpret3_output_port_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/data_bypass_topout"

entity data_bypass_topout_entity_c49bb33b79 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    data_in: in std_logic_vector(127 downto 0); 
    en: in std_logic; 
    rst_n: in std_logic; 
    convert1_x0: out std_logic_vector(127 downto 0); 
    convert2_x0: out std_logic; 
    convert3_x0: out std_logic
  );
end data_bypass_topout_entity_c49bb33b79;

architecture structural of data_bypass_topout_entity_c49bb33b79 is
  signal ce_1_sg_x20: std_logic;
  signal clk_1_sg_x20: std_logic;
  signal convert1_dout_net_x0: std_logic_vector(127 downto 0);
  signal convert2_dout_net_x0: std_logic;
  signal convert3_dout_net_x0: std_logic;
  signal register2_q_net_x2: std_logic_vector(127 downto 0);
  signal register_q_net_x1: std_logic;
  signal reinterpret1_output_port_net: std_logic_vector(127 downto 0);
  signal reinterpret2_output_port_net: std_logic;
  signal reinterpret3_output_port_net: std_logic;
  signal relational2_op_net_x1: std_logic;

begin
  ce_1_sg_x20 <= ce_1;
  clk_1_sg_x20 <= clk_1;
  register2_q_net_x2 <= data_in;
  register_q_net_x1 <= en;
  relational2_op_net_x1 <= rst_n;
  convert1_x0 <= convert1_dout_net_x0;
  convert2_x0 <= convert2_dout_net_x0;
  convert3_x0 <= convert3_dout_net_x0;

  convert1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 128,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 128,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x20,
      clk => clk_1_sg_x20,
      clr => '0',
      din => reinterpret1_output_port_net,
      en => "1",
      dout => convert1_dout_net_x0
    );

  convert2: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x20,
      clk => clk_1_sg_x20,
      clr => '0',
      din(0) => reinterpret2_output_port_net,
      en => "1",
      dout(0) => convert2_dout_net_x0
    );

  convert3: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x20,
      clk => clk_1_sg_x20,
      clr => '0',
      din(0) => reinterpret3_output_port_net,
      en => "1",
      dout(0) => convert3_dout_net_x0
    );

  reinterpret1: entity work.reinterpret_28b9ecc6fc
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => register2_q_net_x2,
      output_port => reinterpret1_output_port_net
    );

  reinterpret2: entity work.reinterpret_112d91c147
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port(0) => register_q_net_x1,
      output_port(0) => reinterpret2_output_port_net
    );

  reinterpret3: entity work.reinterpret_112d91c147
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port(0) => relational2_op_net_x1,
      output_port(0) => reinterpret3_output_port_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/dest_ip0"

entity dest_ip0_entity_1fb46f7727 is
  port (
    top_ethout_dest_ip0_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end dest_ip0_entity_1fb46f7727;

architecture structural of dest_ip0_entity_1fb46f7727 is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip0_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_ethout_dest_ip0_user_data_out_net_x0 <= top_ethout_dest_ip0_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_ethout_dest_ip0_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/dest_ip1"

entity dest_ip1_entity_6dff2b2825 is
  port (
    top_ethout_dest_ip1_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end dest_ip1_entity_6dff2b2825;

architecture structural of dest_ip1_entity_6dff2b2825 is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip1_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_ethout_dest_ip1_user_data_out_net_x0 <= top_ethout_dest_ip1_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_ethout_dest_ip1_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/dest_ip2"

entity dest_ip2_entity_31359874d3 is
  port (
    top_ethout_dest_ip2_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end dest_ip2_entity_31359874d3;

architecture structural of dest_ip2_entity_31359874d3 is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip2_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_ethout_dest_ip2_user_data_out_net_x0 <= top_ethout_dest_ip2_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_ethout_dest_ip2_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/dest_ip3"

entity dest_ip3_entity_ea27a74b94 is
  port (
    top_ethout_dest_ip3_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end dest_ip3_entity_ea27a74b94;

architecture structural of dest_ip3_entity_ea27a74b94 is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip3_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_ethout_dest_ip3_user_data_out_net_x0 <= top_ethout_dest_ip3_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_ethout_dest_ip3_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/dest_port0"

entity dest_port0_entity_4d2c9f8b1c is
  port (
    top_ethout_dest_port0_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end dest_port0_entity_4d2c9f8b1c;

architecture structural of dest_port0_entity_4d2c9f8b1c is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port0_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_ethout_dest_port0_user_data_out_net_x0 <= top_ethout_dest_port0_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_ethout_dest_port0_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/dest_port1"

entity dest_port1_entity_4cf80b4d04 is
  port (
    top_ethout_dest_port1_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end dest_port1_entity_4cf80b4d04;

architecture structural of dest_port1_entity_4cf80b4d04 is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port1_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_ethout_dest_port1_user_data_out_net_x0 <= top_ethout_dest_port1_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_ethout_dest_port1_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/dest_port2"

entity dest_port2_entity_7358153a1b is
  port (
    top_ethout_dest_port2_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end dest_port2_entity_7358153a1b;

architecture structural of dest_port2_entity_7358153a1b is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port2_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_ethout_dest_port2_user_data_out_net_x0 <= top_ethout_dest_port2_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_ethout_dest_port2_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/dest_port3"

entity dest_port3_entity_d4dab57e9b is
  port (
    top_ethout_dest_port3_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end dest_port3_entity_d4dab57e9b;

architecture structural of dest_port3_entity_d4dab57e9b is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port3_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_ethout_dest_port3_user_data_out_net_x0 <= top_ethout_dest_port3_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_ethout_dest_port3_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/rst"

entity rst_entity_11e77febfc is
  port (
    top_ethout_rst_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end rst_entity_11e77febfc;

architecture structural of rst_entity_11e77febfc is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_ethout_rst_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_ethout_rst_user_data_out_net_x0 <= top_ethout_rst_user_data_out;
  in_reg <= reint1_output_port_net_x0;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_ethout_rst_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x0
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout/ten_Gbe0"

entity ten_gbe0_entity_5705cc797b is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    rst: in std_logic; 
    rx_ack: in std_logic; 
    rx_overrun_ack: in std_logic; 
    tx_data: in std_logic_vector(63 downto 0); 
    tx_dest_ip: in std_logic_vector(31 downto 0); 
    tx_dest_port: in std_logic_vector(15 downto 0); 
    tx_end_of_frame: in std_logic; 
    tx_valid: in std_logic; 
    convert_rst_x0: out std_logic; 
    convert_rx_ack_x0: out std_logic; 
    convert_rx_overrun_ack_x0: out std_logic; 
    convert_tx_data_x0: out std_logic_vector(63 downto 0); 
    convert_tx_dest_ip_x0: out std_logic_vector(31 downto 0); 
    convert_tx_end_of_frame_x0: out std_logic; 
    convert_tx_port_x0: out std_logic_vector(15 downto 0); 
    convert_tx_valid_x0: out std_logic
  );
end ten_gbe0_entity_5705cc797b;

architecture structural of ten_gbe0_entity_5705cc797b is
  signal asdf1_y_net_x0: std_logic_vector(63 downto 0);
  signal asdf5_y_net_x0: std_logic_vector(15 downto 0);
  signal ce_1_sg_x21: std_logic;
  signal clk_1_sg_x21: std_logic;
  signal constant8_op_net_x0: std_logic;
  signal convert_rst_dout_net_x0: std_logic;
  signal convert_rx_ack_dout_net_x0: std_logic;
  signal convert_rx_overrun_ack_dout_net_x0: std_logic;
  signal convert_tx_data_dout_net_x0: std_logic_vector(63 downto 0);
  signal convert_tx_dest_ip_dout_net_x0: std_logic_vector(31 downto 0);
  signal convert_tx_end_of_frame_dout_net_x0: std_logic;
  signal convert_tx_port_dout_net_x0: std_logic_vector(15 downto 0);
  signal convert_tx_valid_dout_net_x0: std_logic;
  signal core_rst_y_net_x0: std_logic;
  signal delay4_q_net_x0: std_logic;
  signal reint1_output_port_net_x1: std_logic_vector(31 downto 0);
  signal switch_eof01_net_x0: std_logic;
  signal switch_valid01_net_x0: std_logic;

begin
  ce_1_sg_x21 <= ce_1;
  clk_1_sg_x21 <= clk_1;
  core_rst_y_net_x0 <= rst;
  constant8_op_net_x0 <= rx_ack;
  delay4_q_net_x0 <= rx_overrun_ack;
  asdf1_y_net_x0 <= tx_data;
  reint1_output_port_net_x1 <= tx_dest_ip;
  asdf5_y_net_x0 <= tx_dest_port;
  switch_eof01_net_x0 <= tx_end_of_frame;
  switch_valid01_net_x0 <= tx_valid;
  convert_rst_x0 <= convert_rst_dout_net_x0;
  convert_rx_ack_x0 <= convert_rx_ack_dout_net_x0;
  convert_rx_overrun_ack_x0 <= convert_rx_overrun_ack_dout_net_x0;
  convert_tx_data_x0 <= convert_tx_data_dout_net_x0;
  convert_tx_dest_ip_x0 <= convert_tx_dest_ip_dout_net_x0;
  convert_tx_end_of_frame_x0 <= convert_tx_end_of_frame_dout_net_x0;
  convert_tx_port_x0 <= convert_tx_port_dout_net_x0;
  convert_tx_valid_x0 <= convert_tx_valid_dout_net_x0;

  convert_rst: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x21,
      clk => clk_1_sg_x21,
      clr => '0',
      din(0) => core_rst_y_net_x0,
      en => "1",
      dout(0) => convert_rst_dout_net_x0
    );

  convert_rx_ack: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => '1',
      clk => '1',
      clr => '0',
      din(0) => constant8_op_net_x0,
      en => "1",
      dout(0) => convert_rx_ack_dout_net_x0
    );

  convert_rx_overrun_ack: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x21,
      clk => clk_1_sg_x21,
      clr => '0',
      din(0) => delay4_q_net_x0,
      en => "1",
      dout(0) => convert_rx_overrun_ack_dout_net_x0
    );

  convert_tx_data: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 64,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 64,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x21,
      clk => clk_1_sg_x21,
      clr => '0',
      din => asdf1_y_net_x0,
      en => "1",
      dout => convert_tx_data_dout_net_x0
    );

  convert_tx_dest_ip: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 32,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 32,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x21,
      clk => clk_1_sg_x21,
      clr => '0',
      din => reint1_output_port_net_x1,
      en => "1",
      dout => convert_tx_dest_ip_dout_net_x0
    );

  convert_tx_end_of_frame: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x21,
      clk => clk_1_sg_x21,
      clr => '0',
      din(0) => switch_eof01_net_x0,
      en => "1",
      dout(0) => convert_tx_end_of_frame_dout_net_x0
    );

  convert_tx_port: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 16,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 16,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x21,
      clk => clk_1_sg_x21,
      clr => '0',
      din => asdf5_y_net_x0,
      en => "1",
      dout => convert_tx_port_dout_net_x0
    );

  convert_tx_valid: entity work.xlconvert
    generic map (
      bool_conversion => 1,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x21,
      clk => clk_1_sg_x21,
      clr => '0',
      din(0) => switch_valid01_net_x0,
      en => "1",
      dout(0) => convert_tx_valid_dout_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/ethout"

entity ethout_entity_ef12fc8111 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    dout0: in std_logic_vector(63 downto 0); 
    dout1: in std_logic_vector(63 downto 0); 
    eof0: in std_logic; 
    eof2: in std_logic; 
    top_ethout_dest_ip0_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_ip1_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_ip2_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_ip3_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_port0_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_port1_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_port2_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_port3_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_rst_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe0_rx_overrun: in std_logic; 
    top_ethout_ten_gbe1_rx_overrun: in std_logic; 
    top_ethout_ten_gbe2_rx_overrun: in std_logic; 
    top_ethout_ten_gbe3_rx_overrun: in std_logic; 
    valid0: in std_logic; 
    valid2: in std_logic; 
    ten_gbe0: out std_logic; 
    ten_gbe0_x0: out std_logic; 
    ten_gbe0_x1: out std_logic; 
    ten_gbe0_x2: out std_logic_vector(63 downto 0); 
    ten_gbe0_x3: out std_logic_vector(31 downto 0); 
    ten_gbe0_x4: out std_logic; 
    ten_gbe0_x5: out std_logic_vector(15 downto 0); 
    ten_gbe0_x6: out std_logic; 
    ten_gbe1: out std_logic; 
    ten_gbe1_x0: out std_logic; 
    ten_gbe1_x1: out std_logic; 
    ten_gbe1_x2: out std_logic_vector(63 downto 0); 
    ten_gbe1_x3: out std_logic_vector(31 downto 0); 
    ten_gbe1_x4: out std_logic; 
    ten_gbe1_x5: out std_logic_vector(15 downto 0); 
    ten_gbe1_x6: out std_logic; 
    ten_gbe2: out std_logic; 
    ten_gbe2_x0: out std_logic; 
    ten_gbe2_x1: out std_logic; 
    ten_gbe2_x2: out std_logic_vector(63 downto 0); 
    ten_gbe2_x3: out std_logic_vector(31 downto 0); 
    ten_gbe2_x4: out std_logic; 
    ten_gbe2_x5: out std_logic_vector(15 downto 0); 
    ten_gbe2_x6: out std_logic; 
    ten_gbe3: out std_logic; 
    ten_gbe3_x0: out std_logic; 
    ten_gbe3_x1: out std_logic; 
    ten_gbe3_x2: out std_logic_vector(63 downto 0); 
    ten_gbe3_x3: out std_logic_vector(31 downto 0); 
    ten_gbe3_x4: out std_logic; 
    ten_gbe3_x5: out std_logic_vector(15 downto 0); 
    ten_gbe3_x6: out std_logic
  );
end ethout_entity_ef12fc8111;

architecture structural of ethout_entity_ef12fc8111 is
  signal asdf1_y_net_x2: std_logic_vector(63 downto 0);
  signal asdf2_y_net_x2: std_logic_vector(63 downto 0);
  signal asdf5_y_net_x0: std_logic_vector(15 downto 0);
  signal asdf6_y_net_x0: std_logic_vector(15 downto 0);
  signal asdf7_y_net_x0: std_logic_vector(15 downto 0);
  signal asdf8_y_net_x0: std_logic_vector(15 downto 0);
  signal ce_1_sg_x25: std_logic;
  signal clk_1_sg_x25: std_logic;
  signal constant10_op_net_x0: std_logic;
  signal constant11_op_net_x0: std_logic;
  signal constant8_op_net_x0: std_logic;
  signal constant9_op_net_x0: std_logic;
  signal convert_rst_dout_net_x4: std_logic;
  signal convert_rst_dout_net_x5: std_logic;
  signal convert_rst_dout_net_x6: std_logic;
  signal convert_rst_dout_net_x7: std_logic;
  signal convert_rx_ack_dout_net_x4: std_logic;
  signal convert_rx_ack_dout_net_x5: std_logic;
  signal convert_rx_ack_dout_net_x6: std_logic;
  signal convert_rx_ack_dout_net_x7: std_logic;
  signal convert_rx_overrun_ack_dout_net_x4: std_logic;
  signal convert_rx_overrun_ack_dout_net_x5: std_logic;
  signal convert_rx_overrun_ack_dout_net_x6: std_logic;
  signal convert_rx_overrun_ack_dout_net_x7: std_logic;
  signal convert_tx_data_dout_net_x4: std_logic_vector(63 downto 0);
  signal convert_tx_data_dout_net_x5: std_logic_vector(63 downto 0);
  signal convert_tx_data_dout_net_x6: std_logic_vector(63 downto 0);
  signal convert_tx_data_dout_net_x7: std_logic_vector(63 downto 0);
  signal convert_tx_dest_ip_dout_net_x4: std_logic_vector(31 downto 0);
  signal convert_tx_dest_ip_dout_net_x5: std_logic_vector(31 downto 0);
  signal convert_tx_dest_ip_dout_net_x6: std_logic_vector(31 downto 0);
  signal convert_tx_dest_ip_dout_net_x7: std_logic_vector(31 downto 0);
  signal convert_tx_end_of_frame_dout_net_x4: std_logic;
  signal convert_tx_end_of_frame_dout_net_x5: std_logic;
  signal convert_tx_end_of_frame_dout_net_x6: std_logic;
  signal convert_tx_end_of_frame_dout_net_x7: std_logic;
  signal convert_tx_port_dout_net_x4: std_logic_vector(15 downto 0);
  signal convert_tx_port_dout_net_x5: std_logic_vector(15 downto 0);
  signal convert_tx_port_dout_net_x6: std_logic_vector(15 downto 0);
  signal convert_tx_port_dout_net_x7: std_logic_vector(15 downto 0);
  signal convert_tx_valid_dout_net_x4: std_logic;
  signal convert_tx_valid_dout_net_x5: std_logic;
  signal convert_tx_valid_dout_net_x6: std_logic;
  signal convert_tx_valid_dout_net_x7: std_logic;
  signal core_rst_y_net_x3: std_logic;
  signal delay4_q_net_x0: std_logic;
  signal delay5_q_net_x0: std_logic;
  signal delay6_q_net_x0: std_logic;
  signal delay7_q_net_x0: std_logic;
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x1: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x2: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x3: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x4: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x5: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x6: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x7: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x8: std_logic_vector(31 downto 0);
  signal switch_eof01_net_x2: std_logic;
  signal switch_eof23_net_x2: std_logic;
  signal switch_valid01_net_x2: std_logic;
  signal switch_valid23_net_x2: std_logic;
  signal top_ethout_dest_ip0_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip1_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip2_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip3_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port0_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port1_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port2_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port3_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal top_ethout_rst_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe0_rx_overrun_net_x0: std_logic;
  signal top_ethout_ten_gbe1_rx_overrun_net_x0: std_logic;
  signal top_ethout_ten_gbe2_rx_overrun_net_x0: std_logic;
  signal top_ethout_ten_gbe3_rx_overrun_net_x0: std_logic;

begin
  ce_1_sg_x25 <= ce_1;
  clk_1_sg_x25 <= clk_1;
  asdf1_y_net_x2 <= dout0;
  asdf2_y_net_x2 <= dout1;
  switch_eof01_net_x2 <= eof0;
  switch_eof23_net_x2 <= eof2;
  top_ethout_dest_ip0_user_data_out_net_x1 <= top_ethout_dest_ip0_user_data_out;
  top_ethout_dest_ip1_user_data_out_net_x1 <= top_ethout_dest_ip1_user_data_out;
  top_ethout_dest_ip2_user_data_out_net_x1 <= top_ethout_dest_ip2_user_data_out;
  top_ethout_dest_ip3_user_data_out_net_x1 <= top_ethout_dest_ip3_user_data_out;
  top_ethout_dest_port0_user_data_out_net_x1 <= top_ethout_dest_port0_user_data_out;
  top_ethout_dest_port1_user_data_out_net_x1 <= top_ethout_dest_port1_user_data_out;
  top_ethout_dest_port2_user_data_out_net_x1 <= top_ethout_dest_port2_user_data_out;
  top_ethout_dest_port3_user_data_out_net_x1 <= top_ethout_dest_port3_user_data_out;
  top_ethout_rst_user_data_out_net_x1 <= top_ethout_rst_user_data_out;
  top_ethout_ten_gbe0_rx_overrun_net_x0 <= top_ethout_ten_gbe0_rx_overrun;
  top_ethout_ten_gbe1_rx_overrun_net_x0 <= top_ethout_ten_gbe1_rx_overrun;
  top_ethout_ten_gbe2_rx_overrun_net_x0 <= top_ethout_ten_gbe2_rx_overrun;
  top_ethout_ten_gbe3_rx_overrun_net_x0 <= top_ethout_ten_gbe3_rx_overrun;
  switch_valid01_net_x2 <= valid0;
  switch_valid23_net_x2 <= valid2;
  ten_gbe0 <= convert_rst_dout_net_x4;
  ten_gbe0_x0 <= convert_rx_ack_dout_net_x4;
  ten_gbe0_x1 <= convert_rx_overrun_ack_dout_net_x4;
  ten_gbe0_x2 <= convert_tx_data_dout_net_x4;
  ten_gbe0_x3 <= convert_tx_dest_ip_dout_net_x4;
  ten_gbe0_x4 <= convert_tx_end_of_frame_dout_net_x4;
  ten_gbe0_x5 <= convert_tx_port_dout_net_x4;
  ten_gbe0_x6 <= convert_tx_valid_dout_net_x4;
  ten_gbe1 <= convert_rst_dout_net_x5;
  ten_gbe1_x0 <= convert_rx_ack_dout_net_x5;
  ten_gbe1_x1 <= convert_rx_overrun_ack_dout_net_x5;
  ten_gbe1_x2 <= convert_tx_data_dout_net_x5;
  ten_gbe1_x3 <= convert_tx_dest_ip_dout_net_x5;
  ten_gbe1_x4 <= convert_tx_end_of_frame_dout_net_x5;
  ten_gbe1_x5 <= convert_tx_port_dout_net_x5;
  ten_gbe1_x6 <= convert_tx_valid_dout_net_x5;
  ten_gbe2 <= convert_rst_dout_net_x6;
  ten_gbe2_x0 <= convert_rx_ack_dout_net_x6;
  ten_gbe2_x1 <= convert_rx_overrun_ack_dout_net_x6;
  ten_gbe2_x2 <= convert_tx_data_dout_net_x6;
  ten_gbe2_x3 <= convert_tx_dest_ip_dout_net_x6;
  ten_gbe2_x4 <= convert_tx_end_of_frame_dout_net_x6;
  ten_gbe2_x5 <= convert_tx_port_dout_net_x6;
  ten_gbe2_x6 <= convert_tx_valid_dout_net_x6;
  ten_gbe3 <= convert_rst_dout_net_x7;
  ten_gbe3_x0 <= convert_rx_ack_dout_net_x7;
  ten_gbe3_x1 <= convert_rx_overrun_ack_dout_net_x7;
  ten_gbe3_x2 <= convert_tx_data_dout_net_x7;
  ten_gbe3_x3 <= convert_tx_dest_ip_dout_net_x7;
  ten_gbe3_x4 <= convert_tx_end_of_frame_dout_net_x7;
  ten_gbe3_x5 <= convert_tx_port_dout_net_x7;
  ten_gbe3_x6 <= convert_tx_valid_dout_net_x7;

  asdf5: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 15,
      x_width => 32,
      y_width => 16
    )
    port map (
      x => reint1_output_port_net_x0,
      y => asdf5_y_net_x0
    );

  asdf6: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 15,
      x_width => 32,
      y_width => 16
    )
    port map (
      x => reint1_output_port_net_x5,
      y => asdf6_y_net_x0
    );

  asdf7: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 15,
      x_width => 32,
      y_width => 16
    )
    port map (
      x => reint1_output_port_net_x6,
      y => asdf7_y_net_x0
    );

  asdf8: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 15,
      x_width => 32,
      y_width => 16
    )
    port map (
      x => reint1_output_port_net_x7,
      y => asdf8_y_net_x0
    );

  constant10: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant10_op_net_x0
    );

  constant11: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant11_op_net_x0
    );

  constant8: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant8_op_net_x0
    );

  constant9: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant9_op_net_x0
    );

  core_rst: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 0,
      x_width => 32,
      y_width => 1
    )
    port map (
      x => reint1_output_port_net_x8,
      y(0) => core_rst_y_net_x3
    );

  delay4: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x25,
      clk => clk_1_sg_x25,
      d(0) => top_ethout_ten_gbe0_rx_overrun_net_x0,
      en => '1',
      rst => '1',
      q(0) => delay4_q_net_x0
    );

  delay5: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x25,
      clk => clk_1_sg_x25,
      d(0) => top_ethout_ten_gbe1_rx_overrun_net_x0,
      en => '1',
      rst => '1',
      q(0) => delay5_q_net_x0
    );

  delay6: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x25,
      clk => clk_1_sg_x25,
      d(0) => top_ethout_ten_gbe2_rx_overrun_net_x0,
      en => '1',
      rst => '1',
      q(0) => delay6_q_net_x0
    );

  delay7: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x25,
      clk => clk_1_sg_x25,
      d(0) => top_ethout_ten_gbe3_rx_overrun_net_x0,
      en => '1',
      rst => '1',
      q(0) => delay7_q_net_x0
    );

  dest_ip0_1fb46f7727: entity work.dest_ip0_entity_1fb46f7727
    port map (
      top_ethout_dest_ip0_user_data_out => top_ethout_dest_ip0_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x1
    );

  dest_ip1_6dff2b2825: entity work.dest_ip1_entity_6dff2b2825
    port map (
      top_ethout_dest_ip1_user_data_out => top_ethout_dest_ip1_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x2
    );

  dest_ip2_31359874d3: entity work.dest_ip2_entity_31359874d3
    port map (
      top_ethout_dest_ip2_user_data_out => top_ethout_dest_ip2_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x3
    );

  dest_ip3_ea27a74b94: entity work.dest_ip3_entity_ea27a74b94
    port map (
      top_ethout_dest_ip3_user_data_out => top_ethout_dest_ip3_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x4
    );

  dest_port0_4d2c9f8b1c: entity work.dest_port0_entity_4d2c9f8b1c
    port map (
      top_ethout_dest_port0_user_data_out => top_ethout_dest_port0_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x0
    );

  dest_port1_4cf80b4d04: entity work.dest_port1_entity_4cf80b4d04
    port map (
      top_ethout_dest_port1_user_data_out => top_ethout_dest_port1_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x5
    );

  dest_port2_7358153a1b: entity work.dest_port2_entity_7358153a1b
    port map (
      top_ethout_dest_port2_user_data_out => top_ethout_dest_port2_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x6
    );

  dest_port3_d4dab57e9b: entity work.dest_port3_entity_d4dab57e9b
    port map (
      top_ethout_dest_port3_user_data_out => top_ethout_dest_port3_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x7
    );

  rst_11e77febfc: entity work.rst_entity_11e77febfc
    port map (
      top_ethout_rst_user_data_out => top_ethout_rst_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x8
    );

  ten_gbe0_5705cc797b: entity work.ten_gbe0_entity_5705cc797b
    port map (
      ce_1 => ce_1_sg_x25,
      clk_1 => clk_1_sg_x25,
      rst => core_rst_y_net_x3,
      rx_ack => constant8_op_net_x0,
      rx_overrun_ack => delay4_q_net_x0,
      tx_data => asdf1_y_net_x2,
      tx_dest_ip => reint1_output_port_net_x1,
      tx_dest_port => asdf5_y_net_x0,
      tx_end_of_frame => switch_eof01_net_x2,
      tx_valid => switch_valid01_net_x2,
      convert_rst_x0 => convert_rst_dout_net_x4,
      convert_rx_ack_x0 => convert_rx_ack_dout_net_x4,
      convert_rx_overrun_ack_x0 => convert_rx_overrun_ack_dout_net_x4,
      convert_tx_data_x0 => convert_tx_data_dout_net_x4,
      convert_tx_dest_ip_x0 => convert_tx_dest_ip_dout_net_x4,
      convert_tx_end_of_frame_x0 => convert_tx_end_of_frame_dout_net_x4,
      convert_tx_port_x0 => convert_tx_port_dout_net_x4,
      convert_tx_valid_x0 => convert_tx_valid_dout_net_x4
    );

  ten_gbe1_01466fa736: entity work.ten_gbe0_entity_5705cc797b
    port map (
      ce_1 => ce_1_sg_x25,
      clk_1 => clk_1_sg_x25,
      rst => core_rst_y_net_x3,
      rx_ack => constant9_op_net_x0,
      rx_overrun_ack => delay5_q_net_x0,
      tx_data => asdf2_y_net_x2,
      tx_dest_ip => reint1_output_port_net_x2,
      tx_dest_port => asdf6_y_net_x0,
      tx_end_of_frame => switch_eof01_net_x2,
      tx_valid => switch_valid01_net_x2,
      convert_rst_x0 => convert_rst_dout_net_x5,
      convert_rx_ack_x0 => convert_rx_ack_dout_net_x5,
      convert_rx_overrun_ack_x0 => convert_rx_overrun_ack_dout_net_x5,
      convert_tx_data_x0 => convert_tx_data_dout_net_x5,
      convert_tx_dest_ip_x0 => convert_tx_dest_ip_dout_net_x5,
      convert_tx_end_of_frame_x0 => convert_tx_end_of_frame_dout_net_x5,
      convert_tx_port_x0 => convert_tx_port_dout_net_x5,
      convert_tx_valid_x0 => convert_tx_valid_dout_net_x5
    );

  ten_gbe2_90bdfc98af: entity work.ten_gbe0_entity_5705cc797b
    port map (
      ce_1 => ce_1_sg_x25,
      clk_1 => clk_1_sg_x25,
      rst => core_rst_y_net_x3,
      rx_ack => constant10_op_net_x0,
      rx_overrun_ack => delay6_q_net_x0,
      tx_data => asdf1_y_net_x2,
      tx_dest_ip => reint1_output_port_net_x3,
      tx_dest_port => asdf7_y_net_x0,
      tx_end_of_frame => switch_eof23_net_x2,
      tx_valid => switch_valid23_net_x2,
      convert_rst_x0 => convert_rst_dout_net_x6,
      convert_rx_ack_x0 => convert_rx_ack_dout_net_x6,
      convert_rx_overrun_ack_x0 => convert_rx_overrun_ack_dout_net_x6,
      convert_tx_data_x0 => convert_tx_data_dout_net_x6,
      convert_tx_dest_ip_x0 => convert_tx_dest_ip_dout_net_x6,
      convert_tx_end_of_frame_x0 => convert_tx_end_of_frame_dout_net_x6,
      convert_tx_port_x0 => convert_tx_port_dout_net_x6,
      convert_tx_valid_x0 => convert_tx_valid_dout_net_x6
    );

  ten_gbe3_35d1d43740: entity work.ten_gbe0_entity_5705cc797b
    port map (
      ce_1 => ce_1_sg_x25,
      clk_1 => clk_1_sg_x25,
      rst => core_rst_y_net_x3,
      rx_ack => constant11_op_net_x0,
      rx_overrun_ack => delay7_q_net_x0,
      tx_data => asdf2_y_net_x2,
      tx_dest_ip => reint1_output_port_net_x4,
      tx_dest_port => asdf8_y_net_x0,
      tx_end_of_frame => switch_eof23_net_x2,
      tx_valid => switch_valid23_net_x2,
      convert_rst_x0 => convert_rst_dout_net_x7,
      convert_rx_ack_x0 => convert_rx_ack_dout_net_x7,
      convert_rx_overrun_ack_x0 => convert_rx_overrun_ack_dout_net_x7,
      convert_tx_data_x0 => convert_tx_data_dout_net_x7,
      convert_tx_dest_ip_x0 => convert_tx_dest_ip_dout_net_x7,
      convert_tx_end_of_frame_x0 => convert_tx_end_of_frame_dout_net_x7,
      convert_tx_port_x0 => convert_tx_port_dout_net_x7,
      convert_tx_valid_x0 => convert_tx_valid_dout_net_x7
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/reset"

entity reset_entity_7f945ff177 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    reset: out std_logic
  );
end reset_entity_7f945ff177;

architecture structural of reset_entity_7f945ff177 is
  signal ce_1_sg_x26: std_logic;
  signal clk_1_sg_x26: std_logic;
  signal constant1_op_net: std_logic_vector(1 downto 0);
  signal constant6_op_net: std_logic_vector(1 downto 0);
  signal counter1_op_net: std_logic_vector(1 downto 0);
  signal relational1_op_net: std_logic;
  signal relational2_op_net_x2: std_logic;

begin
  ce_1_sg_x26 <= ce_1;
  clk_1_sg_x26 <= clk_1;
  reset <= relational2_op_net_x2;

  constant1: entity work.constant_e8ddc079e9
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant1_op_net
    );

  constant6: entity work.constant_a7e2bb9e12
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant6_op_net
    );

  counter1: entity work.xlcounter_free_top
    generic map (
      core_name0 => "cntr_11_0_7cba3b1cb5ad2735",
      op_arith => xlUnsigned,
      op_width => 2
    )
    port map (
      ce => ce_1_sg_x26,
      clk => clk_1_sg_x26,
      clr => '0',
      en(0) => relational1_op_net,
      rst => "0",
      op => counter1_op_net
    );

  relational1: entity work.relational_e6bf356533
    port map (
      a => counter1_op_net,
      b => constant1_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational1_op_net
    );

  relational2: entity work.relational_9b3c9652f3
    port map (
      a => counter1_op_net,
      b => constant6_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational2_op_net_x2
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/pipeline2"

entity pipeline2_entity_bd8be06449 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    d: in std_logic; 
    q: out std_logic
  );
end pipeline2_entity_bd8be06449;

architecture structural of pipeline2_entity_bd8be06449 is
  signal ce_1_sg_x27: std_logic;
  signal clk_1_sg_x27: std_logic;
  signal mux_y_net_x0: std_logic;
  signal register0_q_net: std_logic;
  signal register1_q_net: std_logic;
  signal register2_q_net_x0: std_logic;

begin
  ce_1_sg_x27 <= ce_1;
  clk_1_sg_x27 <= clk_1;
  mux_y_net_x0 <= d;
  q <= register2_q_net_x0;

  register0: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x27,
      clk => clk_1_sg_x27,
      d(0) => mux_y_net_x0,
      en => "1",
      rst => "0",
      q(0) => register0_q_net
    );

  register1: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x27,
      clk => clk_1_sg_x27,
      d(0) => register0_q_net,
      en => "1",
      rst => "0",
      q(0) => register1_q_net
    );

  register2: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x27,
      clk => clk_1_sg_x27,
      d(0) => register1_q_net,
      en => "1",
      rst => "0",
      q(0) => register2_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/add_gen/edge_detect"

entity edge_detect_entity_1bf20d21a2 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in_x0: in std_logic; 
    out_x0: out std_logic
  );
end edge_detect_entity_1bf20d21a2;

architecture structural of edge_detect_entity_1bf20d21a2 is
  signal ce_1_sg_x29: std_logic;
  signal clk_1_sg_x29: std_logic;
  signal delay_q_net: std_logic;
  signal edge_op_y_net_x0: std_logic;
  signal inverter_op_net: std_logic;
  signal slice3_y_net_x0: std_logic;

begin
  ce_1_sg_x29 <= ce_1;
  clk_1_sg_x29 <= clk_1;
  slice3_y_net_x0 <= in_x0;
  out_x0 <= edge_op_y_net_x0;

  delay: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x29,
      clk => clk_1_sg_x29,
      d(0) => slice3_y_net_x0,
      en => '1',
      rst => '1',
      q(0) => delay_q_net
    );

  edge_op: entity work.logical_f6397bdee1
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => inverter_op_net,
      d1(0) => delay_q_net,
      y(0) => edge_op_y_net_x0
    );

  inverter: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_1_sg_x29,
      clk => clk_1_sg_x29,
      clr => '0',
      ip(0) => slice3_y_net_x0,
      op(0) => inverter_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/add_gen"

entity add_gen_entity_9cb67b3920 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    cont: in std_logic; 
    din: in std_logic_vector(127 downto 0); 
    go: in std_logic; 
    init: in std_logic; 
    we: in std_logic; 
    add: out std_logic_vector(9 downto 0); 
    dout: out std_logic_vector(127 downto 0); 
    status: out std_logic_vector(31 downto 0); 
    we_o: out std_logic
  );
end add_gen_entity_9cb67b3920;

architecture structural of add_gen_entity_9cb67b3920 is
  signal add_gen_op_net: std_logic_vector(14 downto 0);
  signal ce_1_sg_x30: std_logic;
  signal clk_1_sg_x30: std_logic;
  signal concat_y_net_x0: std_logic_vector(31 downto 0);
  signal convert_dout_net: std_logic_vector(16 downto 0);
  signal data_choice_y_net_x0: std_logic_vector(127 downto 0);
  signal delay1_q_net: std_logic_vector(13 downto 0);
  signal delay3_q_net: std_logic;
  signal delay4_q_net: std_logic;
  signal delay6_q_net_x0: std_logic_vector(127 downto 0);
  signal delay_q_net: std_logic;
  signal edge_op_y_net_x0: std_logic;
  signal edge_op_y_net_x1: std_logic;
  signal inverter1_op_net: std_logic;
  signal inverter_op_net: std_logic;
  signal logical1_y_net: std_logic;
  signal logical4_y_net: std_logic;
  signal logical6_y_net_x0: std_logic;
  signal never_op_net_x0: std_logic;
  signal register5_q_net: std_logic;
  signal register6_q_net_x0: std_logic;
  signal shift_op_net: std_logic_vector(16 downto 0);
  signal slice1_y_net: std_logic_vector(13 downto 0);
  signal slice2_y_net_x0: std_logic_vector(9 downto 0);
  signal slice3_y_net_x0: std_logic;
  signal we_choice_y_net_x0: std_logic;

begin
  ce_1_sg_x30 <= ce_1;
  clk_1_sg_x30 <= clk_1;
  never_op_net_x0 <= cont;
  data_choice_y_net_x0 <= din;
  register6_q_net_x0 <= go;
  edge_op_y_net_x1 <= init;
  we_choice_y_net_x0 <= we;
  add <= slice2_y_net_x0;
  dout <= delay6_q_net_x0;
  status <= concat_y_net_x0;
  we_o <= logical6_y_net_x0;

  add_gen: entity work.xlcounter_free_top
    generic map (
      core_name0 => "cntr_11_0_e77ed84c083e84d1",
      op_arith => xlUnsigned,
      op_width => 15
    )
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      en(0) => logical6_y_net_x0,
      rst(0) => delay4_q_net,
      op => add_gen_op_net
    );

  concat: entity work.concat_350c391ba7
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0 => shift_op_net,
      in1(0) => inverter_op_net,
      in2 => delay1_q_net,
      y => concat_y_net_x0
    );

  convert: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 17,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      din(0) => register5_q_net,
      en => "1",
      dout => convert_dout_net
    );

  delay: entity work.delay_9f02caa990
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      d(0) => delay4_q_net,
      q(0) => delay_q_net
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 14
    )
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      d => slice1_y_net,
      en => '1',
      rst => '1',
      q => delay1_q_net
    );

  delay3: entity work.delay_9f02caa990
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      d(0) => never_op_net_x0,
      q(0) => delay3_q_net
    );

  delay4: entity work.delay_9f02caa990
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      d(0) => edge_op_y_net_x1,
      q(0) => delay4_q_net
    );

  delay6: entity work.delay_ee0f706095
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      d => data_choice_y_net_x0,
      q => delay6_q_net_x0
    );

  edge_detect_1bf20d21a2: entity work.edge_detect_entity_1bf20d21a2
    port map (
      ce_1 => ce_1_sg_x30,
      clk_1 => clk_1_sg_x30,
      in_x0 => slice3_y_net_x0,
      out_x0 => edge_op_y_net_x0
    );

  inverter: entity work.inverter_6844eee868
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      ip(0) => register5_q_net,
      op(0) => inverter_op_net
    );

  inverter1: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      ip(0) => edge_op_y_net_x0,
      op(0) => inverter1_op_net
    );

  logical1: entity work.logical_799f62af22
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      d0(0) => we_choice_y_net_x0,
      d1(0) => register6_q_net_x0,
      y(0) => logical1_y_net
    );

  logical4: entity work.logical_aacf6e1b0e
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => delay3_q_net,
      d1(0) => inverter1_op_net,
      y(0) => logical4_y_net
    );

  logical6: entity work.logical_954ee29728
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => register5_q_net,
      d1(0) => logical4_y_net,
      d2(0) => logical1_y_net,
      y(0) => logical6_y_net_x0
    );

  register5: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"1"
    )
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      d(0) => delay3_q_net,
      en(0) => edge_op_y_net_x0,
      rst(0) => delay_q_net,
      q(0) => register5_q_net
    );

  shift: entity work.shift_df05fba441
    port map (
      ce => ce_1_sg_x30,
      clk => clk_1_sg_x30,
      clr => '0',
      ip => convert_dout_net,
      op => shift_op_net
    );

  slice1: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 13,
      x_width => 15,
      y_width => 14
    )
    port map (
      x => add_gen_op_net,
      y => slice1_y_net
    );

  slice2: entity work.xlslice
    generic map (
      new_lsb => 4,
      new_msb => 13,
      x_width => 15,
      y_width => 10
    )
    port map (
      x => add_gen_op_net,
      y => slice2_y_net_x0
    );

  slice3: entity work.xlslice
    generic map (
      new_lsb => 14,
      new_msb => 14,
      x_width => 15,
      y_width => 1
    )
    port map (
      x => add_gen_op_net,
      y(0) => slice3_y_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/basic_ctrl/dram_munge"

entity dram_munge_entity_f22062c5c2 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    din: in std_logic_vector(127 downto 0); 
    init: in std_logic; 
    we: in std_logic; 
    dout: out std_logic_vector(127 downto 0); 
    we_o: out std_logic
  );
end dram_munge_entity_f22062c5c2;

architecture structural of dram_munge_entity_f22062c5c2 is
  signal cast_dout_net_x0: std_logic_vector(127 downto 0);
  signal ce_1_sg_x31: std_logic;
  signal clk_1_sg_x31: std_logic;
  signal con0_op_net: std_logic_vector(1 downto 0);
  signal con1_op_net: std_logic_vector(1 downto 0);
  signal con2_op_net: std_logic_vector(1 downto 0);
  signal con3_op_net: std_logic_vector(1 downto 0);
  signal concat1_y_net: std_logic_vector(271 downto 0);
  signal concat_y_net: std_logic_vector(271 downto 0);
  signal constant_op_net: std_logic_vector(7 downto 0);
  signal data_choice_y_net_x1: std_logic_vector(127 downto 0);
  signal delay1_q_net: std_logic;
  signal delay_q_net: std_logic;
  signal dout_count_op_net: std_logic;
  signal dram_op_net: std_logic;
  signal edge_op_y_net_x2: std_logic;
  signal input_count_op_net: std_logic_vector(1 downto 0);
  signal logical1_y_net: std_logic;
  signal logical_y_net: std_logic;
  signal mux1_y_net_x0: std_logic_vector(271 downto 0);
  signal mux1_y_net_x1: std_logic;
  signal register1_q_net: std_logic_vector(127 downto 0);
  signal register2_q_net: std_logic_vector(127 downto 0);
  signal register3_q_net: std_logic_vector(127 downto 0);
  signal register_q_net: std_logic_vector(127 downto 0);
  signal relational1_op_net: std_logic;
  signal relational2_op_net: std_logic;
  signal relational3_op_net: std_logic;
  signal relational_op_net: std_logic;
  signal we_choice_y_net_x1: std_logic;

begin
  ce_1_sg_x31 <= ce_1;
  clk_1_sg_x31 <= clk_1;
  cast_dout_net_x0 <= din;
  edge_op_y_net_x2 <= init;
  mux1_y_net_x1 <= we;
  dout <= data_choice_y_net_x1;
  we_o <= we_choice_y_net_x1;

  con0: entity work.constant_cda50df78a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => con0_op_net
    );

  con1: entity work.constant_a7e2bb9e12
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => con1_op_net
    );

  con2: entity work.constant_e8ddc079e9
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => con2_op_net
    );

  con3: entity work.constant_3a9a3daeb9
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => con3_op_net
    );

  concat: entity work.concat_fd1ce36c4a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0 => constant_op_net,
      in1 => register_q_net,
      in2 => constant_op_net,
      in3 => register1_q_net,
      y => concat_y_net
    );

  concat1: entity work.concat_fd1ce36c4a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0 => constant_op_net,
      in1 => register2_q_net,
      in2 => constant_op_net,
      in3 => register3_q_net,
      y => concat1_y_net
    );

  constant_x0: entity work.constant_91ef1678ca
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant_op_net
    );

  data_choice: entity work.mux_5441ad2d93
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => cast_dout_net_x0,
      d1 => mux1_y_net_x0,
      sel(0) => dram_op_net,
      y => data_choice_y_net_x1
    );

  delay: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      d(0) => logical_y_net,
      en => '1',
      rst => '1',
      q(0) => delay_q_net
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      d(0) => logical1_y_net,
      en => '1',
      rst => '1',
      q(0) => delay1_q_net
    );

  dout_count: entity work.counter_caa2b01eef
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      clr => '0',
      en(0) => logical1_y_net,
      rst(0) => edge_op_y_net_x2,
      op(0) => dout_count_op_net
    );

  dram: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => dram_op_net
    );

  input_count: entity work.xlcounter_free_top
    generic map (
      core_name0 => "cntr_11_0_7cba3b1cb5ad2735",
      op_arith => xlUnsigned,
      op_width => 2
    )
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      clr => '0',
      en(0) => mux1_y_net_x1,
      rst(0) => edge_op_y_net_x2,
      op => input_count_op_net
    );

  logical: entity work.logical_799f62af22
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      clr => '0',
      d0(0) => relational3_op_net,
      d1(0) => mux1_y_net_x1,
      y(0) => logical_y_net
    );

  logical1: entity work.logical_aacf6e1b0e
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => logical_y_net,
      d1(0) => delay_q_net,
      y(0) => logical1_y_net
    );

  mux1: entity work.mux_ddf27bda35
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      clr => '0',
      d0 => concat_y_net,
      d1 => concat1_y_net,
      sel(0) => dout_count_op_net,
      y => mux1_y_net_x0
    );

  register1: entity work.xlregister
    generic map (
      d_width => 128,
      init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    )
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      d => cast_dout_net_x0,
      en(0) => relational1_op_net,
      rst => "0",
      q => register1_q_net
    );

  register2: entity work.xlregister
    generic map (
      d_width => 128,
      init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    )
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      d => cast_dout_net_x0,
      en(0) => relational2_op_net,
      rst => "0",
      q => register2_q_net
    );

  register3: entity work.xlregister
    generic map (
      d_width => 128,
      init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    )
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      d => cast_dout_net_x0,
      en(0) => relational3_op_net,
      rst => "0",
      q => register3_q_net
    );

  register_x0: entity work.xlregister
    generic map (
      d_width => 128,
      init_value => b"00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
    )
    port map (
      ce => ce_1_sg_x31,
      clk => clk_1_sg_x31,
      d => cast_dout_net_x0,
      en(0) => relational_op_net,
      rst => "0",
      q => register_q_net
    );

  relational: entity work.relational_5f1eb17108
    port map (
      a => input_count_op_net,
      b => con0_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational_op_net
    );

  relational1: entity work.relational_5f1eb17108
    port map (
      a => input_count_op_net,
      b => con1_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational1_op_net
    );

  relational2: entity work.relational_5f1eb17108
    port map (
      a => input_count_op_net,
      b => con2_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational2_op_net
    );

  relational3: entity work.relational_5f1eb17108
    port map (
      a => input_count_op_net,
      b => con3_op_net,
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => relational3_op_net
    );

  we_choice: entity work.mux_d99e59b6d4
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => mux1_y_net_x1,
      d1(0) => delay1_q_net,
      sel(0) => dram_op_net,
      y(0) => we_choice_y_net_x1
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/basic_ctrl/edge_detect"

entity edge_detect_entity_c8be931955 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in_x0: in std_logic; 
    out_x0: out std_logic
  );
end edge_detect_entity_c8be931955;

architecture structural of edge_detect_entity_c8be931955 is
  signal ce_1_sg_x32: std_logic;
  signal clk_1_sg_x32: std_logic;
  signal delay1_q_net_x0: std_logic;
  signal delay_q_net: std_logic;
  signal edge_op_y_net_x3: std_logic;
  signal inverter_op_net: std_logic;

begin
  ce_1_sg_x32 <= ce_1;
  clk_1_sg_x32 <= clk_1;
  delay1_q_net_x0 <= in_x0;
  out_x0 <= edge_op_y_net_x3;

  delay: entity work.delay_9f02caa990
    port map (
      ce => ce_1_sg_x32,
      clk => clk_1_sg_x32,
      clr => '0',
      d(0) => delay1_q_net_x0,
      q(0) => delay_q_net
    );

  edge_op: entity work.logical_dfe2dded7f
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => inverter_op_net,
      d1(0) => delay_q_net,
      y(0) => edge_op_y_net_x3
    );

  inverter: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_1_sg_x32,
      clk => clk_1_sg_x32,
      clr => '0',
      ip(0) => delay1_q_net_x0,
      op(0) => inverter_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/basic_ctrl"

entity basic_ctrl_entity_deb8f19a04 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    ctrl: in std_logic_vector(31 downto 0); 
    din: in std_logic_vector(127 downto 0); 
    trig: in std_logic; 
    we: in std_logic; 
    dout: out std_logic_vector(127 downto 0); 
    go: out std_logic; 
    init: out std_logic; 
    we_o: out std_logic
  );
end basic_ctrl_entity_deb8f19a04;

architecture structural of basic_ctrl_entity_deb8f19a04 is
  signal cast_dout_net_x1: std_logic_vector(127 downto 0);
  signal ce_1_sg_x33: std_logic;
  signal clk_1_sg_x33: std_logic;
  signal constant1_op_net: std_logic;
  signal constant2_op_net: std_logic;
  signal constant_op_net_x0: std_logic;
  signal constant_op_net_x1: std_logic;
  signal data_choice_y_net_x2: std_logic_vector(127 downto 0);
  signal delay1_q_net_x0: std_logic;
  signal delay2_q_net: std_logic;
  signal delay3_q_net: std_logic;
  signal edge_op_y_net_x4: std_logic;
  signal enable_y_net: std_logic;
  signal inverter_op_net: std_logic;
  signal logical_y_net: std_logic;
  signal mux1_y_net_x1: std_logic;
  signal mux2_y_net: std_logic;
  signal register1_q_net: std_logic;
  signal register2_q_net_x3: std_logic;
  signal register6_q_net_x1: std_logic;
  signal reint1_output_port_net_x0: std_logic_vector(31 downto 0);
  signal trig_src_y_net: std_logic;
  signal valid_src_y_net: std_logic;
  signal we_choice_y_net_x2: std_logic;

begin
  ce_1_sg_x33 <= ce_1;
  clk_1_sg_x33 <= clk_1;
  reint1_output_port_net_x0 <= ctrl;
  cast_dout_net_x1 <= din;
  register2_q_net_x3 <= trig;
  constant_op_net_x1 <= we;
  dout <= data_choice_y_net_x2;
  go <= register6_q_net_x1;
  init <= edge_op_y_net_x4;
  we_o <= we_choice_y_net_x2;

  constant1: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant1_op_net
    );

  constant2: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant2_op_net
    );

  constant_x0: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant_op_net_x0
    );

  delay1: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x33,
      clk => clk_1_sg_x33,
      d(0) => enable_y_net,
      en => '1',
      rst => '1',
      q(0) => delay1_q_net_x0
    );

  delay2: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x33,
      clk => clk_1_sg_x33,
      d(0) => trig_src_y_net,
      en => '1',
      rst => '1',
      q(0) => delay2_q_net
    );

  delay3: entity work.xldelay
    generic map (
      latency => 1,
      reg_retiming => 0,
      reset => 0,
      width => 1
    )
    port map (
      ce => ce_1_sg_x33,
      clk => clk_1_sg_x33,
      d(0) => valid_src_y_net,
      en => '1',
      rst => '1',
      q(0) => delay3_q_net
    );

  dram_munge_f22062c5c2: entity work.dram_munge_entity_f22062c5c2
    port map (
      ce_1 => ce_1_sg_x33,
      clk_1 => clk_1_sg_x33,
      din => cast_dout_net_x1,
      init => edge_op_y_net_x4,
      we => mux1_y_net_x1,
      dout => data_choice_y_net_x2,
      we_o => we_choice_y_net_x2
    );

  edge_detect_c8be931955: entity work.edge_detect_entity_c8be931955
    port map (
      ce_1 => ce_1_sg_x33,
      clk_1 => clk_1_sg_x33,
      in_x0 => delay1_q_net_x0,
      out_x0 => edge_op_y_net_x4
    );

  enable: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 0,
      x_width => 32,
      y_width => 1
    )
    port map (
      x => reint1_output_port_net_x0,
      y(0) => enable_y_net
    );

  inverter: entity work.inverter_e5b38cca3b
    port map (
      ce => ce_1_sg_x33,
      clk => clk_1_sg_x33,
      clr => '0',
      ip(0) => edge_op_y_net_x4,
      op(0) => inverter_op_net
    );

  logical: entity work.logical_80f90b97d0
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => mux2_y_net,
      d1(0) => inverter_op_net,
      y(0) => logical_y_net
    );

  mux1: entity work.mux_d99e59b6d4
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => constant_op_net_x1,
      d1(0) => constant2_op_net,
      sel(0) => delay3_q_net,
      y(0) => mux1_y_net_x1
    );

  mux2: entity work.mux_d99e59b6d4
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => register2_q_net_x3,
      d1(0) => constant1_op_net,
      sel(0) => delay2_q_net,
      y(0) => mux2_y_net
    );

  register1: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x33,
      clk => clk_1_sg_x33,
      d(0) => constant_op_net_x0,
      en(0) => edge_op_y_net_x4,
      rst(0) => logical_y_net,
      q(0) => register1_q_net
    );

  register6: entity work.xlregister
    generic map (
      d_width => 1,
      init_value => b"0"
    )
    port map (
      ce => ce_1_sg_x33,
      clk => clk_1_sg_x33,
      d(0) => mux2_y_net,
      en(0) => register1_q_net,
      rst(0) => edge_op_y_net_x4,
      q(0) => register6_q_net_x1
    );

  trig_src: entity work.xlslice
    generic map (
      new_lsb => 1,
      new_msb => 1,
      x_width => 32,
      y_width => 1
    )
    port map (
      x => reint1_output_port_net_x0,
      y(0) => trig_src_y_net
    );

  valid_src: entity work.xlslice
    generic map (
      new_lsb => 2,
      new_msb => 2,
      x_width => 32,
      y_width => 1
    )
    port map (
      x => reint1_output_port_net_x0,
      y(0) => valid_src_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/bram/calc_add"

entity calc_add_entity_3d0a6b5da7 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    in_x0: in std_logic_vector(9 downto 0); 
    out_x0: out std_logic_vector(9 downto 0)
  );
end calc_add_entity_3d0a6b5da7;

architecture structural of calc_add_entity_3d0a6b5da7 is
  signal add_del_q_net_x0: std_logic_vector(9 downto 0);
  signal add_sub_s_net: std_logic;
  signal ce_1_sg_x34: std_logic;
  signal clk_1_sg_x34: std_logic;
  signal concat_y_net: std_logic_vector(9 downto 0);
  signal const_op_net: std_logic;
  signal convert_addr_dout_net: std_logic_vector(9 downto 0);
  signal lsw_y_net: std_logic;
  signal manipulate_op_net: std_logic;
  signal msw_y_net: std_logic_vector(8 downto 0);
  signal mux_y_net_x0: std_logic_vector(9 downto 0);

begin
  ce_1_sg_x34 <= ce_1;
  clk_1_sg_x34 <= clk_1;
  add_del_q_net_x0 <= in_x0;
  out_x0 <= mux_y_net_x0;

  add_sub: entity work.addsub_c13097e33e
    port map (
      a(0) => const_op_net,
      b(0) => lsw_y_net,
      ce => ce_1_sg_x34,
      clk => clk_1_sg_x34,
      clr => '0',
      s(0) => add_sub_s_net
    );

  concat: entity work.concat_1d98d96b58
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0 => msw_y_net,
      in1(0) => add_sub_s_net,
      y => concat_y_net
    );

  const: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => const_op_net
    );

  convert_addr: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 10,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 10,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x34,
      clk => clk_1_sg_x34,
      clr => '0',
      din => add_del_q_net_x0,
      en => "1",
      dout => convert_addr_dout_net
    );

  lsw: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 0,
      x_width => 10,
      y_width => 1
    )
    port map (
      x => convert_addr_dout_net,
      y(0) => lsw_y_net
    );

  manipulate: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => manipulate_op_net
    );

  msw: entity work.xlslice
    generic map (
      new_lsb => 1,
      new_msb => 9,
      x_width => 10,
      y_width => 9
    )
    port map (
      x => convert_addr_dout_net,
      y => msw_y_net
    );

  mux: entity work.mux_4fe5face7f
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0 => convert_addr_dout_net,
      d1 => concat_y_net,
      sel(0) => manipulate_op_net,
      y => mux_y_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/bram/munge_in/join"

entity join_entity_c0bcbc1513 is
  port (
    in1: in std_logic_vector(31 downto 0); 
    in2: in std_logic_vector(31 downto 0); 
    in3: in std_logic_vector(31 downto 0); 
    in4: in std_logic_vector(31 downto 0); 
    bus_out: out std_logic_vector(127 downto 0)
  );
end join_entity_c0bcbc1513;

architecture structural of join_entity_c0bcbc1513 is
  signal concatenate_y_net_x0: std_logic_vector(127 downto 0);
  signal reinterpret1_output_port_net: std_logic_vector(31 downto 0);
  signal reinterpret1_output_port_net_x1: std_logic_vector(31 downto 0);
  signal reinterpret2_output_port_net: std_logic_vector(31 downto 0);
  signal reinterpret2_output_port_net_x1: std_logic_vector(31 downto 0);
  signal reinterpret3_output_port_net: std_logic_vector(31 downto 0);
  signal reinterpret3_output_port_net_x1: std_logic_vector(31 downto 0);
  signal reinterpret4_output_port_net: std_logic_vector(31 downto 0);
  signal reinterpret4_output_port_net_x1: std_logic_vector(31 downto 0);

begin
  reinterpret1_output_port_net_x1 <= in1;
  reinterpret2_output_port_net_x1 <= in2;
  reinterpret3_output_port_net_x1 <= in3;
  reinterpret4_output_port_net_x1 <= in4;
  bus_out <= concatenate_y_net_x0;

  concatenate: entity work.concat_b11ec1c0d4
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      in0 => reinterpret1_output_port_net,
      in1 => reinterpret2_output_port_net,
      in2 => reinterpret3_output_port_net,
      in3 => reinterpret4_output_port_net,
      y => concatenate_y_net_x0
    );

  reinterpret1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret1_output_port_net_x1,
      output_port => reinterpret1_output_port_net
    );

  reinterpret2: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret2_output_port_net_x1,
      output_port => reinterpret2_output_port_net
    );

  reinterpret3: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret3_output_port_net_x1,
      output_port => reinterpret3_output_port_net
    );

  reinterpret4: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => reinterpret4_output_port_net_x1,
      output_port => reinterpret4_output_port_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/bram/munge_in/split"

entity split_entity_5673bc47c6 is
  port (
    bus_in: in std_logic_vector(127 downto 0); 
    lsb_out1: out std_logic_vector(31 downto 0); 
    msb_out4: out std_logic_vector(31 downto 0); 
    out2: out std_logic_vector(31 downto 0); 
    out3: out std_logic_vector(31 downto 0)
  );
end split_entity_5673bc47c6;

architecture structural of split_entity_5673bc47c6 is
  signal reinterpret1_output_port_net_x2: std_logic_vector(31 downto 0);
  signal reinterpret2_output_port_net_x2: std_logic_vector(31 downto 0);
  signal reinterpret3_output_port_net_x2: std_logic_vector(31 downto 0);
  signal reinterpret4_output_port_net_x2: std_logic_vector(31 downto 0);
  signal reinterpret_output_port_net_x0: std_logic_vector(127 downto 0);
  signal slice1_y_net: std_logic_vector(31 downto 0);
  signal slice2_y_net: std_logic_vector(31 downto 0);
  signal slice3_y_net: std_logic_vector(31 downto 0);
  signal slice4_y_net: std_logic_vector(31 downto 0);

begin
  reinterpret_output_port_net_x0 <= bus_in;
  lsb_out1 <= reinterpret1_output_port_net_x2;
  msb_out4 <= reinterpret4_output_port_net_x2;
  out2 <= reinterpret2_output_port_net_x2;
  out3 <= reinterpret3_output_port_net_x2;

  reinterpret1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice1_y_net,
      output_port => reinterpret1_output_port_net_x2
    );

  reinterpret2: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice2_y_net,
      output_port => reinterpret2_output_port_net_x2
    );

  reinterpret3: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice3_y_net,
      output_port => reinterpret3_output_port_net_x2
    );

  reinterpret4: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice4_y_net,
      output_port => reinterpret4_output_port_net_x2
    );

  slice1: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 128,
      y_width => 32
    )
    port map (
      x => reinterpret_output_port_net_x0,
      y => slice1_y_net
    );

  slice2: entity work.xlslice
    generic map (
      new_lsb => 32,
      new_msb => 63,
      x_width => 128,
      y_width => 32
    )
    port map (
      x => reinterpret_output_port_net_x0,
      y => slice2_y_net
    );

  slice3: entity work.xlslice
    generic map (
      new_lsb => 64,
      new_msb => 95,
      x_width => 128,
      y_width => 32
    )
    port map (
      x => reinterpret_output_port_net_x0,
      y => slice3_y_net
    );

  slice4: entity work.xlslice
    generic map (
      new_lsb => 96,
      new_msb => 127,
      x_width => 128,
      y_width => 32
    )
    port map (
      x => reinterpret_output_port_net_x0,
      y => slice4_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/bram/munge_in"

entity munge_in_entity_544ba8463b is
  port (
    din: in std_logic_vector(127 downto 0); 
    dout: out std_logic_vector(127 downto 0)
  );
end munge_in_entity_544ba8463b;

architecture structural of munge_in_entity_544ba8463b is
  signal concatenate_y_net_x0: std_logic_vector(127 downto 0);
  signal dat_del_q_net_x0: std_logic_vector(127 downto 0);
  signal reinterpret1_output_port_net_x2: std_logic_vector(31 downto 0);
  signal reinterpret2_output_port_net_x2: std_logic_vector(31 downto 0);
  signal reinterpret3_output_port_net_x2: std_logic_vector(31 downto 0);
  signal reinterpret4_output_port_net_x2: std_logic_vector(31 downto 0);
  signal reinterpret_out_output_port_net_x0: std_logic_vector(127 downto 0);
  signal reinterpret_output_port_net_x0: std_logic_vector(127 downto 0);

begin
  dat_del_q_net_x0 <= din;
  dout <= reinterpret_out_output_port_net_x0;

  join_c0bcbc1513: entity work.join_entity_c0bcbc1513
    port map (
      in1 => reinterpret1_output_port_net_x2,
      in2 => reinterpret2_output_port_net_x2,
      in3 => reinterpret3_output_port_net_x2,
      in4 => reinterpret4_output_port_net_x2,
      bus_out => concatenate_y_net_x0
    );

  reinterpret: entity work.reinterpret_28b9ecc6fc
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => dat_del_q_net_x0,
      output_port => reinterpret_output_port_net_x0
    );

  reinterpret_out: entity work.reinterpret_28b9ecc6fc
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => concatenate_y_net_x0,
      output_port => reinterpret_out_output_port_net_x0
    );

  split_5673bc47c6: entity work.split_entity_5673bc47c6
    port map (
      bus_in => reinterpret_output_port_net_x0,
      lsb_out1 => reinterpret1_output_port_net_x2,
      msb_out4 => reinterpret4_output_port_net_x2,
      out2 => reinterpret2_output_port_net_x2,
      out3 => reinterpret3_output_port_net_x2
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/bram"

entity bram_entity_0ad6edfe87 is
  port (
    addr: in std_logic_vector(9 downto 0); 
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    data_in: in std_logic_vector(127 downto 0); 
    we: in std_logic; 
    convert_addr_x0: out std_logic_vector(9 downto 0); 
    convert_din1_x0: out std_logic_vector(127 downto 0); 
    convert_we_x0: out std_logic
  );
end bram_entity_0ad6edfe87;

architecture structural of bram_entity_0ad6edfe87 is
  signal add_del_q_net_x1: std_logic_vector(9 downto 0);
  signal ce_1_sg_x35: std_logic;
  signal clk_1_sg_x35: std_logic;
  signal convert_addr_dout_net_x0: std_logic_vector(9 downto 0);
  signal convert_din1_dout_net_x0: std_logic_vector(127 downto 0);
  signal convert_we_dout_net_x0: std_logic;
  signal dat_del_q_net_x1: std_logic_vector(127 downto 0);
  signal mux_y_net_x0: std_logic_vector(9 downto 0);
  signal reinterpret_out_output_port_net_x0: std_logic_vector(127 downto 0);
  signal we_del_q_net_x0: std_logic;

begin
  add_del_q_net_x1 <= addr;
  ce_1_sg_x35 <= ce_1;
  clk_1_sg_x35 <= clk_1;
  dat_del_q_net_x1 <= data_in;
  we_del_q_net_x0 <= we;
  convert_addr_x0 <= convert_addr_dout_net_x0;
  convert_din1_x0 <= convert_din1_dout_net_x0;
  convert_we_x0 <= convert_we_dout_net_x0;

  calc_add_3d0a6b5da7: entity work.calc_add_entity_3d0a6b5da7
    port map (
      ce_1 => ce_1_sg_x35,
      clk_1 => clk_1_sg_x35,
      in_x0 => add_del_q_net_x1,
      out_x0 => mux_y_net_x0
    );

  convert_addr: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 10,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 10,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x35,
      clk => clk_1_sg_x35,
      clr => '0',
      din => mux_y_net_x0,
      en => "1",
      dout => convert_addr_dout_net_x0
    );

  convert_din1: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 128,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 128,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x35,
      clk => clk_1_sg_x35,
      clr => '0',
      din => reinterpret_out_output_port_net_x0,
      en => "1",
      dout => convert_din1_dout_net_x0
    );

  convert_we: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 1,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 1,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x35,
      clk => clk_1_sg_x35,
      clr => '0',
      din(0) => we_del_q_net_x0,
      en => "1",
      dout(0) => convert_we_dout_net_x0
    );

  munge_in_544ba8463b: entity work.munge_in_entity_544ba8463b
    port map (
      din => dat_del_q_net_x1,
      dout => reinterpret_out_output_port_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/ctrl"

entity ctrl_entity_76b182464d is
  port (
    top_scope_raw_0_snap_ctrl_user_data_out: in std_logic_vector(31 downto 0); 
    in_reg: out std_logic_vector(31 downto 0)
  );
end ctrl_entity_76b182464d;

architecture structural of ctrl_entity_76b182464d is
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net_x1: std_logic_vector(31 downto 0);
  signal slice_reg_y_net: std_logic_vector(31 downto 0);
  signal top_scope_raw_0_snap_ctrl_user_data_out_net_x0: std_logic_vector(31 downto 0);

begin
  top_scope_raw_0_snap_ctrl_user_data_out_net_x0 <= top_scope_raw_0_snap_ctrl_user_data_out;
  in_reg <= reint1_output_port_net_x1;

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => top_scope_raw_0_snap_ctrl_user_data_out_net_x0,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => slice_reg_y_net,
      output_port => reint1_output_port_net_x1
    );

  slice_reg: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 31,
      x_width => 32,
      y_width => 32
    )
    port map (
      x => io_delay_q_net,
      y => slice_reg_y_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap/status"

entity status_entity_00e2beb60f is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    out_reg: in std_logic_vector(31 downto 0); 
    cast_gw_x0: out std_logic_vector(31 downto 0)
  );
end status_entity_00e2beb60f;

architecture structural of status_entity_00e2beb60f is
  signal assert_reg_dout_net: std_logic_vector(31 downto 0);
  signal cast_gw_dout_net_x0: std_logic_vector(31 downto 0);
  signal ce_1_sg_x36: std_logic;
  signal clk_1_sg_x36: std_logic;
  signal concat_y_net_x1: std_logic_vector(31 downto 0);
  signal io_delay_q_net: std_logic_vector(31 downto 0);
  signal reint1_output_port_net: std_logic_vector(31 downto 0);

begin
  ce_1_sg_x36 <= ce_1;
  clk_1_sg_x36 <= clk_1;
  concat_y_net_x1 <= out_reg;
  cast_gw_x0 <= cast_gw_dout_net_x0;

  assert_reg: entity work.xlpassthrough
    generic map (
      din_width => 32,
      dout_width => 32
    )
    port map (
      din => concat_y_net_x1,
      dout => assert_reg_dout_net
    );

  cast_gw: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 32,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 32,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x36,
      clk => clk_1_sg_x36,
      clr => '0',
      din => io_delay_q_net,
      en => "1",
      dout => cast_gw_dout_net_x0
    );

  io_delay: entity work.delay_2b0feb00fb
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d => reint1_output_port_net,
      q => io_delay_q_net
    );

  reint1: entity work.reinterpret_c5d4d59b73
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => assert_reg_dout_net,
      output_port => reint1_output_port_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/snap"

entity snap_entity_7c4700cb51 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    din: in std_logic_vector(127 downto 0); 
    top_scope_raw_0_snap_ctrl_user_data_out: in std_logic_vector(31 downto 0); 
    trig: in std_logic; 
    we: in std_logic; 
    bram: out std_logic_vector(9 downto 0); 
    bram_x0: out std_logic_vector(127 downto 0); 
    bram_x1: out std_logic; 
    status: out std_logic_vector(31 downto 0)
  );
end snap_entity_7c4700cb51;

architecture structural of snap_entity_7c4700cb51 is
  signal add_del_q_net_x1: std_logic_vector(9 downto 0);
  signal cast_dout_net_x1: std_logic_vector(127 downto 0);
  signal cast_gw_dout_net_x1: std_logic_vector(31 downto 0);
  signal ce_1_sg_x37: std_logic;
  signal clk_1_sg_x37: std_logic;
  signal concat_y_net_x1: std_logic_vector(31 downto 0);
  signal constant_op_net_x2: std_logic;
  signal convert_addr_dout_net_x1: std_logic_vector(9 downto 0);
  signal convert_din1_dout_net_x1: std_logic_vector(127 downto 0);
  signal convert_we_dout_net_x1: std_logic;
  signal dat_del_q_net_x1: std_logic_vector(127 downto 0);
  signal data_choice_y_net_x2: std_logic_vector(127 downto 0);
  signal delay6_q_net_x0: std_logic_vector(127 downto 0);
  signal edge_op_y_net_x4: std_logic;
  signal logical6_y_net_x0: std_logic;
  signal never_op_net_x0: std_logic;
  signal register2_q_net_x4: std_logic_vector(127 downto 0);
  signal register2_q_net_x5: std_logic;
  signal register6_q_net_x1: std_logic;
  signal reint1_output_port_net_x1: std_logic_vector(31 downto 0);
  signal ri_output_port_net: std_logic_vector(127 downto 0);
  signal slice2_y_net_x0: std_logic_vector(9 downto 0);
  signal top_scope_raw_0_snap_ctrl_user_data_out_net_x1: std_logic_vector(31 downto 0);
  signal we_choice_y_net_x2: std_logic;
  signal we_del_q_net_x0: std_logic;

begin
  ce_1_sg_x37 <= ce_1;
  clk_1_sg_x37 <= clk_1;
  register2_q_net_x4 <= din;
  top_scope_raw_0_snap_ctrl_user_data_out_net_x1 <= top_scope_raw_0_snap_ctrl_user_data_out;
  register2_q_net_x5 <= trig;
  constant_op_net_x2 <= we;
  bram <= convert_addr_dout_net_x1;
  bram_x0 <= convert_din1_dout_net_x1;
  bram_x1 <= convert_we_dout_net_x1;
  status <= cast_gw_dout_net_x1;

  add_del: entity work.delay_cf4f99539f
    port map (
      ce => ce_1_sg_x37,
      clk => clk_1_sg_x37,
      clr => '0',
      d => slice2_y_net_x0,
      q => add_del_q_net_x1
    );

  add_gen_9cb67b3920: entity work.add_gen_entity_9cb67b3920
    port map (
      ce_1 => ce_1_sg_x37,
      clk_1 => clk_1_sg_x37,
      cont => never_op_net_x0,
      din => data_choice_y_net_x2,
      go => register6_q_net_x1,
      init => edge_op_y_net_x4,
      we => we_choice_y_net_x2,
      add => slice2_y_net_x0,
      dout => delay6_q_net_x0,
      status => concat_y_net_x1,
      we_o => logical6_y_net_x0
    );

  basic_ctrl_deb8f19a04: entity work.basic_ctrl_entity_deb8f19a04
    port map (
      ce_1 => ce_1_sg_x37,
      clk_1 => clk_1_sg_x37,
      ctrl => reint1_output_port_net_x1,
      din => cast_dout_net_x1,
      trig => register2_q_net_x5,
      we => constant_op_net_x2,
      dout => data_choice_y_net_x2,
      go => register6_q_net_x1,
      init => edge_op_y_net_x4,
      we_o => we_choice_y_net_x2
    );

  bram_0ad6edfe87: entity work.bram_entity_0ad6edfe87
    port map (
      addr => add_del_q_net_x1,
      ce_1 => ce_1_sg_x37,
      clk_1 => clk_1_sg_x37,
      data_in => dat_del_q_net_x1,
      we => we_del_q_net_x0,
      convert_addr_x0 => convert_addr_dout_net_x1,
      convert_din1_x0 => convert_din1_dout_net_x1,
      convert_we_x0 => convert_we_dout_net_x1
    );

  cast: entity work.xlconvert
    generic map (
      bool_conversion => 0,
      din_arith => 1,
      din_bin_pt => 0,
      din_width => 128,
      dout_arith => 1,
      dout_bin_pt => 0,
      dout_width => 128,
      latency => 0,
      overflow => xlWrap,
      quantization => xlTruncate
    )
    port map (
      ce => ce_1_sg_x37,
      clk => clk_1_sg_x37,
      clr => '0',
      din => ri_output_port_net,
      en => "1",
      dout => cast_dout_net_x1
    );

  ctrl_76b182464d: entity work.ctrl_entity_76b182464d
    port map (
      top_scope_raw_0_snap_ctrl_user_data_out => top_scope_raw_0_snap_ctrl_user_data_out_net_x1,
      in_reg => reint1_output_port_net_x1
    );

  dat_del: entity work.delay_ee0f706095
    port map (
      ce => ce_1_sg_x37,
      clk => clk_1_sg_x37,
      clr => '0',
      d => delay6_q_net_x0,
      q => dat_del_q_net_x1
    );

  never: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => never_op_net_x0
    );

  ri: entity work.reinterpret_28b9ecc6fc
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      input_port => register2_q_net_x4,
      output_port => ri_output_port_net
    );

  status_00e2beb60f: entity work.status_entity_00e2beb60f
    port map (
      ce_1 => ce_1_sg_x37,
      clk_1 => clk_1_sg_x37,
      out_reg => concat_y_net_x1,
      cast_gw_x0 => cast_gw_dout_net_x1
    );

  we_del: entity work.delay_9f02caa990
    port map (
      ce => ce_1_sg_x37,
      clk => clk_1_sg_x37,
      clr => '0',
      d(0) => logical6_y_net_x0,
      q(0) => we_del_q_net_x0
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0/sync_gen"

entity sync_gen_entity_d600b6d7c4 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    rst: in std_logic; 
    sync: out std_logic
  );
end sync_gen_entity_d600b6d7c4;

architecture structural of sync_gen_entity_d600b6d7c4 is
  signal ce_1_sg_x38: std_logic;
  signal clk_1_sg_x38: std_logic;
  signal constant1_op_net_x0: std_logic;
  signal constant1_op_net_x1: std_logic;
  signal constant6_op_net: std_logic_vector(28 downto 0);
  signal counter1_op_net: std_logic_vector(28 downto 0);
  signal logical_y_net: std_logic;
  signal mux_y_net_x1: std_logic;
  signal relational_op_net: std_logic;

begin
  ce_1_sg_x38 <= ce_1;
  clk_1_sg_x38 <= clk_1;
  constant1_op_net_x1 <= rst;
  sync <= mux_y_net_x1;

  constant1: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant1_op_net_x0
    );

  constant6: entity work.constant_a7c3e0a1bc
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op => constant6_op_net
    );

  counter1: entity work.counter_a8200d1d61
    port map (
      ce => ce_1_sg_x38,
      clk => clk_1_sg_x38,
      clr => '0',
      rst(0) => logical_y_net,
      op => counter1_op_net
    );

  logical: entity work.logical_aacf6e1b0e
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => constant1_op_net_x1,
      d1(0) => relational_op_net,
      y(0) => logical_y_net
    );

  mux: entity work.mux_d99e59b6d4
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      d0(0) => relational_op_net,
      d1(0) => constant1_op_net_x0,
      sel(0) => constant1_op_net_x1,
      y(0) => mux_y_net_x1
    );

  relational: entity work.relational_1e662b6629
    port map (
      a => counter1_op_net,
      b => constant6_op_net,
      ce => ce_1_sg_x38,
      clk => clk_1_sg_x38,
      clr => '0',
      op(0) => relational_op_net
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/scope_raw_0"

entity scope_raw_0_entity_9c55b9e8c3 is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    raw_0: in std_logic_vector(127 downto 0); 
    top_scope_raw_0_snap_ctrl_user_data_out: in std_logic_vector(31 downto 0); 
    snap: out std_logic_vector(9 downto 0); 
    snap_x0: out std_logic_vector(127 downto 0); 
    snap_x1: out std_logic; 
    snap_x2: out std_logic_vector(31 downto 0)
  );
end scope_raw_0_entity_9c55b9e8c3;

architecture structural of scope_raw_0_entity_9c55b9e8c3 is
  signal cast_gw_dout_net_x2: std_logic_vector(31 downto 0);
  signal ce_1_sg_x39: std_logic;
  signal clk_1_sg_x39: std_logic;
  signal constant1_op_net_x1: std_logic;
  signal constant_op_net_x2: std_logic;
  signal convert_addr_dout_net_x2: std_logic_vector(9 downto 0);
  signal convert_din1_dout_net_x2: std_logic_vector(127 downto 0);
  signal convert_we_dout_net_x2: std_logic;
  signal mux_y_net_x1: std_logic;
  signal register2_q_net_x1: std_logic;
  signal register2_q_net_x5: std_logic;
  signal register2_q_net_x6: std_logic_vector(127 downto 0);
  signal top_scope_raw_0_snap_ctrl_user_data_out_net_x2: std_logic_vector(31 downto 0);

begin
  ce_1_sg_x39 <= ce_1;
  clk_1_sg_x39 <= clk_1;
  register2_q_net_x6 <= raw_0;
  top_scope_raw_0_snap_ctrl_user_data_out_net_x2 <= top_scope_raw_0_snap_ctrl_user_data_out;
  snap <= convert_addr_dout_net_x2;
  snap_x0 <= convert_din1_dout_net_x2;
  snap_x1 <= convert_we_dout_net_x2;
  snap_x2 <= cast_gw_dout_net_x2;

  constant1: entity work.constant_963ed6358a
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant1_op_net_x1
    );

  constant_x0: entity work.constant_6293007044
    port map (
      ce => '0',
      clk => '0',
      clr => '0',
      op(0) => constant_op_net_x2
    );

  pipeline2_bd8be06449: entity work.pipeline2_entity_bd8be06449
    port map (
      ce_1 => ce_1_sg_x39,
      clk_1 => clk_1_sg_x39,
      d => mux_y_net_x1,
      q => register2_q_net_x1
    );

  pipeline3_30f5281c7d: entity work.pipeline2_entity_bd8be06449
    port map (
      ce_1 => ce_1_sg_x39,
      clk_1 => clk_1_sg_x39,
      d => register2_q_net_x1,
      q => register2_q_net_x5
    );

  snap_7c4700cb51: entity work.snap_entity_7c4700cb51
    port map (
      ce_1 => ce_1_sg_x39,
      clk_1 => clk_1_sg_x39,
      din => register2_q_net_x6,
      top_scope_raw_0_snap_ctrl_user_data_out => top_scope_raw_0_snap_ctrl_user_data_out_net_x2,
      trig => register2_q_net_x5,
      we => constant_op_net_x2,
      bram => convert_addr_dout_net_x2,
      bram_x0 => convert_din1_dout_net_x2,
      bram_x1 => convert_we_dout_net_x2,
      status => cast_gw_dout_net_x2
    );

  sync_gen_d600b6d7c4: entity work.sync_gen_entity_d600b6d7c4
    port map (
      ce_1 => ce_1_sg_x39,
      clk_1 => clk_1_sg_x39,
      rst => constant1_op_net_x1,
      sync => mux_y_net_x1
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top/switch"

entity switch_entity_e404048cbb is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    det: in std_logic; 
    din: in std_logic_vector(127 downto 0); 
    dout1: out std_logic_vector(63 downto 0); 
    dout2: out std_logic_vector(63 downto 0); 
    eof0: out std_logic; 
    eof2: out std_logic; 
    valid0: out std_logic; 
    valid2: out std_logic
  );
end switch_entity_e404048cbb;

architecture structural of switch_entity_e404048cbb is
  component switch
    port (
      ce: in std_logic; 
      clk: in std_logic; 
      det: in std_logic; 
      reset: in std_logic; 
      eof01: out std_logic; 
      eof23: out std_logic; 
      valid01: out std_logic; 
      valid23: out std_logic
    );
  end component;
  signal asdf1_y_net_x3: std_logic_vector(63 downto 0);
  signal asdf2_y_net_x3: std_logic_vector(63 downto 0);
  signal ce_1_sg_x41: std_logic;
  signal clk_1_sg_x41: std_logic;
  signal relational2_op_net_x0: std_logic;
  signal switch_eof01_net_x3: std_logic;
  signal switch_eof23_net_x3: std_logic;
  signal switch_valid01_net_x3: std_logic;
  signal switch_valid23_net_x3: std_logic;
  signal top_data_bypass_topin_data_out_net_x0: std_logic_vector(127 downto 0);
  signal top_data_bypass_topin_data_out_vld_net_x0: std_logic;

begin
  ce_1_sg_x41 <= ce_1;
  clk_1_sg_x41 <= clk_1;
  top_data_bypass_topin_data_out_vld_net_x0 <= det;
  top_data_bypass_topin_data_out_net_x0 <= din;
  dout1 <= asdf2_y_net_x3;
  dout2 <= asdf1_y_net_x3;
  eof0 <= switch_eof01_net_x3;
  eof2 <= switch_eof23_net_x3;
  valid0 <= switch_valid01_net_x3;
  valid2 <= switch_valid23_net_x3;

  asdf1: entity work.xlslice
    generic map (
      new_lsb => 64,
      new_msb => 127,
      x_width => 128,
      y_width => 64
    )
    port map (
      x => top_data_bypass_topin_data_out_net_x0,
      y => asdf1_y_net_x3
    );

  asdf2: entity work.xlslice
    generic map (
      new_lsb => 0,
      new_msb => 63,
      x_width => 128,
      y_width => 64
    )
    port map (
      x => top_data_bypass_topin_data_out_net_x0,
      y => asdf2_y_net_x3
    );

  reset3_c3a01ac85f: entity work.reset_entity_7f945ff177
    port map (
      ce_1 => ce_1_sg_x41,
      clk_1 => clk_1_sg_x41,
      reset => relational2_op_net_x0
    );

  switch_x0: switch
    port map (
      ce => ce_1_sg_x41,
      clk => clk_1_sg_x41,
      det => top_data_bypass_topin_data_out_vld_net_x0,
      reset => relational2_op_net_x0,
      eof01 => switch_eof01_net_x3,
      eof23 => switch_eof23_net_x3,
      valid01 => switch_valid01_net_x3,
      valid23 => switch_valid23_net_x3
    );

end structural;
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

-- Generated from Simulink block "top"

entity top is
  port (
    ce_1: in std_logic; 
    clk_1: in std_logic; 
    top_adc_adc_sync: in std_logic; 
    top_adc_adc_user_data_i0: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i1: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i2: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i3: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i4: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i5: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i6: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_i7: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q0: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q1: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q2: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q3: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q4: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q5: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q6: in std_logic_vector(7 downto 0); 
    top_adc_adc_user_data_q7: in std_logic_vector(7 downto 0); 
    top_adc_enable_user_data_out: in std_logic_vector(31 downto 0); 
    top_data_bypass_topin_data_out: in std_logic_vector(127 downto 0); 
    top_data_bypass_topin_data_out_vld: in std_logic; 
    top_data_bypass_topout_data_out: in std_logic_vector(127 downto 0); 
    top_data_bypass_topout_data_out_vld: in std_logic; 
    top_ethout_dest_ip0_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_ip1_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_ip2_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_ip3_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_port0_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_port1_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_port2_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_dest_port3_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_rst_user_data_out: in std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe0_led_rx: in std_logic; 
    top_ethout_ten_gbe0_led_tx: in std_logic; 
    top_ethout_ten_gbe0_led_up: in std_logic; 
    top_ethout_ten_gbe0_rx_bad_frame: in std_logic; 
    top_ethout_ten_gbe0_rx_data: in std_logic_vector(63 downto 0); 
    top_ethout_ten_gbe0_rx_end_of_frame: in std_logic; 
    top_ethout_ten_gbe0_rx_overrun: in std_logic; 
    top_ethout_ten_gbe0_rx_source_ip: in std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe0_rx_source_port: in std_logic_vector(15 downto 0); 
    top_ethout_ten_gbe0_rx_valid: in std_logic; 
    top_ethout_ten_gbe0_tx_afull: in std_logic; 
    top_ethout_ten_gbe0_tx_overflow: in std_logic; 
    top_ethout_ten_gbe1_led_rx: in std_logic; 
    top_ethout_ten_gbe1_led_tx: in std_logic; 
    top_ethout_ten_gbe1_led_up: in std_logic; 
    top_ethout_ten_gbe1_rx_bad_frame: in std_logic; 
    top_ethout_ten_gbe1_rx_data: in std_logic_vector(63 downto 0); 
    top_ethout_ten_gbe1_rx_end_of_frame: in std_logic; 
    top_ethout_ten_gbe1_rx_overrun: in std_logic; 
    top_ethout_ten_gbe1_rx_source_ip: in std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe1_rx_source_port: in std_logic_vector(15 downto 0); 
    top_ethout_ten_gbe1_rx_valid: in std_logic; 
    top_ethout_ten_gbe1_tx_afull: in std_logic; 
    top_ethout_ten_gbe1_tx_overflow: in std_logic; 
    top_ethout_ten_gbe2_led_rx: in std_logic; 
    top_ethout_ten_gbe2_led_tx: in std_logic; 
    top_ethout_ten_gbe2_led_up: in std_logic; 
    top_ethout_ten_gbe2_rx_bad_frame: in std_logic; 
    top_ethout_ten_gbe2_rx_data: in std_logic_vector(63 downto 0); 
    top_ethout_ten_gbe2_rx_end_of_frame: in std_logic; 
    top_ethout_ten_gbe2_rx_overrun: in std_logic; 
    top_ethout_ten_gbe2_rx_source_ip: in std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe2_rx_source_port: in std_logic_vector(15 downto 0); 
    top_ethout_ten_gbe2_rx_valid: in std_logic; 
    top_ethout_ten_gbe2_tx_afull: in std_logic; 
    top_ethout_ten_gbe2_tx_overflow: in std_logic; 
    top_ethout_ten_gbe3_led_rx: in std_logic; 
    top_ethout_ten_gbe3_led_tx: in std_logic; 
    top_ethout_ten_gbe3_led_up: in std_logic; 
    top_ethout_ten_gbe3_rx_bad_frame: in std_logic; 
    top_ethout_ten_gbe3_rx_data: in std_logic_vector(63 downto 0); 
    top_ethout_ten_gbe3_rx_end_of_frame: in std_logic; 
    top_ethout_ten_gbe3_rx_overrun: in std_logic; 
    top_ethout_ten_gbe3_rx_source_ip: in std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe3_rx_source_port: in std_logic_vector(15 downto 0); 
    top_ethout_ten_gbe3_rx_valid: in std_logic; 
    top_ethout_ten_gbe3_tx_afull: in std_logic; 
    top_ethout_ten_gbe3_tx_overflow: in std_logic; 
    top_scope_raw_0_snap_bram_data_out: in std_logic_vector(127 downto 0); 
    top_scope_raw_0_snap_ctrl_user_data_out: in std_logic_vector(31 downto 0); 
    top_data_bypass_topin_data_in: out std_logic_vector(127 downto 0); 
    top_data_bypass_topin_en: out std_logic; 
    top_data_bypass_topin_rst_n: out std_logic; 
    top_data_bypass_topout_data_in: out std_logic_vector(127 downto 0); 
    top_data_bypass_topout_en: out std_logic; 
    top_data_bypass_topout_rst_n: out std_logic; 
    top_ethout_ten_gbe0_rst: out std_logic; 
    top_ethout_ten_gbe0_rx_ack: out std_logic; 
    top_ethout_ten_gbe0_rx_overrun_ack: out std_logic; 
    top_ethout_ten_gbe0_tx_data: out std_logic_vector(63 downto 0); 
    top_ethout_ten_gbe0_tx_dest_ip: out std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe0_tx_dest_port: out std_logic_vector(15 downto 0); 
    top_ethout_ten_gbe0_tx_end_of_frame: out std_logic; 
    top_ethout_ten_gbe0_tx_valid: out std_logic; 
    top_ethout_ten_gbe1_rst: out std_logic; 
    top_ethout_ten_gbe1_rx_ack: out std_logic; 
    top_ethout_ten_gbe1_rx_overrun_ack: out std_logic; 
    top_ethout_ten_gbe1_tx_data: out std_logic_vector(63 downto 0); 
    top_ethout_ten_gbe1_tx_dest_ip: out std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe1_tx_dest_port: out std_logic_vector(15 downto 0); 
    top_ethout_ten_gbe1_tx_end_of_frame: out std_logic; 
    top_ethout_ten_gbe1_tx_valid: out std_logic; 
    top_ethout_ten_gbe2_rst: out std_logic; 
    top_ethout_ten_gbe2_rx_ack: out std_logic; 
    top_ethout_ten_gbe2_rx_overrun_ack: out std_logic; 
    top_ethout_ten_gbe2_tx_data: out std_logic_vector(63 downto 0); 
    top_ethout_ten_gbe2_tx_dest_ip: out std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe2_tx_dest_port: out std_logic_vector(15 downto 0); 
    top_ethout_ten_gbe2_tx_end_of_frame: out std_logic; 
    top_ethout_ten_gbe2_tx_valid: out std_logic; 
    top_ethout_ten_gbe3_rst: out std_logic; 
    top_ethout_ten_gbe3_rx_ack: out std_logic; 
    top_ethout_ten_gbe3_rx_overrun_ack: out std_logic; 
    top_ethout_ten_gbe3_tx_data: out std_logic_vector(63 downto 0); 
    top_ethout_ten_gbe3_tx_dest_ip: out std_logic_vector(31 downto 0); 
    top_ethout_ten_gbe3_tx_dest_port: out std_logic_vector(15 downto 0); 
    top_ethout_ten_gbe3_tx_end_of_frame: out std_logic; 
    top_ethout_ten_gbe3_tx_valid: out std_logic; 
    top_scope_raw_0_snap_bram_addr: out std_logic_vector(9 downto 0); 
    top_scope_raw_0_snap_bram_data_in: out std_logic_vector(127 downto 0); 
    top_scope_raw_0_snap_bram_we: out std_logic; 
    top_scope_raw_0_snap_status_user_data_in: out std_logic_vector(31 downto 0)
  );
end top;

architecture structural of top is
  attribute core_generation_info: string;
  attribute core_generation_info of structural : architecture is "top,sysgen_core,{black_box_isim_used=1,clock_period=4.00000000,clocking=Clock_Enables,compilation=NGC_Netlist,sample_periods=1.00000000000,testbench=0,total_blocks=1218,xilinx_adder_subtracter_block=2,xilinx_arithmetic_relational_operator_block=9,xilinx_assert_block=1,xilinx_binary_shift_operator_block=1,xilinx_bit_slice_extractor_block=77,xilinx_black_box_block=1,xilinx_bus_concatenator_block=26,xilinx_bus_multiplexer_block=9,xilinx_constant_block_block=27,xilinx_counter_block=6,xilinx_delay_block=34,xilinx_disregard_subsystem_for_generation_block=1,xilinx_gateway_in_block=84,xilinx_gateway_out_block=49,xilinx_inverter_block=21,xilinx_logical_block_block=11,xilinx_register_block=21,xilinx_single_port_random_access_memory_block=1,xilinx_system_generator_block=1,xilinx_type_converter_block=46,xilinx_type_reinterpreter_block=91,}";

  signal asdf1_y_net_x3: std_logic_vector(63 downto 0);
  signal asdf2_y_net_x3: std_logic_vector(63 downto 0);
  signal ce_1_sg_x42: std_logic;
  signal clk_1_sg_x42: std_logic;
  signal register2_q_net_x6: std_logic_vector(127 downto 0);
  signal register_q_net_x1: std_logic;
  signal relational2_op_net_x2: std_logic;
  signal switch_eof01_net_x3: std_logic;
  signal switch_eof23_net_x3: std_logic;
  signal switch_valid01_net_x3: std_logic;
  signal switch_valid23_net_x3: std_logic;
  signal top_adc_adc_sync_net: std_logic;
  signal top_adc_adc_user_data_i0_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i1_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i2_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i3_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i4_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i5_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i6_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i7_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q0_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q1_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q2_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q3_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q4_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q5_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q6_net: std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q7_net: std_logic_vector(7 downto 0);
  signal top_adc_enable_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_data_bypass_topin_data_in_net: std_logic_vector(127 downto 0);
  signal top_data_bypass_topin_data_out_net: std_logic_vector(127 downto 0);
  signal top_data_bypass_topin_data_out_vld_net: std_logic;
  signal top_data_bypass_topin_en_net: std_logic;
  signal top_data_bypass_topin_rst_n_net: std_logic;
  signal top_data_bypass_topout_data_in_net: std_logic_vector(127 downto 0);
  signal top_data_bypass_topout_data_out_net: std_logic_vector(127 downto 0);
  signal top_data_bypass_topout_data_out_vld_net: std_logic;
  signal top_data_bypass_topout_en_net: std_logic;
  signal top_data_bypass_topout_rst_n_net: std_logic;
  signal top_ethout_dest_ip0_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip1_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip2_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip3_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port0_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port1_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port2_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_ethout_dest_port3_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_ethout_rst_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe0_led_rx_net: std_logic;
  signal top_ethout_ten_gbe0_led_tx_net: std_logic;
  signal top_ethout_ten_gbe0_led_up_net: std_logic;
  signal top_ethout_ten_gbe0_rst_net: std_logic;
  signal top_ethout_ten_gbe0_rx_ack_net: std_logic;
  signal top_ethout_ten_gbe0_rx_bad_frame_net: std_logic;
  signal top_ethout_ten_gbe0_rx_data_net: std_logic_vector(63 downto 0);
  signal top_ethout_ten_gbe0_rx_end_of_frame_net: std_logic;
  signal top_ethout_ten_gbe0_rx_overrun_ack_net: std_logic;
  signal top_ethout_ten_gbe0_rx_overrun_net: std_logic;
  signal top_ethout_ten_gbe0_rx_source_ip_net: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe0_rx_source_port_net: std_logic_vector(15 downto 0);
  signal top_ethout_ten_gbe0_rx_valid_net: std_logic;
  signal top_ethout_ten_gbe0_tx_afull_net: std_logic;
  signal top_ethout_ten_gbe0_tx_data_net: std_logic_vector(63 downto 0);
  signal top_ethout_ten_gbe0_tx_dest_ip_net: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe0_tx_dest_port_net: std_logic_vector(15 downto 0);
  signal top_ethout_ten_gbe0_tx_end_of_frame_net: std_logic;
  signal top_ethout_ten_gbe0_tx_overflow_net: std_logic;
  signal top_ethout_ten_gbe0_tx_valid_net: std_logic;
  signal top_ethout_ten_gbe1_led_rx_net: std_logic;
  signal top_ethout_ten_gbe1_led_tx_net: std_logic;
  signal top_ethout_ten_gbe1_led_up_net: std_logic;
  signal top_ethout_ten_gbe1_rst_net: std_logic;
  signal top_ethout_ten_gbe1_rx_ack_net: std_logic;
  signal top_ethout_ten_gbe1_rx_bad_frame_net: std_logic;
  signal top_ethout_ten_gbe1_rx_data_net: std_logic_vector(63 downto 0);
  signal top_ethout_ten_gbe1_rx_end_of_frame_net: std_logic;
  signal top_ethout_ten_gbe1_rx_overrun_ack_net: std_logic;
  signal top_ethout_ten_gbe1_rx_overrun_net: std_logic;
  signal top_ethout_ten_gbe1_rx_source_ip_net: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe1_rx_source_port_net: std_logic_vector(15 downto 0);
  signal top_ethout_ten_gbe1_rx_valid_net: std_logic;
  signal top_ethout_ten_gbe1_tx_afull_net: std_logic;
  signal top_ethout_ten_gbe1_tx_data_net: std_logic_vector(63 downto 0);
  signal top_ethout_ten_gbe1_tx_dest_ip_net: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe1_tx_dest_port_net: std_logic_vector(15 downto 0);
  signal top_ethout_ten_gbe1_tx_end_of_frame_net: std_logic;
  signal top_ethout_ten_gbe1_tx_overflow_net: std_logic;
  signal top_ethout_ten_gbe1_tx_valid_net: std_logic;
  signal top_ethout_ten_gbe2_led_rx_net: std_logic;
  signal top_ethout_ten_gbe2_led_tx_net: std_logic;
  signal top_ethout_ten_gbe2_led_up_net: std_logic;
  signal top_ethout_ten_gbe2_rst_net: std_logic;
  signal top_ethout_ten_gbe2_rx_ack_net: std_logic;
  signal top_ethout_ten_gbe2_rx_bad_frame_net: std_logic;
  signal top_ethout_ten_gbe2_rx_data_net: std_logic_vector(63 downto 0);
  signal top_ethout_ten_gbe2_rx_end_of_frame_net: std_logic;
  signal top_ethout_ten_gbe2_rx_overrun_ack_net: std_logic;
  signal top_ethout_ten_gbe2_rx_overrun_net: std_logic;
  signal top_ethout_ten_gbe2_rx_source_ip_net: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe2_rx_source_port_net: std_logic_vector(15 downto 0);
  signal top_ethout_ten_gbe2_rx_valid_net: std_logic;
  signal top_ethout_ten_gbe2_tx_afull_net: std_logic;
  signal top_ethout_ten_gbe2_tx_data_net: std_logic_vector(63 downto 0);
  signal top_ethout_ten_gbe2_tx_dest_ip_net: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe2_tx_dest_port_net: std_logic_vector(15 downto 0);
  signal top_ethout_ten_gbe2_tx_end_of_frame_net: std_logic;
  signal top_ethout_ten_gbe2_tx_overflow_net: std_logic;
  signal top_ethout_ten_gbe2_tx_valid_net: std_logic;
  signal top_ethout_ten_gbe3_led_rx_net: std_logic;
  signal top_ethout_ten_gbe3_led_tx_net: std_logic;
  signal top_ethout_ten_gbe3_led_up_net: std_logic;
  signal top_ethout_ten_gbe3_rst_net: std_logic;
  signal top_ethout_ten_gbe3_rx_ack_net: std_logic;
  signal top_ethout_ten_gbe3_rx_bad_frame_net: std_logic;
  signal top_ethout_ten_gbe3_rx_data_net: std_logic_vector(63 downto 0);
  signal top_ethout_ten_gbe3_rx_end_of_frame_net: std_logic;
  signal top_ethout_ten_gbe3_rx_overrun_ack_net: std_logic;
  signal top_ethout_ten_gbe3_rx_overrun_net: std_logic;
  signal top_ethout_ten_gbe3_rx_source_ip_net: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe3_rx_source_port_net: std_logic_vector(15 downto 0);
  signal top_ethout_ten_gbe3_rx_valid_net: std_logic;
  signal top_ethout_ten_gbe3_tx_afull_net: std_logic;
  signal top_ethout_ten_gbe3_tx_data_net: std_logic_vector(63 downto 0);
  signal top_ethout_ten_gbe3_tx_dest_ip_net: std_logic_vector(31 downto 0);
  signal top_ethout_ten_gbe3_tx_dest_port_net: std_logic_vector(15 downto 0);
  signal top_ethout_ten_gbe3_tx_end_of_frame_net: std_logic;
  signal top_ethout_ten_gbe3_tx_overflow_net: std_logic;
  signal top_ethout_ten_gbe3_tx_valid_net: std_logic;
  signal top_scope_raw_0_snap_bram_addr_net: std_logic_vector(9 downto 0);
  signal top_scope_raw_0_snap_bram_data_in_net: std_logic_vector(127 downto 0);
  signal top_scope_raw_0_snap_bram_data_out_net: std_logic_vector(127 downto 0);
  signal top_scope_raw_0_snap_bram_we_net: std_logic;
  signal top_scope_raw_0_snap_ctrl_user_data_out_net: std_logic_vector(31 downto 0);
  signal top_scope_raw_0_snap_status_user_data_in_net: std_logic_vector(31 downto 0);

begin
  ce_1_sg_x42 <= ce_1;
  clk_1_sg_x42 <= clk_1;
  top_adc_adc_sync_net <= top_adc_adc_sync;
  top_adc_adc_user_data_i0_net <= top_adc_adc_user_data_i0;
  top_adc_adc_user_data_i1_net <= top_adc_adc_user_data_i1;
  top_adc_adc_user_data_i2_net <= top_adc_adc_user_data_i2;
  top_adc_adc_user_data_i3_net <= top_adc_adc_user_data_i3;
  top_adc_adc_user_data_i4_net <= top_adc_adc_user_data_i4;
  top_adc_adc_user_data_i5_net <= top_adc_adc_user_data_i5;
  top_adc_adc_user_data_i6_net <= top_adc_adc_user_data_i6;
  top_adc_adc_user_data_i7_net <= top_adc_adc_user_data_i7;
  top_adc_adc_user_data_q0_net <= top_adc_adc_user_data_q0;
  top_adc_adc_user_data_q1_net <= top_adc_adc_user_data_q1;
  top_adc_adc_user_data_q2_net <= top_adc_adc_user_data_q2;
  top_adc_adc_user_data_q3_net <= top_adc_adc_user_data_q3;
  top_adc_adc_user_data_q4_net <= top_adc_adc_user_data_q4;
  top_adc_adc_user_data_q5_net <= top_adc_adc_user_data_q5;
  top_adc_adc_user_data_q6_net <= top_adc_adc_user_data_q6;
  top_adc_adc_user_data_q7_net <= top_adc_adc_user_data_q7;
  top_adc_enable_user_data_out_net <= top_adc_enable_user_data_out;
  top_data_bypass_topin_data_out_net <= top_data_bypass_topin_data_out;
  top_data_bypass_topin_data_out_vld_net <= top_data_bypass_topin_data_out_vld;
  top_data_bypass_topout_data_out_net <= top_data_bypass_topout_data_out;
  top_data_bypass_topout_data_out_vld_net <= top_data_bypass_topout_data_out_vld;
  top_ethout_dest_ip0_user_data_out_net <= top_ethout_dest_ip0_user_data_out;
  top_ethout_dest_ip1_user_data_out_net <= top_ethout_dest_ip1_user_data_out;
  top_ethout_dest_ip2_user_data_out_net <= top_ethout_dest_ip2_user_data_out;
  top_ethout_dest_ip3_user_data_out_net <= top_ethout_dest_ip3_user_data_out;
  top_ethout_dest_port0_user_data_out_net <= top_ethout_dest_port0_user_data_out;
  top_ethout_dest_port1_user_data_out_net <= top_ethout_dest_port1_user_data_out;
  top_ethout_dest_port2_user_data_out_net <= top_ethout_dest_port2_user_data_out;
  top_ethout_dest_port3_user_data_out_net <= top_ethout_dest_port3_user_data_out;
  top_ethout_rst_user_data_out_net <= top_ethout_rst_user_data_out;
  top_ethout_ten_gbe0_led_rx_net <= top_ethout_ten_gbe0_led_rx;
  top_ethout_ten_gbe0_led_tx_net <= top_ethout_ten_gbe0_led_tx;
  top_ethout_ten_gbe0_led_up_net <= top_ethout_ten_gbe0_led_up;
  top_ethout_ten_gbe0_rx_bad_frame_net <= top_ethout_ten_gbe0_rx_bad_frame;
  top_ethout_ten_gbe0_rx_data_net <= top_ethout_ten_gbe0_rx_data;
  top_ethout_ten_gbe0_rx_end_of_frame_net <= top_ethout_ten_gbe0_rx_end_of_frame;
  top_ethout_ten_gbe0_rx_overrun_net <= top_ethout_ten_gbe0_rx_overrun;
  top_ethout_ten_gbe0_rx_source_ip_net <= top_ethout_ten_gbe0_rx_source_ip;
  top_ethout_ten_gbe0_rx_source_port_net <= top_ethout_ten_gbe0_rx_source_port;
  top_ethout_ten_gbe0_rx_valid_net <= top_ethout_ten_gbe0_rx_valid;
  top_ethout_ten_gbe0_tx_afull_net <= top_ethout_ten_gbe0_tx_afull;
  top_ethout_ten_gbe0_tx_overflow_net <= top_ethout_ten_gbe0_tx_overflow;
  top_ethout_ten_gbe1_led_rx_net <= top_ethout_ten_gbe1_led_rx;
  top_ethout_ten_gbe1_led_tx_net <= top_ethout_ten_gbe1_led_tx;
  top_ethout_ten_gbe1_led_up_net <= top_ethout_ten_gbe1_led_up;
  top_ethout_ten_gbe1_rx_bad_frame_net <= top_ethout_ten_gbe1_rx_bad_frame;
  top_ethout_ten_gbe1_rx_data_net <= top_ethout_ten_gbe1_rx_data;
  top_ethout_ten_gbe1_rx_end_of_frame_net <= top_ethout_ten_gbe1_rx_end_of_frame;
  top_ethout_ten_gbe1_rx_overrun_net <= top_ethout_ten_gbe1_rx_overrun;
  top_ethout_ten_gbe1_rx_source_ip_net <= top_ethout_ten_gbe1_rx_source_ip;
  top_ethout_ten_gbe1_rx_source_port_net <= top_ethout_ten_gbe1_rx_source_port;
  top_ethout_ten_gbe1_rx_valid_net <= top_ethout_ten_gbe1_rx_valid;
  top_ethout_ten_gbe1_tx_afull_net <= top_ethout_ten_gbe1_tx_afull;
  top_ethout_ten_gbe1_tx_overflow_net <= top_ethout_ten_gbe1_tx_overflow;
  top_ethout_ten_gbe2_led_rx_net <= top_ethout_ten_gbe2_led_rx;
  top_ethout_ten_gbe2_led_tx_net <= top_ethout_ten_gbe2_led_tx;
  top_ethout_ten_gbe2_led_up_net <= top_ethout_ten_gbe2_led_up;
  top_ethout_ten_gbe2_rx_bad_frame_net <= top_ethout_ten_gbe2_rx_bad_frame;
  top_ethout_ten_gbe2_rx_data_net <= top_ethout_ten_gbe2_rx_data;
  top_ethout_ten_gbe2_rx_end_of_frame_net <= top_ethout_ten_gbe2_rx_end_of_frame;
  top_ethout_ten_gbe2_rx_overrun_net <= top_ethout_ten_gbe2_rx_overrun;
  top_ethout_ten_gbe2_rx_source_ip_net <= top_ethout_ten_gbe2_rx_source_ip;
  top_ethout_ten_gbe2_rx_source_port_net <= top_ethout_ten_gbe2_rx_source_port;
  top_ethout_ten_gbe2_rx_valid_net <= top_ethout_ten_gbe2_rx_valid;
  top_ethout_ten_gbe2_tx_afull_net <= top_ethout_ten_gbe2_tx_afull;
  top_ethout_ten_gbe2_tx_overflow_net <= top_ethout_ten_gbe2_tx_overflow;
  top_ethout_ten_gbe3_led_rx_net <= top_ethout_ten_gbe3_led_rx;
  top_ethout_ten_gbe3_led_tx_net <= top_ethout_ten_gbe3_led_tx;
  top_ethout_ten_gbe3_led_up_net <= top_ethout_ten_gbe3_led_up;
  top_ethout_ten_gbe3_rx_bad_frame_net <= top_ethout_ten_gbe3_rx_bad_frame;
  top_ethout_ten_gbe3_rx_data_net <= top_ethout_ten_gbe3_rx_data;
  top_ethout_ten_gbe3_rx_end_of_frame_net <= top_ethout_ten_gbe3_rx_end_of_frame;
  top_ethout_ten_gbe3_rx_overrun_net <= top_ethout_ten_gbe3_rx_overrun;
  top_ethout_ten_gbe3_rx_source_ip_net <= top_ethout_ten_gbe3_rx_source_ip;
  top_ethout_ten_gbe3_rx_source_port_net <= top_ethout_ten_gbe3_rx_source_port;
  top_ethout_ten_gbe3_rx_valid_net <= top_ethout_ten_gbe3_rx_valid;
  top_ethout_ten_gbe3_tx_afull_net <= top_ethout_ten_gbe3_tx_afull;
  top_ethout_ten_gbe3_tx_overflow_net <= top_ethout_ten_gbe3_tx_overflow;
  top_scope_raw_0_snap_bram_data_out_net <= top_scope_raw_0_snap_bram_data_out;
  top_scope_raw_0_snap_ctrl_user_data_out_net <= top_scope_raw_0_snap_ctrl_user_data_out;
  top_data_bypass_topin_data_in <= top_data_bypass_topin_data_in_net;
  top_data_bypass_topin_en <= top_data_bypass_topin_en_net;
  top_data_bypass_topin_rst_n <= top_data_bypass_topin_rst_n_net;
  top_data_bypass_topout_data_in <= top_data_bypass_topout_data_in_net;
  top_data_bypass_topout_en <= top_data_bypass_topout_en_net;
  top_data_bypass_topout_rst_n <= top_data_bypass_topout_rst_n_net;
  top_ethout_ten_gbe0_rst <= top_ethout_ten_gbe0_rst_net;
  top_ethout_ten_gbe0_rx_ack <= top_ethout_ten_gbe0_rx_ack_net;
  top_ethout_ten_gbe0_rx_overrun_ack <= top_ethout_ten_gbe0_rx_overrun_ack_net;
  top_ethout_ten_gbe0_tx_data <= top_ethout_ten_gbe0_tx_data_net;
  top_ethout_ten_gbe0_tx_dest_ip <= top_ethout_ten_gbe0_tx_dest_ip_net;
  top_ethout_ten_gbe0_tx_dest_port <= top_ethout_ten_gbe0_tx_dest_port_net;
  top_ethout_ten_gbe0_tx_end_of_frame <= top_ethout_ten_gbe0_tx_end_of_frame_net;
  top_ethout_ten_gbe0_tx_valid <= top_ethout_ten_gbe0_tx_valid_net;
  top_ethout_ten_gbe1_rst <= top_ethout_ten_gbe1_rst_net;
  top_ethout_ten_gbe1_rx_ack <= top_ethout_ten_gbe1_rx_ack_net;
  top_ethout_ten_gbe1_rx_overrun_ack <= top_ethout_ten_gbe1_rx_overrun_ack_net;
  top_ethout_ten_gbe1_tx_data <= top_ethout_ten_gbe1_tx_data_net;
  top_ethout_ten_gbe1_tx_dest_ip <= top_ethout_ten_gbe1_tx_dest_ip_net;
  top_ethout_ten_gbe1_tx_dest_port <= top_ethout_ten_gbe1_tx_dest_port_net;
  top_ethout_ten_gbe1_tx_end_of_frame <= top_ethout_ten_gbe1_tx_end_of_frame_net;
  top_ethout_ten_gbe1_tx_valid <= top_ethout_ten_gbe1_tx_valid_net;
  top_ethout_ten_gbe2_rst <= top_ethout_ten_gbe2_rst_net;
  top_ethout_ten_gbe2_rx_ack <= top_ethout_ten_gbe2_rx_ack_net;
  top_ethout_ten_gbe2_rx_overrun_ack <= top_ethout_ten_gbe2_rx_overrun_ack_net;
  top_ethout_ten_gbe2_tx_data <= top_ethout_ten_gbe2_tx_data_net;
  top_ethout_ten_gbe2_tx_dest_ip <= top_ethout_ten_gbe2_tx_dest_ip_net;
  top_ethout_ten_gbe2_tx_dest_port <= top_ethout_ten_gbe2_tx_dest_port_net;
  top_ethout_ten_gbe2_tx_end_of_frame <= top_ethout_ten_gbe2_tx_end_of_frame_net;
  top_ethout_ten_gbe2_tx_valid <= top_ethout_ten_gbe2_tx_valid_net;
  top_ethout_ten_gbe3_rst <= top_ethout_ten_gbe3_rst_net;
  top_ethout_ten_gbe3_rx_ack <= top_ethout_ten_gbe3_rx_ack_net;
  top_ethout_ten_gbe3_rx_overrun_ack <= top_ethout_ten_gbe3_rx_overrun_ack_net;
  top_ethout_ten_gbe3_tx_data <= top_ethout_ten_gbe3_tx_data_net;
  top_ethout_ten_gbe3_tx_dest_ip <= top_ethout_ten_gbe3_tx_dest_ip_net;
  top_ethout_ten_gbe3_tx_dest_port <= top_ethout_ten_gbe3_tx_dest_port_net;
  top_ethout_ten_gbe3_tx_end_of_frame <= top_ethout_ten_gbe3_tx_end_of_frame_net;
  top_ethout_ten_gbe3_tx_valid <= top_ethout_ten_gbe3_tx_valid_net;
  top_scope_raw_0_snap_bram_addr <= top_scope_raw_0_snap_bram_addr_net;
  top_scope_raw_0_snap_bram_data_in <= top_scope_raw_0_snap_bram_data_in_net;
  top_scope_raw_0_snap_bram_we <= top_scope_raw_0_snap_bram_we_net;
  top_scope_raw_0_snap_status_user_data_in <= top_scope_raw_0_snap_status_user_data_in_net;

  adc_1abe01bdeb: entity work.adc_entity_1abe01bdeb
    port map (
      ce_1 => ce_1_sg_x42,
      clk_1 => clk_1_sg_x42,
      top_adc_adc_user_data_i0 => top_adc_adc_user_data_i0_net,
      top_adc_adc_user_data_i1 => top_adc_adc_user_data_i1_net,
      top_adc_adc_user_data_i2 => top_adc_adc_user_data_i2_net,
      top_adc_adc_user_data_i3 => top_adc_adc_user_data_i3_net,
      top_adc_adc_user_data_i4 => top_adc_adc_user_data_i4_net,
      top_adc_adc_user_data_i5 => top_adc_adc_user_data_i5_net,
      top_adc_adc_user_data_i6 => top_adc_adc_user_data_i6_net,
      top_adc_adc_user_data_i7 => top_adc_adc_user_data_i7_net,
      top_adc_adc_user_data_q0 => top_adc_adc_user_data_q0_net,
      top_adc_adc_user_data_q1 => top_adc_adc_user_data_q1_net,
      top_adc_adc_user_data_q2 => top_adc_adc_user_data_q2_net,
      top_adc_adc_user_data_q3 => top_adc_adc_user_data_q3_net,
      top_adc_adc_user_data_q4 => top_adc_adc_user_data_q4_net,
      top_adc_adc_user_data_q5 => top_adc_adc_user_data_q5_net,
      top_adc_adc_user_data_q6 => top_adc_adc_user_data_q6_net,
      top_adc_adc_user_data_q7 => top_adc_adc_user_data_q7_net,
      top_adc_enable_user_data_out => top_adc_enable_user_data_out_net,
      data => register2_q_net_x6,
      we => register_q_net_x1
    );

  data_bypass_topin_fb713132b6: entity work.data_bypass_topin_entity_fb713132b6
    port map (
      ce_1 => ce_1_sg_x42,
      clk_1 => clk_1_sg_x42,
      data_in => top_data_bypass_topout_data_out_net,
      en => top_data_bypass_topout_data_out_vld_net,
      rst_n => relational2_op_net_x2,
      convert1_x0 => top_data_bypass_topin_data_in_net,
      convert2_x0 => top_data_bypass_topin_en_net,
      convert3_x0 => top_data_bypass_topin_rst_n_net
    );

  data_bypass_topout_c49bb33b79: entity work.data_bypass_topout_entity_c49bb33b79
    port map (
      ce_1 => ce_1_sg_x42,
      clk_1 => clk_1_sg_x42,
      data_in => register2_q_net_x6,
      en => register_q_net_x1,
      rst_n => relational2_op_net_x2,
      convert1_x0 => top_data_bypass_topout_data_in_net,
      convert2_x0 => top_data_bypass_topout_en_net,
      convert3_x0 => top_data_bypass_topout_rst_n_net
    );

  ethout_ef12fc8111: entity work.ethout_entity_ef12fc8111
    port map (
      ce_1 => ce_1_sg_x42,
      clk_1 => clk_1_sg_x42,
      dout0 => asdf1_y_net_x3,
      dout1 => asdf2_y_net_x3,
      eof0 => switch_eof01_net_x3,
      eof2 => switch_eof23_net_x3,
      top_ethout_dest_ip0_user_data_out => top_ethout_dest_ip0_user_data_out_net,
      top_ethout_dest_ip1_user_data_out => top_ethout_dest_ip1_user_data_out_net,
      top_ethout_dest_ip2_user_data_out => top_ethout_dest_ip2_user_data_out_net,
      top_ethout_dest_ip3_user_data_out => top_ethout_dest_ip3_user_data_out_net,
      top_ethout_dest_port0_user_data_out => top_ethout_dest_port0_user_data_out_net,
      top_ethout_dest_port1_user_data_out => top_ethout_dest_port1_user_data_out_net,
      top_ethout_dest_port2_user_data_out => top_ethout_dest_port2_user_data_out_net,
      top_ethout_dest_port3_user_data_out => top_ethout_dest_port3_user_data_out_net,
      top_ethout_rst_user_data_out => top_ethout_rst_user_data_out_net,
      top_ethout_ten_gbe0_rx_overrun => top_ethout_ten_gbe0_rx_overrun_net,
      top_ethout_ten_gbe1_rx_overrun => top_ethout_ten_gbe1_rx_overrun_net,
      top_ethout_ten_gbe2_rx_overrun => top_ethout_ten_gbe2_rx_overrun_net,
      top_ethout_ten_gbe3_rx_overrun => top_ethout_ten_gbe3_rx_overrun_net,
      valid0 => switch_valid01_net_x3,
      valid2 => switch_valid23_net_x3,
      ten_gbe0 => top_ethout_ten_gbe0_rst_net,
      ten_gbe0_x0 => top_ethout_ten_gbe0_rx_ack_net,
      ten_gbe0_x1 => top_ethout_ten_gbe0_rx_overrun_ack_net,
      ten_gbe0_x2 => top_ethout_ten_gbe0_tx_data_net,
      ten_gbe0_x3 => top_ethout_ten_gbe0_tx_dest_ip_net,
      ten_gbe0_x4 => top_ethout_ten_gbe0_tx_end_of_frame_net,
      ten_gbe0_x5 => top_ethout_ten_gbe0_tx_dest_port_net,
      ten_gbe0_x6 => top_ethout_ten_gbe0_tx_valid_net,
      ten_gbe1 => top_ethout_ten_gbe1_rst_net,
      ten_gbe1_x0 => top_ethout_ten_gbe1_rx_ack_net,
      ten_gbe1_x1 => top_ethout_ten_gbe1_rx_overrun_ack_net,
      ten_gbe1_x2 => top_ethout_ten_gbe1_tx_data_net,
      ten_gbe1_x3 => top_ethout_ten_gbe1_tx_dest_ip_net,
      ten_gbe1_x4 => top_ethout_ten_gbe1_tx_end_of_frame_net,
      ten_gbe1_x5 => top_ethout_ten_gbe1_tx_dest_port_net,
      ten_gbe1_x6 => top_ethout_ten_gbe1_tx_valid_net,
      ten_gbe2 => top_ethout_ten_gbe2_rst_net,
      ten_gbe2_x0 => top_ethout_ten_gbe2_rx_ack_net,
      ten_gbe2_x1 => top_ethout_ten_gbe2_rx_overrun_ack_net,
      ten_gbe2_x2 => top_ethout_ten_gbe2_tx_data_net,
      ten_gbe2_x3 => top_ethout_ten_gbe2_tx_dest_ip_net,
      ten_gbe2_x4 => top_ethout_ten_gbe2_tx_end_of_frame_net,
      ten_gbe2_x5 => top_ethout_ten_gbe2_tx_dest_port_net,
      ten_gbe2_x6 => top_ethout_ten_gbe2_tx_valid_net,
      ten_gbe3 => top_ethout_ten_gbe3_rst_net,
      ten_gbe3_x0 => top_ethout_ten_gbe3_rx_ack_net,
      ten_gbe3_x1 => top_ethout_ten_gbe3_rx_overrun_ack_net,
      ten_gbe3_x2 => top_ethout_ten_gbe3_tx_data_net,
      ten_gbe3_x3 => top_ethout_ten_gbe3_tx_dest_ip_net,
      ten_gbe3_x4 => top_ethout_ten_gbe3_tx_end_of_frame_net,
      ten_gbe3_x5 => top_ethout_ten_gbe3_tx_dest_port_net,
      ten_gbe3_x6 => top_ethout_ten_gbe3_tx_valid_net
    );

  reset_7f945ff177: entity work.reset_entity_7f945ff177
    port map (
      ce_1 => ce_1_sg_x42,
      clk_1 => clk_1_sg_x42,
      reset => relational2_op_net_x2
    );

  scope_raw_0_9c55b9e8c3: entity work.scope_raw_0_entity_9c55b9e8c3
    port map (
      ce_1 => ce_1_sg_x42,
      clk_1 => clk_1_sg_x42,
      raw_0 => register2_q_net_x6,
      top_scope_raw_0_snap_ctrl_user_data_out => top_scope_raw_0_snap_ctrl_user_data_out_net,
      snap => top_scope_raw_0_snap_bram_addr_net,
      snap_x0 => top_scope_raw_0_snap_bram_data_in_net,
      snap_x1 => top_scope_raw_0_snap_bram_we_net,
      snap_x2 => top_scope_raw_0_snap_status_user_data_in_net
    );

  switch_e404048cbb: entity work.switch_entity_e404048cbb
    port map (
      ce_1 => ce_1_sg_x42,
      clk_1 => clk_1_sg_x42,
      det => top_data_bypass_topin_data_out_vld_net,
      din => top_data_bypass_topin_data_out_net,
      dout1 => asdf2_y_net_x3,
      dout2 => asdf1_y_net_x3,
      eof0 => switch_eof01_net_x3,
      eof2 => switch_eof23_net_x3,
      valid0 => switch_valid01_net_x3,
      valid2 => switch_valid23_net_x3
    );

end structural;
