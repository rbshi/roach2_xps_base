-- If you see error messages concerning missing libraries for
-- XilinxCoreLib, unisims, or simprims, you may not have set
-- up your ModelSim environment correctly.  See the Xilinx
-- Support Website for instructions telling how to compile
-- these libraries.

vlib work

vlog /data/tools/Xilinx/14.7/ISE_DS/ISE/verilog/src/glbl.v
vlog  switch.v
vcom  -nowarn 1 top.vhd
vcom  -nowarn 1 top_cw.vhd
