----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Design Name: 
-- Module Name: 
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity top_cw_stub is
    port (
      ce: in std_logic := '1'; 
      clk: in std_logic; -- clock period = 4.0 ns (250.0 Mhz)
      top_adc_adc_sync: in std_logic; 
      top_adc_adc_user_data_i0: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i1: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i2: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i3: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i4: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i5: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i6: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i7: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q0: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q1: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q2: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q3: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q4: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q5: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q6: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q7: in std_logic_vector(7 downto 0); 
      top_adc_enable_user_data_out: in std_logic_vector(31 downto 0); 
      top_data_bypass_topin_data_out: in std_logic_vector(127 downto 0); 
      top_data_bypass_topin_data_out_vld: in std_logic; 
      top_data_bypass_topout_data_out: in std_logic_vector(127 downto 0); 
      top_data_bypass_topout_data_out_vld: in std_logic; 
      top_ethout_dest_ip0_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_ip1_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_ip2_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_ip3_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_port0_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_port1_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_port2_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_port3_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_rst_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe0_led_rx: in std_logic; 
      top_ethout_ten_gbe0_led_tx: in std_logic; 
      top_ethout_ten_gbe0_led_up: in std_logic; 
      top_ethout_ten_gbe0_rx_bad_frame: in std_logic; 
      top_ethout_ten_gbe0_rx_data: in std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe0_rx_end_of_frame: in std_logic; 
      top_ethout_ten_gbe0_rx_overrun: in std_logic; 
      top_ethout_ten_gbe0_rx_source_ip: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe0_rx_source_port: in std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe0_rx_valid: in std_logic; 
      top_ethout_ten_gbe0_tx_afull: in std_logic; 
      top_ethout_ten_gbe0_tx_overflow: in std_logic; 
      top_ethout_ten_gbe1_led_rx: in std_logic; 
      top_ethout_ten_gbe1_led_tx: in std_logic; 
      top_ethout_ten_gbe1_led_up: in std_logic; 
      top_ethout_ten_gbe1_rx_bad_frame: in std_logic; 
      top_ethout_ten_gbe1_rx_data: in std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe1_rx_end_of_frame: in std_logic; 
      top_ethout_ten_gbe1_rx_overrun: in std_logic; 
      top_ethout_ten_gbe1_rx_source_ip: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe1_rx_source_port: in std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe1_rx_valid: in std_logic; 
      top_ethout_ten_gbe1_tx_afull: in std_logic; 
      top_ethout_ten_gbe1_tx_overflow: in std_logic; 
      top_ethout_ten_gbe2_led_rx: in std_logic; 
      top_ethout_ten_gbe2_led_tx: in std_logic; 
      top_ethout_ten_gbe2_led_up: in std_logic; 
      top_ethout_ten_gbe2_rx_bad_frame: in std_logic; 
      top_ethout_ten_gbe2_rx_data: in std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe2_rx_end_of_frame: in std_logic; 
      top_ethout_ten_gbe2_rx_overrun: in std_logic; 
      top_ethout_ten_gbe2_rx_source_ip: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe2_rx_source_port: in std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe2_rx_valid: in std_logic; 
      top_ethout_ten_gbe2_tx_afull: in std_logic; 
      top_ethout_ten_gbe2_tx_overflow: in std_logic; 
      top_ethout_ten_gbe3_led_rx: in std_logic; 
      top_ethout_ten_gbe3_led_tx: in std_logic; 
      top_ethout_ten_gbe3_led_up: in std_logic; 
      top_ethout_ten_gbe3_rx_bad_frame: in std_logic; 
      top_ethout_ten_gbe3_rx_data: in std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe3_rx_end_of_frame: in std_logic; 
      top_ethout_ten_gbe3_rx_overrun: in std_logic; 
      top_ethout_ten_gbe3_rx_source_ip: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe3_rx_source_port: in std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe3_rx_valid: in std_logic; 
      top_ethout_ten_gbe3_tx_afull: in std_logic; 
      top_ethout_ten_gbe3_tx_overflow: in std_logic; 
      top_scope_raw_0_snap_bram_data_out: in std_logic_vector(127 downto 0); 
      top_scope_raw_0_snap_ctrl_user_data_out: in std_logic_vector(31 downto 0); 
      top_data_bypass_topin_data_in: out std_logic_vector(127 downto 0); 
      top_data_bypass_topin_en: out std_logic; 
      top_data_bypass_topin_rst_n: out std_logic; 
      top_data_bypass_topout_data_in: out std_logic_vector(127 downto 0); 
      top_data_bypass_topout_en: out std_logic; 
      top_data_bypass_topout_rst_n: out std_logic; 
      top_ethout_ten_gbe0_rst: out std_logic; 
      top_ethout_ten_gbe0_rx_ack: out std_logic; 
      top_ethout_ten_gbe0_rx_overrun_ack: out std_logic; 
      top_ethout_ten_gbe0_tx_data: out std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe0_tx_dest_ip: out std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe0_tx_dest_port: out std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe0_tx_end_of_frame: out std_logic; 
      top_ethout_ten_gbe0_tx_valid: out std_logic; 
      top_ethout_ten_gbe1_rst: out std_logic; 
      top_ethout_ten_gbe1_rx_ack: out std_logic; 
      top_ethout_ten_gbe1_rx_overrun_ack: out std_logic; 
      top_ethout_ten_gbe1_tx_data: out std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe1_tx_dest_ip: out std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe1_tx_dest_port: out std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe1_tx_end_of_frame: out std_logic; 
      top_ethout_ten_gbe1_tx_valid: out std_logic; 
      top_ethout_ten_gbe2_rst: out std_logic; 
      top_ethout_ten_gbe2_rx_ack: out std_logic; 
      top_ethout_ten_gbe2_rx_overrun_ack: out std_logic; 
      top_ethout_ten_gbe2_tx_data: out std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe2_tx_dest_ip: out std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe2_tx_dest_port: out std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe2_tx_end_of_frame: out std_logic; 
      top_ethout_ten_gbe2_tx_valid: out std_logic; 
      top_ethout_ten_gbe3_rst: out std_logic; 
      top_ethout_ten_gbe3_rx_ack: out std_logic; 
      top_ethout_ten_gbe3_rx_overrun_ack: out std_logic; 
      top_ethout_ten_gbe3_tx_data: out std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe3_tx_dest_ip: out std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe3_tx_dest_port: out std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe3_tx_end_of_frame: out std_logic; 
      top_ethout_ten_gbe3_tx_valid: out std_logic; 
      top_scope_raw_0_snap_bram_addr: out std_logic_vector(9 downto 0); 
      top_scope_raw_0_snap_bram_data_in: out std_logic_vector(127 downto 0); 
      top_scope_raw_0_snap_bram_we: out std_logic; 
      top_scope_raw_0_snap_status_user_data_in: out std_logic_vector(31 downto 0)
    );
end top_cw_stub;

architecture Behavioral of top_cw_stub is

  component top_cw
    port (
      ce: in std_logic := '1'; 
      clk: in std_logic; -- clock period = 4.0 ns (250.0 Mhz)
      top_adc_adc_sync: in std_logic; 
      top_adc_adc_user_data_i0: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i1: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i2: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i3: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i4: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i5: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i6: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_i7: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q0: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q1: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q2: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q3: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q4: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q5: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q6: in std_logic_vector(7 downto 0); 
      top_adc_adc_user_data_q7: in std_logic_vector(7 downto 0); 
      top_adc_enable_user_data_out: in std_logic_vector(31 downto 0); 
      top_data_bypass_topin_data_out: in std_logic_vector(127 downto 0); 
      top_data_bypass_topin_data_out_vld: in std_logic; 
      top_data_bypass_topout_data_out: in std_logic_vector(127 downto 0); 
      top_data_bypass_topout_data_out_vld: in std_logic; 
      top_ethout_dest_ip0_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_ip1_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_ip2_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_ip3_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_port0_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_port1_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_port2_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_dest_port3_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_rst_user_data_out: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe0_led_rx: in std_logic; 
      top_ethout_ten_gbe0_led_tx: in std_logic; 
      top_ethout_ten_gbe0_led_up: in std_logic; 
      top_ethout_ten_gbe0_rx_bad_frame: in std_logic; 
      top_ethout_ten_gbe0_rx_data: in std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe0_rx_end_of_frame: in std_logic; 
      top_ethout_ten_gbe0_rx_overrun: in std_logic; 
      top_ethout_ten_gbe0_rx_source_ip: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe0_rx_source_port: in std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe0_rx_valid: in std_logic; 
      top_ethout_ten_gbe0_tx_afull: in std_logic; 
      top_ethout_ten_gbe0_tx_overflow: in std_logic; 
      top_ethout_ten_gbe1_led_rx: in std_logic; 
      top_ethout_ten_gbe1_led_tx: in std_logic; 
      top_ethout_ten_gbe1_led_up: in std_logic; 
      top_ethout_ten_gbe1_rx_bad_frame: in std_logic; 
      top_ethout_ten_gbe1_rx_data: in std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe1_rx_end_of_frame: in std_logic; 
      top_ethout_ten_gbe1_rx_overrun: in std_logic; 
      top_ethout_ten_gbe1_rx_source_ip: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe1_rx_source_port: in std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe1_rx_valid: in std_logic; 
      top_ethout_ten_gbe1_tx_afull: in std_logic; 
      top_ethout_ten_gbe1_tx_overflow: in std_logic; 
      top_ethout_ten_gbe2_led_rx: in std_logic; 
      top_ethout_ten_gbe2_led_tx: in std_logic; 
      top_ethout_ten_gbe2_led_up: in std_logic; 
      top_ethout_ten_gbe2_rx_bad_frame: in std_logic; 
      top_ethout_ten_gbe2_rx_data: in std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe2_rx_end_of_frame: in std_logic; 
      top_ethout_ten_gbe2_rx_overrun: in std_logic; 
      top_ethout_ten_gbe2_rx_source_ip: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe2_rx_source_port: in std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe2_rx_valid: in std_logic; 
      top_ethout_ten_gbe2_tx_afull: in std_logic; 
      top_ethout_ten_gbe2_tx_overflow: in std_logic; 
      top_ethout_ten_gbe3_led_rx: in std_logic; 
      top_ethout_ten_gbe3_led_tx: in std_logic; 
      top_ethout_ten_gbe3_led_up: in std_logic; 
      top_ethout_ten_gbe3_rx_bad_frame: in std_logic; 
      top_ethout_ten_gbe3_rx_data: in std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe3_rx_end_of_frame: in std_logic; 
      top_ethout_ten_gbe3_rx_overrun: in std_logic; 
      top_ethout_ten_gbe3_rx_source_ip: in std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe3_rx_source_port: in std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe3_rx_valid: in std_logic; 
      top_ethout_ten_gbe3_tx_afull: in std_logic; 
      top_ethout_ten_gbe3_tx_overflow: in std_logic; 
      top_scope_raw_0_snap_bram_data_out: in std_logic_vector(127 downto 0); 
      top_scope_raw_0_snap_ctrl_user_data_out: in std_logic_vector(31 downto 0); 
      top_data_bypass_topin_data_in: out std_logic_vector(127 downto 0); 
      top_data_bypass_topin_en: out std_logic; 
      top_data_bypass_topin_rst_n: out std_logic; 
      top_data_bypass_topout_data_in: out std_logic_vector(127 downto 0); 
      top_data_bypass_topout_en: out std_logic; 
      top_data_bypass_topout_rst_n: out std_logic; 
      top_ethout_ten_gbe0_rst: out std_logic; 
      top_ethout_ten_gbe0_rx_ack: out std_logic; 
      top_ethout_ten_gbe0_rx_overrun_ack: out std_logic; 
      top_ethout_ten_gbe0_tx_data: out std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe0_tx_dest_ip: out std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe0_tx_dest_port: out std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe0_tx_end_of_frame: out std_logic; 
      top_ethout_ten_gbe0_tx_valid: out std_logic; 
      top_ethout_ten_gbe1_rst: out std_logic; 
      top_ethout_ten_gbe1_rx_ack: out std_logic; 
      top_ethout_ten_gbe1_rx_overrun_ack: out std_logic; 
      top_ethout_ten_gbe1_tx_data: out std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe1_tx_dest_ip: out std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe1_tx_dest_port: out std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe1_tx_end_of_frame: out std_logic; 
      top_ethout_ten_gbe1_tx_valid: out std_logic; 
      top_ethout_ten_gbe2_rst: out std_logic; 
      top_ethout_ten_gbe2_rx_ack: out std_logic; 
      top_ethout_ten_gbe2_rx_overrun_ack: out std_logic; 
      top_ethout_ten_gbe2_tx_data: out std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe2_tx_dest_ip: out std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe2_tx_dest_port: out std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe2_tx_end_of_frame: out std_logic; 
      top_ethout_ten_gbe2_tx_valid: out std_logic; 
      top_ethout_ten_gbe3_rst: out std_logic; 
      top_ethout_ten_gbe3_rx_ack: out std_logic; 
      top_ethout_ten_gbe3_rx_overrun_ack: out std_logic; 
      top_ethout_ten_gbe3_tx_data: out std_logic_vector(63 downto 0); 
      top_ethout_ten_gbe3_tx_dest_ip: out std_logic_vector(31 downto 0); 
      top_ethout_ten_gbe3_tx_dest_port: out std_logic_vector(15 downto 0); 
      top_ethout_ten_gbe3_tx_end_of_frame: out std_logic; 
      top_ethout_ten_gbe3_tx_valid: out std_logic; 
      top_scope_raw_0_snap_bram_addr: out std_logic_vector(9 downto 0); 
      top_scope_raw_0_snap_bram_data_in: out std_logic_vector(127 downto 0); 
      top_scope_raw_0_snap_bram_we: out std_logic; 
      top_scope_raw_0_snap_status_user_data_in: out std_logic_vector(31 downto 0)
    );
  end component;
begin

top_cw_i : top_cw
  port map (
    ce => ce,
    clk => clk,
    top_adc_adc_sync => top_adc_adc_sync,
    top_adc_adc_user_data_i0 => top_adc_adc_user_data_i0,
    top_adc_adc_user_data_i1 => top_adc_adc_user_data_i1,
    top_adc_adc_user_data_i2 => top_adc_adc_user_data_i2,
    top_adc_adc_user_data_i3 => top_adc_adc_user_data_i3,
    top_adc_adc_user_data_i4 => top_adc_adc_user_data_i4,
    top_adc_adc_user_data_i5 => top_adc_adc_user_data_i5,
    top_adc_adc_user_data_i6 => top_adc_adc_user_data_i6,
    top_adc_adc_user_data_i7 => top_adc_adc_user_data_i7,
    top_adc_adc_user_data_q0 => top_adc_adc_user_data_q0,
    top_adc_adc_user_data_q1 => top_adc_adc_user_data_q1,
    top_adc_adc_user_data_q2 => top_adc_adc_user_data_q2,
    top_adc_adc_user_data_q3 => top_adc_adc_user_data_q3,
    top_adc_adc_user_data_q4 => top_adc_adc_user_data_q4,
    top_adc_adc_user_data_q5 => top_adc_adc_user_data_q5,
    top_adc_adc_user_data_q6 => top_adc_adc_user_data_q6,
    top_adc_adc_user_data_q7 => top_adc_adc_user_data_q7,
    top_adc_enable_user_data_out => top_adc_enable_user_data_out,
    top_data_bypass_topin_data_out => top_data_bypass_topin_data_out,
    top_data_bypass_topin_data_out_vld => top_data_bypass_topin_data_out_vld,
    top_data_bypass_topout_data_out => top_data_bypass_topout_data_out,
    top_data_bypass_topout_data_out_vld => top_data_bypass_topout_data_out_vld,
    top_ethout_dest_ip0_user_data_out => top_ethout_dest_ip0_user_data_out,
    top_ethout_dest_ip1_user_data_out => top_ethout_dest_ip1_user_data_out,
    top_ethout_dest_ip2_user_data_out => top_ethout_dest_ip2_user_data_out,
    top_ethout_dest_ip3_user_data_out => top_ethout_dest_ip3_user_data_out,
    top_ethout_dest_port0_user_data_out => top_ethout_dest_port0_user_data_out,
    top_ethout_dest_port1_user_data_out => top_ethout_dest_port1_user_data_out,
    top_ethout_dest_port2_user_data_out => top_ethout_dest_port2_user_data_out,
    top_ethout_dest_port3_user_data_out => top_ethout_dest_port3_user_data_out,
    top_ethout_rst_user_data_out => top_ethout_rst_user_data_out,
    top_ethout_ten_gbe0_led_rx => top_ethout_ten_gbe0_led_rx,
    top_ethout_ten_gbe0_led_tx => top_ethout_ten_gbe0_led_tx,
    top_ethout_ten_gbe0_led_up => top_ethout_ten_gbe0_led_up,
    top_ethout_ten_gbe0_rx_bad_frame => top_ethout_ten_gbe0_rx_bad_frame,
    top_ethout_ten_gbe0_rx_data => top_ethout_ten_gbe0_rx_data,
    top_ethout_ten_gbe0_rx_end_of_frame => top_ethout_ten_gbe0_rx_end_of_frame,
    top_ethout_ten_gbe0_rx_overrun => top_ethout_ten_gbe0_rx_overrun,
    top_ethout_ten_gbe0_rx_source_ip => top_ethout_ten_gbe0_rx_source_ip,
    top_ethout_ten_gbe0_rx_source_port => top_ethout_ten_gbe0_rx_source_port,
    top_ethout_ten_gbe0_rx_valid => top_ethout_ten_gbe0_rx_valid,
    top_ethout_ten_gbe0_tx_afull => top_ethout_ten_gbe0_tx_afull,
    top_ethout_ten_gbe0_tx_overflow => top_ethout_ten_gbe0_tx_overflow,
    top_ethout_ten_gbe1_led_rx => top_ethout_ten_gbe1_led_rx,
    top_ethout_ten_gbe1_led_tx => top_ethout_ten_gbe1_led_tx,
    top_ethout_ten_gbe1_led_up => top_ethout_ten_gbe1_led_up,
    top_ethout_ten_gbe1_rx_bad_frame => top_ethout_ten_gbe1_rx_bad_frame,
    top_ethout_ten_gbe1_rx_data => top_ethout_ten_gbe1_rx_data,
    top_ethout_ten_gbe1_rx_end_of_frame => top_ethout_ten_gbe1_rx_end_of_frame,
    top_ethout_ten_gbe1_rx_overrun => top_ethout_ten_gbe1_rx_overrun,
    top_ethout_ten_gbe1_rx_source_ip => top_ethout_ten_gbe1_rx_source_ip,
    top_ethout_ten_gbe1_rx_source_port => top_ethout_ten_gbe1_rx_source_port,
    top_ethout_ten_gbe1_rx_valid => top_ethout_ten_gbe1_rx_valid,
    top_ethout_ten_gbe1_tx_afull => top_ethout_ten_gbe1_tx_afull,
    top_ethout_ten_gbe1_tx_overflow => top_ethout_ten_gbe1_tx_overflow,
    top_ethout_ten_gbe2_led_rx => top_ethout_ten_gbe2_led_rx,
    top_ethout_ten_gbe2_led_tx => top_ethout_ten_gbe2_led_tx,
    top_ethout_ten_gbe2_led_up => top_ethout_ten_gbe2_led_up,
    top_ethout_ten_gbe2_rx_bad_frame => top_ethout_ten_gbe2_rx_bad_frame,
    top_ethout_ten_gbe2_rx_data => top_ethout_ten_gbe2_rx_data,
    top_ethout_ten_gbe2_rx_end_of_frame => top_ethout_ten_gbe2_rx_end_of_frame,
    top_ethout_ten_gbe2_rx_overrun => top_ethout_ten_gbe2_rx_overrun,
    top_ethout_ten_gbe2_rx_source_ip => top_ethout_ten_gbe2_rx_source_ip,
    top_ethout_ten_gbe2_rx_source_port => top_ethout_ten_gbe2_rx_source_port,
    top_ethout_ten_gbe2_rx_valid => top_ethout_ten_gbe2_rx_valid,
    top_ethout_ten_gbe2_tx_afull => top_ethout_ten_gbe2_tx_afull,
    top_ethout_ten_gbe2_tx_overflow => top_ethout_ten_gbe2_tx_overflow,
    top_ethout_ten_gbe3_led_rx => top_ethout_ten_gbe3_led_rx,
    top_ethout_ten_gbe3_led_tx => top_ethout_ten_gbe3_led_tx,
    top_ethout_ten_gbe3_led_up => top_ethout_ten_gbe3_led_up,
    top_ethout_ten_gbe3_rx_bad_frame => top_ethout_ten_gbe3_rx_bad_frame,
    top_ethout_ten_gbe3_rx_data => top_ethout_ten_gbe3_rx_data,
    top_ethout_ten_gbe3_rx_end_of_frame => top_ethout_ten_gbe3_rx_end_of_frame,
    top_ethout_ten_gbe3_rx_overrun => top_ethout_ten_gbe3_rx_overrun,
    top_ethout_ten_gbe3_rx_source_ip => top_ethout_ten_gbe3_rx_source_ip,
    top_ethout_ten_gbe3_rx_source_port => top_ethout_ten_gbe3_rx_source_port,
    top_ethout_ten_gbe3_rx_valid => top_ethout_ten_gbe3_rx_valid,
    top_ethout_ten_gbe3_tx_afull => top_ethout_ten_gbe3_tx_afull,
    top_ethout_ten_gbe3_tx_overflow => top_ethout_ten_gbe3_tx_overflow,
    top_scope_raw_0_snap_bram_data_out => top_scope_raw_0_snap_bram_data_out,
    top_scope_raw_0_snap_ctrl_user_data_out => top_scope_raw_0_snap_ctrl_user_data_out,
    top_data_bypass_topin_data_in => top_data_bypass_topin_data_in,
    top_data_bypass_topin_en => top_data_bypass_topin_en,
    top_data_bypass_topin_rst_n => top_data_bypass_topin_rst_n,
    top_data_bypass_topout_data_in => top_data_bypass_topout_data_in,
    top_data_bypass_topout_en => top_data_bypass_topout_en,
    top_data_bypass_topout_rst_n => top_data_bypass_topout_rst_n,
    top_ethout_ten_gbe0_rst => top_ethout_ten_gbe0_rst,
    top_ethout_ten_gbe0_rx_ack => top_ethout_ten_gbe0_rx_ack,
    top_ethout_ten_gbe0_rx_overrun_ack => top_ethout_ten_gbe0_rx_overrun_ack,
    top_ethout_ten_gbe0_tx_data => top_ethout_ten_gbe0_tx_data,
    top_ethout_ten_gbe0_tx_dest_ip => top_ethout_ten_gbe0_tx_dest_ip,
    top_ethout_ten_gbe0_tx_dest_port => top_ethout_ten_gbe0_tx_dest_port,
    top_ethout_ten_gbe0_tx_end_of_frame => top_ethout_ten_gbe0_tx_end_of_frame,
    top_ethout_ten_gbe0_tx_valid => top_ethout_ten_gbe0_tx_valid,
    top_ethout_ten_gbe1_rst => top_ethout_ten_gbe1_rst,
    top_ethout_ten_gbe1_rx_ack => top_ethout_ten_gbe1_rx_ack,
    top_ethout_ten_gbe1_rx_overrun_ack => top_ethout_ten_gbe1_rx_overrun_ack,
    top_ethout_ten_gbe1_tx_data => top_ethout_ten_gbe1_tx_data,
    top_ethout_ten_gbe1_tx_dest_ip => top_ethout_ten_gbe1_tx_dest_ip,
    top_ethout_ten_gbe1_tx_dest_port => top_ethout_ten_gbe1_tx_dest_port,
    top_ethout_ten_gbe1_tx_end_of_frame => top_ethout_ten_gbe1_tx_end_of_frame,
    top_ethout_ten_gbe1_tx_valid => top_ethout_ten_gbe1_tx_valid,
    top_ethout_ten_gbe2_rst => top_ethout_ten_gbe2_rst,
    top_ethout_ten_gbe2_rx_ack => top_ethout_ten_gbe2_rx_ack,
    top_ethout_ten_gbe2_rx_overrun_ack => top_ethout_ten_gbe2_rx_overrun_ack,
    top_ethout_ten_gbe2_tx_data => top_ethout_ten_gbe2_tx_data,
    top_ethout_ten_gbe2_tx_dest_ip => top_ethout_ten_gbe2_tx_dest_ip,
    top_ethout_ten_gbe2_tx_dest_port => top_ethout_ten_gbe2_tx_dest_port,
    top_ethout_ten_gbe2_tx_end_of_frame => top_ethout_ten_gbe2_tx_end_of_frame,
    top_ethout_ten_gbe2_tx_valid => top_ethout_ten_gbe2_tx_valid,
    top_ethout_ten_gbe3_rst => top_ethout_ten_gbe3_rst,
    top_ethout_ten_gbe3_rx_ack => top_ethout_ten_gbe3_rx_ack,
    top_ethout_ten_gbe3_rx_overrun_ack => top_ethout_ten_gbe3_rx_overrun_ack,
    top_ethout_ten_gbe3_tx_data => top_ethout_ten_gbe3_tx_data,
    top_ethout_ten_gbe3_tx_dest_ip => top_ethout_ten_gbe3_tx_dest_ip,
    top_ethout_ten_gbe3_tx_dest_port => top_ethout_ten_gbe3_tx_dest_port,
    top_ethout_ten_gbe3_tx_end_of_frame => top_ethout_ten_gbe3_tx_end_of_frame,
    top_ethout_ten_gbe3_tx_valid => top_ethout_ten_gbe3_tx_valid,
    top_scope_raw_0_snap_bram_addr => top_scope_raw_0_snap_bram_addr,
    top_scope_raw_0_snap_bram_data_in => top_scope_raw_0_snap_bram_data_in,
    top_scope_raw_0_snap_bram_we => top_scope_raw_0_snap_bram_we,
    top_scope_raw_0_snap_status_user_data_in => top_scope_raw_0_snap_status_user_data_in);
end Behavioral;

