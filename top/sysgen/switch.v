//This module control the send logic of 4 10Ge ports
module switch(
    input clk,
    input ce,
    input det,
    input reset,
    output reg valid01,
    output reg valid23,
    output reg eof01,
    output reg eof23
);

localparam eth01=0,eth23=1;
reg state;
reg [9:0] blkcnt;


always @(posedge clk or negedge reset) begin
    if (~reset) begin
        valid01<=0;
        valid23<=0;
        eof01<=0;
        eof23<=0;
        blkcnt<=0;
        state<=eth01;
    end
    else begin
        case (state)
            eth01:begin
                eof23<=0;
                valid23<=0;
                if(det==1)begin
                    valid01<=1;
                    if(blkcnt==1023)begin
                        eof01<=1;
                        state<=eth23;
                    end
                    blkcnt<=blkcnt+1;
                end
                else begin
                    valid01<=0;
                end
            end
            eth23:begin
                eof01<=0;
                valid01<=0;
                if(det==1)begin
                    valid23<=1;
                    if(blkcnt==1023) begin
                        eof23<=1;
                        state<=eth01;
                    end
                    blkcnt<=blkcnt+1;
                end
                else begin
                    valid23<=0;
                end
            end
        endcase
    end
end
endmodule
