@ECHO OFF
@REM ###########################################
@REM # Script file to run the flow 
@REM # 
@REM ###########################################
@REM #
@REM # Command line for ngdbuild
@REM #
ngdbuild -p xc6vsx475tff1759-1 -nt timestamp -sd ../pcores -uc system.ucf "C:/FPGA_Projects/roach2_xps_base/top/XPS_ROACH2_base/implementation/system.ngc" system.ngd 

@REM #
@REM # Command line for map
@REM #
map -timing -detail -ol high -xe n -mt 2 -register_duplication -o system_map.ncd -w -pr b system.ngd system.pcf 

@REM #
@REM # Command line for par
@REM #
par -w -mt 4 system_map.ncd system.ncd system.pcf 

@REM #
@REM # Command line for post_par_trce
@REM #
trce -e 200 -xml system.twx system.ncd system.pcf 

