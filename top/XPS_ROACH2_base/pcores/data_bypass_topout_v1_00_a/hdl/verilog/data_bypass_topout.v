//-----------------------------------------------------------------------------
// Title         : Data Bypass Module
// Project       : ROACH2
//-----------------------------------------------------------------------------
// File          : data_bypass_topout.v
// Author        : Runbin Shi
// Created       : 03.12.2016
// Last modified : 03.12.2016
//-----------------------------------------------------------------------------
// Description : This is a simple example module which connects the Matlab &
//               XPS Project. This module will do a flip of MSByte and LSByte
//-----------------------------------------------------------------------------

module data_bypass_topout(
                           input wire         clk,
                           input wire         rst_n,
                           input wire         en,
                           input wire [127:0] data_in,

                           output wire [127:0] data_out,
                           output wire         data_out_vld
                           );

   assign data_out_vld = en;

   assign data_out[8-1:0] = data_in[128-1:120];
   assign data_out[16-1:8] = data_in[120-1:112];
   assign data_out[24-1:16] = data_in[112-1:104];
   assign data_out[32-1:24] = data_in[104-1:96];
   assign data_out[40-1:32] = data_in[96-1:88];
   assign data_out[48-1:40] = data_in[88-1:80];
   assign data_out[56-1:48] = data_in[80-1:72];
   assign data_out[64-1:56] = data_in[72-1:64];
   assign data_out[72-1:64] = data_in[64-1:56];
   assign data_out[80-1:72] = data_in[56-1:48];
   assign data_out[88-1:80] = data_in[48-1:40];
   assign data_out[96-1:88] = data_in[40-1:32];
   assign data_out[104-1:96] = data_in[32-1:24];
   assign data_out[112-1:104] = data_in[24-1:16];
   assign data_out[120-1:112] = data_in[16-1:8];
   assign data_out[128-1:120] = data_in[8-1:0];

endmodule // data_bypass_topout
