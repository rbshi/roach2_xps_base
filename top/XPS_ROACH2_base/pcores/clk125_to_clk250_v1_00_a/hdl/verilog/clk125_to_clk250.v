//-----------------------------------------------------------------------------
// Title         : 125Mhz Clk to 250Mhz Clk data interface
// Project       : ROACH2
//-----------------------------------------------------------------------------
// File          : clk125_to_clk250.v
// Author        : Runbin Shi
// Created       : 23.11.2016
// Last modified : 23.11.2016
//-----------------------------------------------------------------------------
// Description :
//
//-----------------------------------------------------------------------------

module clk125_to_clk250(
                        input wire         clk_250,
                        input wire         clk_125,
                        input wire [255:0] data_in,
                        input wire         data_in_vld,
                        input wire         rst_n,

                        output reg [127:0] data_out,
                        output reg         data_out_vld
                        );
   (* ASYNC_REG = "TRUE" *) reg [255:0]                             data_in_d1;
   reg                                     data_in_vld_d1;
   reg                                     toggle_flag;

   always @(posedge clk_125) begin
      data_in_d1 <= data_in;
      data_in_vld_d1 <= data_in_vld;
   end


   always @(posedge clk_250) begin
      if(~rst_n) begin
         toggle_flag <= 0;
      end else begin
         if(data_in_vld_d1) begin
            toggle_flag <= ~toggle_flag;
         end
      end
   end

   always @(posedge clk_250) begin
      case (toggle_flag)
        1: data_out <= data_in_d1[255:128];
        default:data_out <= data_in_d1[127:0];
      endcase // case (toggle_flag)
   end

   always @(posedge clk_250) begin
      data_out_vld <= data_in_vld_d1;
   end

endmodule // clk125_to_clk250
