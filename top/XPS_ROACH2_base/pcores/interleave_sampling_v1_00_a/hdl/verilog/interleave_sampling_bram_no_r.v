`timescale 1ns / 1ps
`default_nettype none
//-----------------------------------------------------------------------------
// Title         : Super Sampling by pixel interleaving using FPGA BRAM
// Project       : ROACH2/Virtex-6
//-----------------------------------------------------------------------------
// File          : interleave_sampling.v
// Author        : Justin S. J. Wong
// Created       : 26 Jan 2017
// Release (v1.0): 03 Feb 2017 
//-----------------------------------------------------------------------------
// Description : p (BUF_LEN) = 1024, q (Interleave DIST) =3
//-----------------------------------------------------------------------------

module interleave_sampling #(
								parameter BITS_PER_PIX = 8,
								parameter PIXS_PER_BLK = 32
							)(
								input wire         clk,
								input wire         rst_n,
								input wire         en,
								input wire [PIXS_PER_BLK*BITS_PER_PIX-1:0] data_in, //[255:0]
								
								output reg [PIXS_PER_BLK*BITS_PER_PIX-1:0] data_out, //[255:0]
								output wire        wr_en
							);

parameter BUF_LEN = 1024, DIST = 3; //p: buffer word length, q: interleave distance
localparam BLK_NUM = BUF_LEN / PIXS_PER_BLK; // 1024 / 32 = 32
localparam ADDR_BLK_SIZE = PIXS_PER_BLK * DIST; // 32 * 3 = 96

//States labels:
localparam  IDLE = 0,
   			WR_BUF = 1;

reg [0:0] state;
reg rWR_EN;

localparam MemAddrWidth = 4;
localparam MemAddrNum = 11;

reg [5-1:0] wr_cnt;
reg [1:0] interleave_cnt;
reg [1:0] iWE_A32b;
reg [1:0] iWE_B10b, iWE_B22b;
reg [1:0] iWE_C21b, iWE_C11b;

reg [MemAddrWidth-1:0] iAddr_A32b [1:0];
reg [MemAddrWidth-1:0] iAddr_B10b [1:0];
reg [MemAddrWidth-1:0] iAddr_B22b [1:0];
reg [MemAddrWidth-1:0] iAddr_C21b [1:0];
reg [MemAddrWidth-1:0] iAddr_C11b [1:0];
reg [MemAddrWidth-1:0] iAddr_Rd_ABC [1:0];

wire [PIXS_PER_BLK*BITS_PER_PIX-1:0] iDat_A32b;
wire [10*BITS_PER_PIX-1:0] iDat_B10b;
wire [22*BITS_PER_PIX-1:0] iDat_B22b;
wire [21*BITS_PER_PIX-1:0] iDat_C21b;
wire [11*BITS_PER_PIX-1:0] iDat_C11b;

wire [PIXS_PER_BLK*BITS_PER_PIX-1:0] oDat_A32b [1:0];
wire [10*BITS_PER_PIX-1:0] oDat_B10b [1:0];
wire [22*BITS_PER_PIX-1:0] oDat_B22b [1:0];
wire [21*BITS_PER_PIX-1:0] oDat_C21b [1:0];
wire [11*BITS_PER_PIX-1:0] oDat_C11b [1:0];

wire [PIXS_PER_BLK*BITS_PER_PIX-1:0] oDat_C32b [1:0];
wire [PIXS_PER_BLK*BITS_PER_PIX-1:0] oDat_B32b [1:0];

genvar i;
generate
	assign iDat_A32b = data_in;
	assign iDat_B10b = data_in[(PIXS_PER_BLK*BITS_PER_PIX-1) : (PIXS_PER_BLK-10)*BITS_PER_PIX];
	assign iDat_B22b = data_in[((PIXS_PER_BLK-10)*BITS_PER_PIX-1) : 0];
	assign iDat_C21b = data_in[(PIXS_PER_BLK*BITS_PER_PIX-1) : (PIXS_PER_BLK-21)*BITS_PER_PIX];
	assign iDat_C11b = data_in[((PIXS_PER_BLK-21)*BITS_PER_PIX-1) : 0];

	for(i=0;i<2;i=i+1)
	begin: Line_Buffer 	//Instantiates BRAM line buffers
		assign oDat_B32b[i] = {oDat_B22b[i],oDat_B10b[i]};
		assign oDat_C32b[i] = {oDat_C11b[i],oDat_C21b[i]};
		linear_bram_1clk #(
		   .DataWidth(32 * BITS_PER_PIX),
		   .MemAddrWidth(MemAddrWidth),.MemAddrNum(MemAddrNum),.UseOutReg(0)
		) LINE_A32b (
		   .iClk_A(clk),
		   .iWE_A(iWE_A32b[i]),.iAddrA(iAddr_A32b[i]),.iDatA(iDat_A32b),
		   .iAddrB(iAddr_Rd_ABC[i]),.oDatB(oDat_A32b[i])
		);

		linear_bram_1clk #(
		   .DataWidth(10 * BITS_PER_PIX),
		   .MemAddrWidth(MemAddrWidth),.MemAddrNum(MemAddrNum),.UseOutReg(0)
		) LINE_B10b (
		   .iClk_A(clk),
		   .iWE_A(iWE_B10b[i]),.iAddrA(iAddr_B10b[i]),.iDatA(iDat_B10b),
		   .iAddrB(iAddr_Rd_ABC[i]),.oDatB(oDat_B10b[i])
		);
		linear_bram_1clk #(
		   .DataWidth(22 * BITS_PER_PIX),
		   .MemAddrWidth(MemAddrWidth),.MemAddrNum(MemAddrNum),.UseOutReg(0)
		) LINE_B22b (
		   .iClk_A(clk),
		   .iWE_A(iWE_B22b[i]),.iAddrA(iAddr_B22b[i]),.iDatA(iDat_B22b),
		   .iAddrB(iAddr_Rd_ABC[i]),.oDatB(oDat_B22b[i])
		);
		
		linear_bram_1clk #(
		   .DataWidth(21 * BITS_PER_PIX),
		   .MemAddrWidth(MemAddrWidth),.MemAddrNum(MemAddrNum),.UseOutReg(0)
		) LINE_C21b (
		   .iClk_A(clk),
		   .iWE_A(iWE_C21b[i]),.iAddrA(iAddr_C21b[i]),.iDatA(iDat_C21b),
		   .iAddrB(iAddr_Rd_ABC[i]),.oDatB(oDat_C21b[i])
		);
		linear_bram_1clk #(
		   .DataWidth(11 * BITS_PER_PIX),
		   .MemAddrWidth(MemAddrWidth),.MemAddrNum(MemAddrNum),.UseOutReg(0)
		) LINE_C11b (
		   .iClk_A(clk),
		   .iWE_A(iWE_C11b[i]),.iAddrA(iAddr_C11b[i]),.iDatA(iDat_C11b),
		   .iAddrB(iAddr_Rd_ABC[i]),.oDatB(oDat_C11b[i])
		);
	end
endgenerate	

reg rS = 1'bx; //Line ping-pong buffer select
reg rrS = 1'bx; //Delayed Line ping-pong buffer select

task reset_write_addr;
input [0:0] index;
begin
	iAddr_A32b[index] <= {4{1'b1}}; //set to all ones (-1), so the next increment will wrap back to zero
	iAddr_B10b[index] <= {4{1'b1}};
	iAddr_B22b[index] <= {4{1'b1}};
	iAddr_C21b[index] <= {4{1'b1}};
	iAddr_C11b[index] <= {4{1'b1}};
end
endtask

task clear_all;
begin
	data_out <= 0;
	state <= IDLE;
	rWR_EN <= 1'b0;		
	wr_cnt <= 0;
	interleave_cnt <= 0;
	rS <= 1'b0;
	rrS <= 1'b0;
	iWE_A32b <= 0;
	iWE_B10b <= 0;
	iWE_B22b <= 0;
	iWE_C21b <= 0;
	iWE_C11b <= 0;
	begin: Clear_Addr_Arrays
		integer k;
		for(k=0;k<2;k=k+1) begin                 
			reset_write_addr(k[0]);  //task
			iAddr_Rd_ABC[k[0]] <= 0;
		end
	end    
end
endtask

assign wr_en = rWR_EN;

always @(posedge clk or negedge rst_n) begin
	 if(!rst_n) begin //Set power on default values
		clear_all;
	 end else begin
		rrS <= rS;
		//Default values for BRAM Write ENs
		iWE_A32b <= 0;
		iWE_B10b <= 0;
		iWE_B22b <= 0;
		iWE_C21b <= 0;
		iWE_C11b <= 0;
				
			//****************************** State machine logic ************************************
			case(state) 
				IDLE: begin
				clear_all;  
					if (en) begin
						state <= WR_BUF;							
					end
				end
				WR_BUF: begin
					wr_cnt <= wr_cnt + 1'b1; //default action
					
					//Fill Buffer A ***********************************************
					if (wr_cnt<=10) begin
						iWE_A32b[rS] <= 1'b1;
						iAddr_A32b[rS] <= iAddr_A32b[rS] + 1'b1;
						if (wr_cnt==10) begin
							iWE_B10b[rS] <= 1'b1;
							iAddr_B10b[rS] <= iAddr_B10b[rS] + 1'b1;
						end
					//Fill Buffer C ***********************************************
					end else if (wr_cnt<=21) begin
						iWE_B22b[rS] <= 1'b1;
						iAddr_B22b[rS] <= iAddr_B22b[rS] + 1'b1;
						if (wr_cnt<21) begin
							iWE_B10b[rS] <= 1'b1;
							iAddr_B10b[rS] <= iAddr_B10b[rS] + 1'b1;				
						end	else begin
							iWE_C21b[rS] <= 1'b1;
							iAddr_C21b[rS] <= iAddr_C21b[rS] + 1'b1;							
						end			
					//Fill Buffer B ***********************************************
					end else if (wr_cnt<=31) begin
						iWE_C11b[rS] <= 1'b1;
						iAddr_C11b[rS] <= iAddr_C11b[rS] + 1'b1;							
						iWE_C21b[rS] <= 1'b1;
						iAddr_C21b[rS] <= iAddr_C21b[rS] + 1'b1;
						if (wr_cnt==31) begin //clear and select the other buffer
							reset_write_addr(~rS); //task
							rS <= ~rS; //toggle buffer select
							wr_cnt <= 0;								
						end	
					end 
					//**************************************************************
					
					if (rrS | rWR_EN)  begin //****** Buffer Read and construct interleaved output *****************************
						interleave_cnt <= interleave_cnt + 1'b1; //default action
						rWR_EN <= 1'b1; // assert write enable
						//$display( "iAddr_Rd_ABC[~rS]=%0d, interleave_cnt=%0d",iAddr_Rd_ABC[~rS],interleave_cnt); //
						case (interleave_cnt)
							2'd0: begin: Interleave_Output0
								 integer i;
								 for(i=0;i<=9;i=i+1) begin
									data_out[i*3*BITS_PER_PIX +: 3*BITS_PER_PIX] <= {	oDat_B32b[~rrS][i*BITS_PER_PIX+:BITS_PER_PIX],
																						oDat_C32b[~rrS][i*BITS_PER_PIX+:BITS_PER_PIX],
																						oDat_A32b[~rrS][i*BITS_PER_PIX+:BITS_PER_PIX] };
								 end
									data_out[10*3*BITS_PER_PIX +: 2*BITS_PER_PIX] <= {	oDat_C32b[~rrS][10*BITS_PER_PIX+:BITS_PER_PIX],
																						oDat_A32b[~rrS][10*BITS_PER_PIX+:BITS_PER_PIX] };
							end
							2'd1: begin: Interleave_Output1
								 integer i;
								 for(i=11;i<=20;i=i+1) begin
									data_out[(i-11)*3*BITS_PER_PIX +: 3*BITS_PER_PIX] <= {oDat_C32b[~rrS][i*BITS_PER_PIX+:BITS_PER_PIX],
																						oDat_A32b[~rrS][i*BITS_PER_PIX+:BITS_PER_PIX],
																						oDat_B32b[~rrS][(i-1)*BITS_PER_PIX+:BITS_PER_PIX] };
								 end
									data_out[(21-11)*3*BITS_PER_PIX +: 2*BITS_PER_PIX] <= {oDat_A32b[~rrS][21*BITS_PER_PIX+:BITS_PER_PIX],
																											oDat_B32b[~rrS][20*BITS_PER_PIX+:BITS_PER_PIX] };
								iAddr_Rd_ABC[~rrS] <= iAddr_Rd_ABC[~rrS] + 1'b1;  //default action																		
							end
							2'd2: begin: Interleave_Output2
								 integer i;
								 for(i=21;i<=30;i=i+1) begin
									data_out[(i-21)*3*BITS_PER_PIX +: 3*BITS_PER_PIX] <= {oDat_A32b[~rrS][(i+1)*BITS_PER_PIX+:BITS_PER_PIX],
																						oDat_B32b[~rrS][i*BITS_PER_PIX+:BITS_PER_PIX],
																						oDat_C32b[~rrS][i*BITS_PER_PIX+:BITS_PER_PIX] };
								 end
									data_out[(31-21)*3*BITS_PER_PIX +: 2*BITS_PER_PIX] <=  {	oDat_B32b[~rrS][31*BITS_PER_PIX+:BITS_PER_PIX],
																						oDat_C32b[~rrS][31*BITS_PER_PIX+:BITS_PER_PIX] };
								interleave_cnt <= 0;									
							end
							default: clear_all;
						endcase
						
						if (wr_cnt==0) begin 
							interleave_cnt <= 0; //clear interleave_cnt when switching line buffer
							iAddr_Rd_ABC[~rrS] <= 0; //override action
						end
						
					end else begin
						data_out <= 0;
					end
				end //end state WR_BUF
				default: clear_all;
			endcase // case (state)

	  //*******************************************************************           
	  end //end else(!rst_n)
 end // always @ (posedge clk)

endmodule
`default_nettype wire