//-----------------------------------------------------------------------------
// Title         : Sampling Module
// Project       : ROACH2
//-----------------------------------------------------------------------------
// File          : interleave_sampling.v
// Author        : Runbin Shi
// Created       : 16.11.2016
// Last modified : 18.11.2016
//-----------------------------------------------------------------------------
// Description : p = 1024 / q =3
//
//-----------------------------------------------------------------------------

module interleave_sampling(
                           input wire         clk,
                           input wire         rst_n,
                           input wire         en,
                           input wire [255:0] data_in,

                           output reg [255:0] data_out,
                           output reg         wr_en
                           );

   parameter BITS_PER_PIX = 8;
   parameter PIXS_PER_BLK = 32;

   localparam IDLE = 0;
   localparam WRA = 1;
   localparam WRB = 2;

   reg [1:0]                                  cur_state;
   reg [1:0]                                  cur_state_d1;
   reg [1:0]                                  nxt_state;


   reg [BITS_PER_PIX-1:0]                     samp_buf_a [1024-1:0];
   reg [BITS_PER_PIX-1:0]                     samp_buf_b [1024-1:0];

   reg [10-1:0]                               wr_address_a;
   reg [10-1:0]                               wr_address_b;

   reg [5-1:0]                                wr_cnt_a;
   reg [5-1:0]                                wr_cnt_b;

   // always @(posedge clk or posedge rst_n) begin
   always @(posedge clk) begin
      if(!rst_n) begin
         cur_state <= IDLE;
         cur_state_d1 <= IDLE;
      end else begin
         cur_state <= nxt_state;
         cur_state_d1 <= cur_state;
      end
   end

   always @(*) begin
      case(cur_state)
        IDLE:
          if(en) begin
             nxt_state = WRA;
          end else begin
             nxt_state = IDLE;
          end
        WRA:
          if(wr_cnt_a == 1024/PIXS_PER_BLK-1) begin
             nxt_state = WRB;
          end else begin
             nxt_state = WRA;
          end
        WRB:
          if(wr_cnt_b == 1024/PIXS_PER_BLK-1)
            nxt_state = WRA;
          else
            nxt_state = WRB;
        default: nxt_state = IDLE;
      endcase // case (cur_state)
   end

   // Buffer write related logic
   always @(posedge clk) begin

      case(cur_state_d1)
        IDLE: wr_en <= 0;
        WRA: wr_en <= wr_en;
        WRB: wr_en <= 1;
        default: wr_en <= 0;
      endcase // case (cur_state_d1)

   end



   always @(posedge clk) begin
      case(cur_state)
        IDLE: begin
           wr_cnt_a <= 0;
           wr_cnt_b <= 0;
           wr_address_a <=0;
           wr_address_b <= 0;

        end

        WRA: begin
           wr_cnt_a <= wr_cnt_a + 1;
           wr_address_a <= wr_address_a + 96;
           wr_cnt_b <=0;
           wr_address_b <= 0;
        end

        WRB: begin
           wr_cnt_b <= wr_cnt_b + 1;
           wr_address_b <= wr_address_b + 96;
           wr_cnt_a <=0;
           wr_address_a <= 0;
        end

        default: begin
           wr_cnt_a <= 0;
           wr_cnt_b <= 0;
           wr_address_a <=0;
           wr_address_b <= 0;
        end // case: default
      endcase // case (cur_state)
   end // always @ (posedge clk)

   // Write related logic
   reg [10-1:0] wr_addr_a_0;
   reg [10-1:0] wr_addr_a_1;
   reg [10-1:0] wr_addr_a_2;
   reg [10-1:0] wr_addr_a_3;
   reg [10-1:0] wr_addr_a_4;
   reg [10-1:0] wr_addr_a_5;
   reg [10-1:0] wr_addr_a_6;
   reg [10-1:0] wr_addr_a_7;
   reg [10-1:0] wr_addr_a_8;
   reg [10-1:0] wr_addr_a_9;
   reg [10-1:0] wr_addr_a_10;
   reg [10-1:0] wr_addr_a_11;
   reg [10-1:0] wr_addr_a_12;
   reg [10-1:0] wr_addr_a_13;
   reg [10-1:0] wr_addr_a_14;
   reg [10-1:0] wr_addr_a_15;
   reg [10-1:0] wr_addr_a_16;
   reg [10-1:0] wr_addr_a_17;
   reg [10-1:0] wr_addr_a_18;
   reg [10-1:0] wr_addr_a_19;
   reg [10-1:0] wr_addr_a_20;
   reg [10-1:0] wr_addr_a_21;
   reg [10-1:0] wr_addr_a_22;
   reg [10-1:0] wr_addr_a_23;
   reg [10-1:0] wr_addr_a_24;
   reg [10-1:0] wr_addr_a_25;
   reg [10-1:0] wr_addr_a_26;
   reg [10-1:0] wr_addr_a_27;
   reg [10-1:0] wr_addr_a_28;
   reg [10-1:0] wr_addr_a_29;
   reg [10-1:0] wr_addr_a_30;
   reg [10-1:0] wr_addr_a_31;


   reg [10-1:0] wr_addr_b_0;
   reg [10-1:0] wr_addr_b_1;
   reg [10-1:0] wr_addr_b_2;
   reg [10-1:0] wr_addr_b_3;
   reg [10-1:0] wr_addr_b_4;
   reg [10-1:0] wr_addr_b_5;
   reg [10-1:0] wr_addr_b_6;
   reg [10-1:0] wr_addr_b_7;
   reg [10-1:0] wr_addr_b_8;
   reg [10-1:0] wr_addr_b_9;
   reg [10-1:0] wr_addr_b_10;
   reg [10-1:0] wr_addr_b_11;
   reg [10-1:0] wr_addr_b_12;
   reg [10-1:0] wr_addr_b_13;
   reg [10-1:0] wr_addr_b_14;
   reg [10-1:0] wr_addr_b_15;
   reg [10-1:0] wr_addr_b_16;
   reg [10-1:0] wr_addr_b_17;
   reg [10-1:0] wr_addr_b_18;
   reg [10-1:0] wr_addr_b_19;
   reg [10-1:0] wr_addr_b_20;
   reg [10-1:0] wr_addr_b_21;
   reg [10-1:0] wr_addr_b_22;
   reg [10-1:0] wr_addr_b_23;
   reg [10-1:0] wr_addr_b_24;
   reg [10-1:0] wr_addr_b_25;
   reg [10-1:0] wr_addr_b_26;
   reg [10-1:0] wr_addr_b_27;
   reg [10-1:0] wr_addr_b_28;
   reg [10-1:0] wr_addr_b_29;
   reg [10-1:0] wr_addr_b_30;
   reg [10-1:0] wr_addr_b_31;





   always @(posedge clk) begin
      wr_addr_a_0 <= wr_address_a+0;
      wr_addr_a_1 <= wr_address_a+3;
      wr_addr_a_2 <= wr_address_a+6;
      wr_addr_a_3 <= wr_address_a+9;
      wr_addr_a_4 <= wr_address_a+12;
      wr_addr_a_5 <= wr_address_a+15;
      wr_addr_a_6 <= wr_address_a+18;
      wr_addr_a_7 <= wr_address_a+21;
      wr_addr_a_8 <= wr_address_a+24;
      wr_addr_a_9 <= wr_address_a+27;
      wr_addr_a_10 <= wr_address_a+30;
      wr_addr_a_11 <= wr_address_a+33;
      wr_addr_a_12 <= wr_address_a+36;
      wr_addr_a_13 <= wr_address_a+39;
      wr_addr_a_14 <= wr_address_a+42;
      wr_addr_a_15 <= wr_address_a+45;
      wr_addr_a_16 <= wr_address_a+48;
      wr_addr_a_17 <= wr_address_a+51;
      wr_addr_a_18 <= wr_address_a+54;
      wr_addr_a_19 <= wr_address_a+57;
      wr_addr_a_20 <= wr_address_a+60;
      wr_addr_a_21 <= wr_address_a+63;
      wr_addr_a_22 <= wr_address_a+66;
      wr_addr_a_23 <= wr_address_a+69;
      wr_addr_a_24 <= wr_address_a+72;
      wr_addr_a_25 <= wr_address_a+75;
      wr_addr_a_26 <= wr_address_a+78;
      wr_addr_a_27 <= wr_address_a+81;
      wr_addr_a_28 <= wr_address_a+84;
      wr_addr_a_29 <= wr_address_a+87;
      wr_addr_a_30 <= wr_address_a+90;
      wr_addr_a_31 <= wr_address_a+93;

      wr_addr_b_0 <= wr_address_b+0;
      wr_addr_b_1 <= wr_address_b+3;
      wr_addr_b_2 <= wr_address_b+6;
      wr_addr_b_3 <= wr_address_b+9;
      wr_addr_b_4 <= wr_address_b+12;
      wr_addr_b_5 <= wr_address_b+15;
      wr_addr_b_6 <= wr_address_b+18;
      wr_addr_b_7 <= wr_address_b+21;
      wr_addr_b_8 <= wr_address_b+24;
      wr_addr_b_9 <= wr_address_b+27;
      wr_addr_b_10 <= wr_address_b+30;
      wr_addr_b_11 <= wr_address_b+33;
      wr_addr_b_12 <= wr_address_b+36;
      wr_addr_b_13 <= wr_address_b+39;
      wr_addr_b_14 <= wr_address_b+42;
      wr_addr_b_15 <= wr_address_b+45;
      wr_addr_b_16 <= wr_address_b+48;
      wr_addr_b_17 <= wr_address_b+51;
      wr_addr_b_18 <= wr_address_b+54;
      wr_addr_b_19 <= wr_address_b+57;
      wr_addr_b_20 <= wr_address_b+60;
      wr_addr_b_21 <= wr_address_b+63;
      wr_addr_b_22 <= wr_address_b+66;
      wr_addr_b_23 <= wr_address_b+69;
      wr_addr_b_24 <= wr_address_b+72;
      wr_addr_b_25 <= wr_address_b+75;
      wr_addr_b_26 <= wr_address_b+78;
      wr_addr_b_27 <= wr_address_b+81;
      wr_addr_b_28 <= wr_address_b+84;
      wr_addr_b_29 <= wr_address_b+87;
      wr_addr_b_30 <= wr_address_b+90;
      wr_addr_b_31 <= wr_address_b+93;

   end // always @ (posedge clk)



   always @(posedge clk) begin
		if(cur_state_d1[0]) begin
           samp_buf_a[wr_addr_a_0] <= data_in[1*BITS_PER_PIX-1:0*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_1] <= data_in[2*BITS_PER_PIX-1:1*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_2] <= data_in[3*BITS_PER_PIX-1:2*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_3] <= data_in[4*BITS_PER_PIX-1:3*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_4] <= data_in[5*BITS_PER_PIX-1:4*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_5] <= data_in[6*BITS_PER_PIX-1:5*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_6] <= data_in[7*BITS_PER_PIX-1:6*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_7] <= data_in[8*BITS_PER_PIX-1:7*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_8] <= data_in[9*BITS_PER_PIX-1:8*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_9] <= data_in[10*BITS_PER_PIX-1:9*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_10] <= data_in[11*BITS_PER_PIX-1:10*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_11] <= data_in[12*BITS_PER_PIX-1:11*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_12] <= data_in[13*BITS_PER_PIX-1:12*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_13] <= data_in[14*BITS_PER_PIX-1:13*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_14] <= data_in[15*BITS_PER_PIX-1:14*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_15] <= data_in[16*BITS_PER_PIX-1:15*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_16] <= data_in[17*BITS_PER_PIX-1:16*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_17] <= data_in[18*BITS_PER_PIX-1:17*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_18] <= data_in[19*BITS_PER_PIX-1:18*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_19] <= data_in[20*BITS_PER_PIX-1:19*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_20] <= data_in[21*BITS_PER_PIX-1:20*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_21] <= data_in[22*BITS_PER_PIX-1:21*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_22] <= data_in[23*BITS_PER_PIX-1:22*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_23] <= data_in[24*BITS_PER_PIX-1:23*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_24] <= data_in[25*BITS_PER_PIX-1:24*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_25] <= data_in[26*BITS_PER_PIX-1:25*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_26] <= data_in[27*BITS_PER_PIX-1:26*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_27] <= data_in[28*BITS_PER_PIX-1:27*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_28] <= data_in[29*BITS_PER_PIX-1:28*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_29] <= data_in[30*BITS_PER_PIX-1:29*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_30] <= data_in[31*BITS_PER_PIX-1:30*BITS_PER_PIX];
           samp_buf_a[wr_addr_a_31] <= data_in[32*BITS_PER_PIX-1:31*BITS_PER_PIX];
        end else begin
           samp_buf_b[wr_addr_b_0] <= data_in[1*BITS_PER_PIX-1:0*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_1] <= data_in[2*BITS_PER_PIX-1:1*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_2] <= data_in[3*BITS_PER_PIX-1:2*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_3] <= data_in[4*BITS_PER_PIX-1:3*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_4] <= data_in[5*BITS_PER_PIX-1:4*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_5] <= data_in[6*BITS_PER_PIX-1:5*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_6] <= data_in[7*BITS_PER_PIX-1:6*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_7] <= data_in[8*BITS_PER_PIX-1:7*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_8] <= data_in[9*BITS_PER_PIX-1:8*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_9] <= data_in[10*BITS_PER_PIX-1:9*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_10] <= data_in[11*BITS_PER_PIX-1:10*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_11] <= data_in[12*BITS_PER_PIX-1:11*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_12] <= data_in[13*BITS_PER_PIX-1:12*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_13] <= data_in[14*BITS_PER_PIX-1:13*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_14] <= data_in[15*BITS_PER_PIX-1:14*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_15] <= data_in[16*BITS_PER_PIX-1:15*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_16] <= data_in[17*BITS_PER_PIX-1:16*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_17] <= data_in[18*BITS_PER_PIX-1:17*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_18] <= data_in[19*BITS_PER_PIX-1:18*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_19] <= data_in[20*BITS_PER_PIX-1:19*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_20] <= data_in[21*BITS_PER_PIX-1:20*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_21] <= data_in[22*BITS_PER_PIX-1:21*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_22] <= data_in[23*BITS_PER_PIX-1:22*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_23] <= data_in[24*BITS_PER_PIX-1:23*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_24] <= data_in[25*BITS_PER_PIX-1:24*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_25] <= data_in[26*BITS_PER_PIX-1:25*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_26] <= data_in[27*BITS_PER_PIX-1:26*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_27] <= data_in[28*BITS_PER_PIX-1:27*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_28] <= data_in[29*BITS_PER_PIX-1:28*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_29] <= data_in[30*BITS_PER_PIX-1:29*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_30] <= data_in[31*BITS_PER_PIX-1:30*BITS_PER_PIX];
           samp_buf_b[wr_addr_b_31] <= data_in[32*BITS_PER_PIX-1:31*BITS_PER_PIX];
			  end
   end // always @ (posedge clk)

reg [10-1:0] rd_addr_a_0;
reg [10-1:0] rd_addr_a_1;
reg [10-1:0] rd_addr_a_2;
reg [10-1:0] rd_addr_a_3;
reg [10-1:0] rd_addr_a_4;
reg [10-1:0] rd_addr_a_5;
reg [10-1:0] rd_addr_a_6;
reg [10-1:0] rd_addr_a_7;
reg [10-1:0] rd_addr_a_8;
reg [10-1:0] rd_addr_a_9;
reg [10-1:0] rd_addr_a_10;
reg [10-1:0] rd_addr_a_11;
reg [10-1:0] rd_addr_a_12;
reg [10-1:0] rd_addr_a_13;
reg [10-1:0] rd_addr_a_14;
reg [10-1:0] rd_addr_a_15;
reg [10-1:0] rd_addr_a_16;
reg [10-1:0] rd_addr_a_17;
reg [10-1:0] rd_addr_a_18;
reg [10-1:0] rd_addr_a_19;
reg [10-1:0] rd_addr_a_20;
reg [10-1:0] rd_addr_a_21;
reg [10-1:0] rd_addr_a_22;
reg [10-1:0] rd_addr_a_23;
reg [10-1:0] rd_addr_a_24;
reg [10-1:0] rd_addr_a_25;
reg [10-1:0] rd_addr_a_26;
reg [10-1:0] rd_addr_a_27;
reg [10-1:0] rd_addr_a_28;
reg [10-1:0] rd_addr_a_29;
reg [10-1:0] rd_addr_a_30;
reg [10-1:0] rd_addr_a_31;

reg [10-1:0] rd_addr_b_0;
reg [10-1:0] rd_addr_b_1;
reg [10-1:0] rd_addr_b_2;
reg [10-1:0] rd_addr_b_3;
reg [10-1:0] rd_addr_b_4;
reg [10-1:0] rd_addr_b_5;
reg [10-1:0] rd_addr_b_6;
reg [10-1:0] rd_addr_b_7;
reg [10-1:0] rd_addr_b_8;
reg [10-1:0] rd_addr_b_9;
reg [10-1:0] rd_addr_b_10;
reg [10-1:0] rd_addr_b_11;
reg [10-1:0] rd_addr_b_12;
reg [10-1:0] rd_addr_b_13;
reg [10-1:0] rd_addr_b_14;
reg [10-1:0] rd_addr_b_15;
reg [10-1:0] rd_addr_b_16;
reg [10-1:0] rd_addr_b_17;
reg [10-1:0] rd_addr_b_18;
reg [10-1:0] rd_addr_b_19;
reg [10-1:0] rd_addr_b_20;
reg [10-1:0] rd_addr_b_21;
reg [10-1:0] rd_addr_b_22;
reg [10-1:0] rd_addr_b_23;
reg [10-1:0] rd_addr_b_24;
reg [10-1:0] rd_addr_b_25;
reg [10-1:0] rd_addr_b_26;
reg [10-1:0] rd_addr_b_27;
reg [10-1:0] rd_addr_b_28;
reg [10-1:0] rd_addr_b_29;
reg [10-1:0] rd_addr_b_30;
reg [10-1:0] rd_addr_b_31;

   always @(posedge clk) begin
rd_addr_a_0 <= (wr_cnt_b<<5)+0;
rd_addr_a_1 <= (wr_cnt_b<<5)+1;
rd_addr_a_2 <= (wr_cnt_b<<5)+2;
rd_addr_a_3 <= (wr_cnt_b<<5)+3;
rd_addr_a_4 <= (wr_cnt_b<<5)+4;
rd_addr_a_5 <= (wr_cnt_b<<5)+5;
rd_addr_a_6 <= (wr_cnt_b<<5)+6;
rd_addr_a_7 <= (wr_cnt_b<<5)+7;
rd_addr_a_8 <= (wr_cnt_b<<5)+8;
rd_addr_a_9 <= (wr_cnt_b<<5)+9;
rd_addr_a_10 <= (wr_cnt_b<<5)+10;
rd_addr_a_11 <= (wr_cnt_b<<5)+11;
rd_addr_a_12 <= (wr_cnt_b<<5)+12;
rd_addr_a_13 <= (wr_cnt_b<<5)+13;
rd_addr_a_14 <= (wr_cnt_b<<5)+14;
rd_addr_a_15 <= (wr_cnt_b<<5)+15;
rd_addr_a_16 <= (wr_cnt_b<<5)+16;
rd_addr_a_17 <= (wr_cnt_b<<5)+17;
rd_addr_a_18 <= (wr_cnt_b<<5)+18;
rd_addr_a_19 <= (wr_cnt_b<<5)+19;
rd_addr_a_20 <= (wr_cnt_b<<5)+20;
rd_addr_a_21 <= (wr_cnt_b<<5)+21;
rd_addr_a_22 <= (wr_cnt_b<<5)+22;
rd_addr_a_23 <= (wr_cnt_b<<5)+23;
rd_addr_a_24 <= (wr_cnt_b<<5)+24;
rd_addr_a_25 <= (wr_cnt_b<<5)+25;
rd_addr_a_26 <= (wr_cnt_b<<5)+26;
rd_addr_a_27 <= (wr_cnt_b<<5)+27;
rd_addr_a_28 <= (wr_cnt_b<<5)+28;
rd_addr_a_29 <= (wr_cnt_b<<5)+29;
rd_addr_a_30 <= (wr_cnt_b<<5)+30;
rd_addr_a_31 <= (wr_cnt_b<<5)+31;

rd_addr_b_0 <= (wr_cnt_a<<5)+0;
rd_addr_b_1 <= (wr_cnt_a<<5)+1;
rd_addr_b_2 <= (wr_cnt_a<<5)+2;
rd_addr_b_3 <= (wr_cnt_a<<5)+3;
rd_addr_b_4 <= (wr_cnt_a<<5)+4;
rd_addr_b_5 <= (wr_cnt_a<<5)+5;
rd_addr_b_6 <= (wr_cnt_a<<5)+6;
rd_addr_b_7 <= (wr_cnt_a<<5)+7;
rd_addr_b_8 <= (wr_cnt_a<<5)+8;
rd_addr_b_9 <= (wr_cnt_a<<5)+9;
rd_addr_b_10 <= (wr_cnt_a<<5)+10;
rd_addr_b_11 <= (wr_cnt_a<<5)+11;
rd_addr_b_12 <= (wr_cnt_a<<5)+12;
rd_addr_b_13 <= (wr_cnt_a<<5)+13;
rd_addr_b_14 <= (wr_cnt_a<<5)+14;
rd_addr_b_15 <= (wr_cnt_a<<5)+15;
rd_addr_b_16 <= (wr_cnt_a<<5)+16;
rd_addr_b_17 <= (wr_cnt_a<<5)+17;
rd_addr_b_18 <= (wr_cnt_a<<5)+18;
rd_addr_b_19 <= (wr_cnt_a<<5)+19;
rd_addr_b_20 <= (wr_cnt_a<<5)+20;
rd_addr_b_21 <= (wr_cnt_a<<5)+21;
rd_addr_b_22 <= (wr_cnt_a<<5)+22;
rd_addr_b_23 <= (wr_cnt_a<<5)+23;
rd_addr_b_24 <= (wr_cnt_a<<5)+24;
rd_addr_b_25 <= (wr_cnt_a<<5)+25;
rd_addr_b_26 <= (wr_cnt_a<<5)+26;
rd_addr_b_27 <= (wr_cnt_a<<5)+27;
rd_addr_b_28 <= (wr_cnt_a<<5)+28;
rd_addr_b_29 <= (wr_cnt_a<<5)+29;
rd_addr_b_30 <= (wr_cnt_a<<5)+30;
rd_addr_b_31 <= (wr_cnt_a<<5)+31;

   end // always @ (posedge clk)

   // Buffer Read related logic
   always @(posedge clk) begin
      if (cur_state_d1[0]) begin
data_out[1*BITS_PER_PIX-1:0*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_0];
data_out[2*BITS_PER_PIX-1:1*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_1];
data_out[3*BITS_PER_PIX-1:2*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_2];
data_out[4*BITS_PER_PIX-1:3*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_3];
data_out[5*BITS_PER_PIX-1:4*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_4];
data_out[6*BITS_PER_PIX-1:5*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_5];
data_out[7*BITS_PER_PIX-1:6*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_6];
data_out[8*BITS_PER_PIX-1:7*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_7];
data_out[9*BITS_PER_PIX-1:8*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_8];
data_out[10*BITS_PER_PIX-1:9*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_9];
data_out[11*BITS_PER_PIX-1:10*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_10];
data_out[12*BITS_PER_PIX-1:11*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_11];
data_out[13*BITS_PER_PIX-1:12*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_12];
data_out[14*BITS_PER_PIX-1:13*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_13];
data_out[15*BITS_PER_PIX-1:14*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_14];
data_out[16*BITS_PER_PIX-1:15*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_15];
data_out[17*BITS_PER_PIX-1:16*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_16];
data_out[18*BITS_PER_PIX-1:17*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_17];
data_out[19*BITS_PER_PIX-1:18*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_18];
data_out[20*BITS_PER_PIX-1:19*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_19];
data_out[21*BITS_PER_PIX-1:20*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_20];
data_out[22*BITS_PER_PIX-1:21*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_21];
data_out[23*BITS_PER_PIX-1:22*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_22];
data_out[24*BITS_PER_PIX-1:23*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_23];
data_out[25*BITS_PER_PIX-1:24*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_24];
data_out[26*BITS_PER_PIX-1:25*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_25];
data_out[27*BITS_PER_PIX-1:26*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_26];
data_out[28*BITS_PER_PIX-1:27*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_27];
data_out[29*BITS_PER_PIX-1:28*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_28];
data_out[30*BITS_PER_PIX-1:29*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_29];
data_out[31*BITS_PER_PIX-1:30*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_30];
data_out[32*BITS_PER_PIX-1:31*BITS_PER_PIX] <= samp_buf_b[rd_addr_b_31];

end else begin

data_out[1*BITS_PER_PIX-1:0*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_0];
data_out[2*BITS_PER_PIX-1:1*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_1];
data_out[3*BITS_PER_PIX-1:2*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_2];
data_out[4*BITS_PER_PIX-1:3*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_3];
data_out[5*BITS_PER_PIX-1:4*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_4];
data_out[6*BITS_PER_PIX-1:5*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_5];
data_out[7*BITS_PER_PIX-1:6*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_6];
data_out[8*BITS_PER_PIX-1:7*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_7];
data_out[9*BITS_PER_PIX-1:8*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_8];
data_out[10*BITS_PER_PIX-1:9*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_9];
data_out[11*BITS_PER_PIX-1:10*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_10];
data_out[12*BITS_PER_PIX-1:11*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_11];
data_out[13*BITS_PER_PIX-1:12*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_12];
data_out[14*BITS_PER_PIX-1:13*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_13];
data_out[15*BITS_PER_PIX-1:14*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_14];
data_out[16*BITS_PER_PIX-1:15*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_15];
data_out[17*BITS_PER_PIX-1:16*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_16];
data_out[18*BITS_PER_PIX-1:17*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_17];
data_out[19*BITS_PER_PIX-1:18*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_18];
data_out[20*BITS_PER_PIX-1:19*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_19];
data_out[21*BITS_PER_PIX-1:20*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_20];
data_out[22*BITS_PER_PIX-1:21*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_21];
data_out[23*BITS_PER_PIX-1:22*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_22];
data_out[24*BITS_PER_PIX-1:23*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_23];
data_out[25*BITS_PER_PIX-1:24*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_24];
data_out[26*BITS_PER_PIX-1:25*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_25];
data_out[27*BITS_PER_PIX-1:26*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_26];
data_out[28*BITS_PER_PIX-1:27*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_27];
data_out[29*BITS_PER_PIX-1:28*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_28];
data_out[30*BITS_PER_PIX-1:29*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_29];
data_out[31*BITS_PER_PIX-1:30*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_30];
data_out[32*BITS_PER_PIX-1:31*BITS_PER_PIX] <= samp_buf_a[rd_addr_a_31];

        end
   end // always @ (posedge clk)

endmodule // interleave_sampling
