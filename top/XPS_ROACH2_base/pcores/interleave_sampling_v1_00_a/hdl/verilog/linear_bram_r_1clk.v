`timescale 1ns / 1ps
`default_nettype none
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 25.10.2014 00:55:02
// Design Name: 
// Module Name: frame_block_ram
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////



module linear_bram_r_1clk  #(
parameter DataWidth = 256,
parameter MemAddrWidth = 4,      
parameter MemAddrNum = 11,
parameter UseOutReg = 0

)
(
input  wire iClk_A, iWE_A,
input  wire [MemAddrWidth-1:0] iAddrA,
input  wire [DataWidth-1:0] iDatA,
//output reg [DataWidth-1:0] oDatA,

//input wire iWE_B,
input wire [MemAddrWidth-1:0] iAddrB,
//input  wire [DataWidth-1:0] iDatB,
output reg [DataWidth-1:0] oDatB
);

(*RAM_STYLE= "BLOCK" *)
reg [DataWidth-1:0] ram [MemAddrNum-1:0];
reg [DataWidth-1:0] rDatB;
//******************************************************************

//wire [MemAddrWidth-1:0] wAddrA = iAddrA;
//wire [ADDR_WIDTH-1:0] wAddrA = (iAddrAy*XMax[XAddrSize-1:0])+iAddrAx;  //Applying 2D to 1D Address Translation to avoid wasting memory cells on usued 2D addresses. 
//wire [ADDR_WIDTH-1:0] wAddrA = {iAddrAy/*-YSkip[5:0]*/,iAddrAx}; // 2D Address, Note that Memory cells allocated to unused address space is wasted.
always @ (posedge iClk_A)
begin
	//oDatA <= ram[iAddrA];
	if (iWE_A) ram[iAddrA] <= iDatA;
end

//wire [MemAddrWidth-1:0] wAddrB = iAddrB;
always @ (posedge iClk_A)
begin
	
	//if (UseOutReg) begin: Direct_Out
	//	rDatB <= ram[iAddrB];
	//	oDatB <= rDatB;
	//end else begin: Registered_Out
		rDatB <= ram[iAddrB];
		oDatB <= rDatB;
	//end
	//if (iWE_B) ram[iAddrB] <= iDatB;
end

endmodule // dualport_ram
`default_nettype wire

