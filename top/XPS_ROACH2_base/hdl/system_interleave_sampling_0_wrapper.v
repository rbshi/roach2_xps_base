//-----------------------------------------------------------------------------
// system_interleave_sampling_0_wrapper.v
//-----------------------------------------------------------------------------

module system_interleave_sampling_0_wrapper
  (
    clk,
    rst_n,
    en,
    data_in,
    data_out,
    wr_en
  );
  input clk;
  input rst_n;
  input en;
  input [255:0] data_in;
  output [255:0] data_out;
  output wr_en;

  interleave_sampling
    #(
      .BITS_PER_PIX ( 'h00000008 ),
      .PIXS_PER_BLK ( 'h00000020 )
    )
    interleave_sampling_0 (
      .clk ( clk ),
      .rst_n ( rst_n ),
      .en ( en ),
      .data_in ( data_in ),
      .data_out ( data_out ),
      .wr_en ( wr_en )
    );

endmodule

