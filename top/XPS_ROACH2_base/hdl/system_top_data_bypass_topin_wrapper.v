//-----------------------------------------------------------------------------
// system_top_data_bypass_topin_wrapper.v
//-----------------------------------------------------------------------------

module system_top_data_bypass_topin_wrapper
  (
    clk,
    rst_n,
    en,
    data_in,
    data_out,
    data_out_vld
  );
  input clk;
  input rst_n;
  input en;
  input [127:0] data_in;
  output [127:0] data_out;
  output data_out_vld;

  data_bypass_topin
    top_data_bypass_topin (
      .clk ( clk ),
      .rst_n ( rst_n ),
      .en ( en ),
      .data_in ( data_in ),
      .data_out ( data_out ),
      .data_out_vld ( data_out_vld )
    );

endmodule

