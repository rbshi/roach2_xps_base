//-----------------------------------------------------------------------------
// system_clk125_to_clk250_0_wrapper.v
//-----------------------------------------------------------------------------

module system_clk125_to_clk250_0_wrapper
  (
    clk_250,
    clk_125,
    data_in,
    data_in_vld,
    rst_n,
    data_out,
    data_out_vld
  );
  input clk_250;
  input clk_125;
  input [255:0] data_in;
  input data_in_vld;
  input rst_n;
  output [127:0] data_out;
  output data_out_vld;

  clk125_to_clk250
    clk125_to_clk250_0 (
      .clk_250 ( clk_250 ),
      .clk_125 ( clk_125 ),
      .data_in ( data_in ),
      .data_in_vld ( data_in_vld ),
      .rst_n ( rst_n ),
      .data_out ( data_out ),
      .data_out_vld ( data_out_vld )
    );

endmodule

