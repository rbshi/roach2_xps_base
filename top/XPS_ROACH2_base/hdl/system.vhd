-------------------------------------------------------------------------------
-- system.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity system is
  port (
    sys_clk_n : in std_logic;
    sys_clk_p : in std_logic;
    aux_clk_n : in std_logic;
    aux_clk_p : in std_logic;
    epb_clk_in : in std_logic;
    epb_data : inout std_logic_vector(0 to 31);
    epb_addr : in std_logic_vector(5 to 29);
    epb_cs_n : in std_logic;
    epb_be_n : in std_logic_vector(0 to 3);
    epb_r_w_n : in std_logic;
    epb_oe_n : in std_logic;
    epb_doe_n : out std_logic;
    epb_rdy : out std_logic;
    ppc_irq_n : out std_logic;
    xaui_refclk_n : in std_logic_vector(2 downto 0);
    xaui_refclk_p : in std_logic_vector(2 downto 0);
    mgt_rx_n : in std_logic_vector(8*4-1 downto 0);
    mgt_rx_p : in std_logic_vector(8*4-1 downto 0);
    mgt_tx_n : out std_logic_vector(8*4-1 downto 0);
    mgt_tx_p : out std_logic_vector(8*4-1 downto 0);
    adc0_adc3wire_clk : out std_logic;
    adc0_adc3wire_data : out std_logic;
    adc0_adc3wire_data_o : in std_logic;
    adc0_adc3wire_spi_rst : out std_logic;
    adc0_modepin : out std_logic;
    adc0_reset : out std_logic;
    mgt_gpio : inout std_logic_vector(11 downto 0);
    adc0clk_p : in std_logic;
    adc0clk_n : in std_logic;
    adc0sync_p : in std_logic;
    adc0sync_n : in std_logic;
    adc0data0_p_i : in std_logic_vector(7 downto 0);
    adc0data0_n_i : in std_logic_vector(7 downto 0);
    adc0data1_p_i : in std_logic_vector(7 downto 0);
    adc0data1_n_i : in std_logic_vector(7 downto 0);
    adc0data2_p_i : in std_logic_vector(7 downto 0);
    adc0data2_n_i : in std_logic_vector(7 downto 0);
    adc0data3_p_i : in std_logic_vector(7 downto 0);
    adc0data3_n_i : in std_logic_vector(7 downto 0)
  );
end system;

architecture STRUCTURE of system is

  component system_xaui_infrastructure_inst_wrapper is
    port (
      reset : in std_logic;
      xaui_refclk_n : in std_logic_vector(2 downto 0);
      xaui_refclk_p : in std_logic_vector(2 downto 0);
      mgt_rx_n : in std_logic_vector(31 downto 0);
      mgt_rx_p : in std_logic_vector(31 downto 0);
      mgt_tx_n : out std_logic_vector(31 downto 0);
      mgt_tx_p : out std_logic_vector(31 downto 0);
      xaui_clk : out std_logic;
      mgt_tx_rst0 : in std_logic_vector(0 to 0);
      mgt_rx_rst0 : in std_logic_vector(0 to 0);
      mgt_txdata0 : in std_logic_vector(63 downto 0);
      mgt_txcharisk0 : in std_logic_vector(7 downto 0);
      mgt_rxdata0 : out std_logic_vector(63 downto 0);
      mgt_rxcharisk0 : out std_logic_vector(7 downto 0);
      mgt_rxcodecomma0 : out std_logic_vector(7 downto 0);
      mgt_rxencommaalign0 : in std_logic_vector(3 downto 0);
      mgt_rxenchansync0 : in std_logic_vector(0 to 0);
      mgt_rxsyncok0 : out std_logic_vector(3 downto 0);
      mgt_rxcodevalid0 : out std_logic_vector(7 downto 0);
      mgt_rxbufferr0 : out std_logic_vector(3 downto 0);
      mgt_rxlock0 : out std_logic_vector(3 downto 0);
      mgt_txpostemphasis0 : in std_logic_vector(4 downto 0);
      mgt_txpreemphasis0 : in std_logic_vector(3 downto 0);
      mgt_txdiffctrl0 : in std_logic_vector(3 downto 0);
      mgt_rxeqmix0 : in std_logic_vector(2 downto 0);
      mgt_tx_rst1 : in std_logic_vector(0 to 0);
      mgt_rx_rst1 : in std_logic_vector(0 to 0);
      mgt_txdata1 : in std_logic_vector(63 downto 0);
      mgt_txcharisk1 : in std_logic_vector(7 downto 0);
      mgt_rxdata1 : out std_logic_vector(63 downto 0);
      mgt_rxcharisk1 : out std_logic_vector(7 downto 0);
      mgt_rxcodecomma1 : out std_logic_vector(7 downto 0);
      mgt_rxencommaalign1 : in std_logic_vector(3 downto 0);
      mgt_rxenchansync1 : in std_logic_vector(0 to 0);
      mgt_rxsyncok1 : out std_logic_vector(3 downto 0);
      mgt_rxcodevalid1 : out std_logic_vector(7 downto 0);
      mgt_rxbufferr1 : out std_logic_vector(3 downto 0);
      mgt_rxlock1 : out std_logic_vector(3 downto 0);
      mgt_txpostemphasis1 : in std_logic_vector(4 downto 0);
      mgt_txpreemphasis1 : in std_logic_vector(3 downto 0);
      mgt_txdiffctrl1 : in std_logic_vector(3 downto 0);
      mgt_rxeqmix1 : in std_logic_vector(2 downto 0);
      mgt_tx_rst2 : in std_logic_vector(0 to 0);
      mgt_rx_rst2 : in std_logic_vector(0 to 0);
      mgt_txdata2 : in std_logic_vector(63 downto 0);
      mgt_txcharisk2 : in std_logic_vector(7 downto 0);
      mgt_rxdata2 : out std_logic_vector(63 downto 0);
      mgt_rxcharisk2 : out std_logic_vector(7 downto 0);
      mgt_rxcodecomma2 : out std_logic_vector(7 downto 0);
      mgt_rxencommaalign2 : in std_logic_vector(3 downto 0);
      mgt_rxenchansync2 : in std_logic_vector(0 to 0);
      mgt_rxsyncok2 : out std_logic_vector(3 downto 0);
      mgt_rxcodevalid2 : out std_logic_vector(7 downto 0);
      mgt_rxbufferr2 : out std_logic_vector(3 downto 0);
      mgt_rxlock2 : out std_logic_vector(3 downto 0);
      mgt_txpostemphasis2 : in std_logic_vector(4 downto 0);
      mgt_txpreemphasis2 : in std_logic_vector(3 downto 0);
      mgt_txdiffctrl2 : in std_logic_vector(3 downto 0);
      mgt_rxeqmix2 : in std_logic_vector(2 downto 0);
      mgt_tx_rst3 : in std_logic_vector(0 to 0);
      mgt_rx_rst3 : in std_logic_vector(0 to 0);
      mgt_txdata3 : in std_logic_vector(63 downto 0);
      mgt_txcharisk3 : in std_logic_vector(7 downto 0);
      mgt_rxdata3 : out std_logic_vector(63 downto 0);
      mgt_rxcharisk3 : out std_logic_vector(7 downto 0);
      mgt_rxcodecomma3 : out std_logic_vector(7 downto 0);
      mgt_rxencommaalign3 : in std_logic_vector(3 downto 0);
      mgt_rxenchansync3 : in std_logic_vector(0 to 0);
      mgt_rxsyncok3 : out std_logic_vector(3 downto 0);
      mgt_rxcodevalid3 : out std_logic_vector(7 downto 0);
      mgt_rxbufferr3 : out std_logic_vector(3 downto 0);
      mgt_rxlock3 : out std_logic_vector(3 downto 0);
      mgt_txpostemphasis3 : in std_logic_vector(4 downto 0);
      mgt_txpreemphasis3 : in std_logic_vector(3 downto 0);
      mgt_txdiffctrl3 : in std_logic_vector(3 downto 0);
      mgt_rxeqmix3 : in std_logic_vector(2 downto 0);
      mgt_tx_rst4 : in std_logic_vector(0 to 0);
      mgt_rx_rst4 : in std_logic_vector(0 to 0);
      mgt_txdata4 : in std_logic_vector(63 downto 0);
      mgt_txcharisk4 : in std_logic_vector(7 downto 0);
      mgt_rxdata4 : out std_logic_vector(63 downto 0);
      mgt_rxcharisk4 : out std_logic_vector(7 downto 0);
      mgt_rxcodecomma4 : out std_logic_vector(7 downto 0);
      mgt_rxencommaalign4 : in std_logic_vector(3 downto 0);
      mgt_rxenchansync4 : in std_logic_vector(0 to 0);
      mgt_rxsyncok4 : out std_logic_vector(3 downto 0);
      mgt_rxcodevalid4 : out std_logic_vector(7 downto 0);
      mgt_rxbufferr4 : out std_logic_vector(3 downto 0);
      mgt_rxlock4 : out std_logic_vector(3 downto 0);
      mgt_txpostemphasis4 : in std_logic_vector(4 downto 0);
      mgt_txpreemphasis4 : in std_logic_vector(3 downto 0);
      mgt_txdiffctrl4 : in std_logic_vector(3 downto 0);
      mgt_rxeqmix4 : in std_logic_vector(2 downto 0);
      mgt_tx_rst5 : in std_logic_vector(0 to 0);
      mgt_rx_rst5 : in std_logic_vector(0 to 0);
      mgt_txdata5 : in std_logic_vector(63 downto 0);
      mgt_txcharisk5 : in std_logic_vector(7 downto 0);
      mgt_rxdata5 : out std_logic_vector(63 downto 0);
      mgt_rxcharisk5 : out std_logic_vector(7 downto 0);
      mgt_rxcodecomma5 : out std_logic_vector(7 downto 0);
      mgt_rxencommaalign5 : in std_logic_vector(3 downto 0);
      mgt_rxenchansync5 : in std_logic_vector(0 to 0);
      mgt_rxsyncok5 : out std_logic_vector(3 downto 0);
      mgt_rxcodevalid5 : out std_logic_vector(7 downto 0);
      mgt_rxbufferr5 : out std_logic_vector(3 downto 0);
      mgt_rxlock5 : out std_logic_vector(3 downto 0);
      mgt_txpostemphasis5 : in std_logic_vector(4 downto 0);
      mgt_txpreemphasis5 : in std_logic_vector(3 downto 0);
      mgt_txdiffctrl5 : in std_logic_vector(3 downto 0);
      mgt_rxeqmix5 : in std_logic_vector(2 downto 0);
      mgt_tx_rst6 : in std_logic_vector(0 to 0);
      mgt_rx_rst6 : in std_logic_vector(0 to 0);
      mgt_txdata6 : in std_logic_vector(63 downto 0);
      mgt_txcharisk6 : in std_logic_vector(7 downto 0);
      mgt_rxdata6 : out std_logic_vector(63 downto 0);
      mgt_rxcharisk6 : out std_logic_vector(7 downto 0);
      mgt_rxcodecomma6 : out std_logic_vector(7 downto 0);
      mgt_rxencommaalign6 : in std_logic_vector(3 downto 0);
      mgt_rxenchansync6 : in std_logic_vector(0 to 0);
      mgt_rxsyncok6 : out std_logic_vector(3 downto 0);
      mgt_rxcodevalid6 : out std_logic_vector(7 downto 0);
      mgt_rxbufferr6 : out std_logic_vector(3 downto 0);
      mgt_rxlock6 : out std_logic_vector(3 downto 0);
      mgt_txpostemphasis6 : in std_logic_vector(4 downto 0);
      mgt_txpreemphasis6 : in std_logic_vector(3 downto 0);
      mgt_txdiffctrl6 : in std_logic_vector(3 downto 0);
      mgt_rxeqmix6 : in std_logic_vector(2 downto 0);
      mgt_tx_rst7 : in std_logic_vector(0 to 0);
      mgt_rx_rst7 : in std_logic_vector(0 to 0);
      mgt_txdata7 : in std_logic_vector(63 downto 0);
      mgt_txcharisk7 : in std_logic_vector(7 downto 0);
      mgt_rxdata7 : out std_logic_vector(63 downto 0);
      mgt_rxcharisk7 : out std_logic_vector(7 downto 0);
      mgt_rxcodecomma7 : out std_logic_vector(7 downto 0);
      mgt_rxencommaalign7 : in std_logic_vector(3 downto 0);
      mgt_rxenchansync7 : in std_logic_vector(0 to 0);
      mgt_rxsyncok7 : out std_logic_vector(3 downto 0);
      mgt_rxcodevalid7 : out std_logic_vector(7 downto 0);
      mgt_rxbufferr7 : out std_logic_vector(3 downto 0);
      mgt_rxlock7 : out std_logic_vector(3 downto 0);
      mgt_txpostemphasis7 : in std_logic_vector(4 downto 0);
      mgt_txpreemphasis7 : in std_logic_vector(3 downto 0);
      mgt_txdiffctrl7 : in std_logic_vector(3 downto 0);
      mgt_rxeqmix7 : in std_logic_vector(2 downto 0)
    );
  end component;

  component system_opb_adc5g_controller_0_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      adc0_adc3wire_clk : out std_logic;
      adc0_adc3wire_data : out std_logic;
      adc0_adc3wire_data_o : in std_logic;
      adc0_adc3wire_spi_rst : out std_logic;
      adc0_modepin : out std_logic;
      adc0_reset : out std_logic;
      adc0_dcm_reset : out std_logic;
      adc0_dcm_locked : in std_logic;
      adc0_fifo_full_cnt : in std_logic_vector(15 downto 0);
      adc0_fifo_empty_cnt : in std_logic_vector(15 downto 0);
      adc0_psclk : out std_logic;
      adc0_psen : out std_logic;
      adc0_psincdec : out std_logic;
      adc0_psdone : in std_logic;
      adc0_clk : in std_logic;
      adc0_datain_pin : out std_logic_vector(4 downto 0);
      adc0_datain_tap : out std_logic_vector(4 downto 0);
      adc0_tap_rst : out std_logic;
      adc1_adc3wire_clk : out std_logic;
      adc1_adc3wire_data : out std_logic;
      adc1_adc3wire_data_o : in std_logic;
      adc1_adc3wire_spi_rst : out std_logic;
      adc1_modepin : out std_logic;
      adc1_reset : out std_logic;
      adc1_dcm_reset : out std_logic;
      adc1_dcm_locked : in std_logic;
      adc1_fifo_full_cnt : in std_logic_vector(15 downto 0);
      adc1_fifo_empty_cnt : in std_logic_vector(15 downto 0);
      adc1_psclk : out std_logic;
      adc1_psen : out std_logic;
      adc1_psincdec : out std_logic;
      adc1_psdone : in std_logic;
      adc1_clk : in std_logic;
      adc1_datain_pin : out std_logic_vector(4 downto 0);
      adc1_datain_tap : out std_logic_vector(4 downto 0);
      adc1_tap_rst : out std_logic
    );
  end component;

  component system_infrastructure_inst_wrapper is
    port (
      sys_clk_n : in std_logic;
      sys_clk_p : in std_logic;
      aux_clk_n : in std_logic;
      aux_clk_p : in std_logic;
      epb_clk_in : in std_logic;
      sys_clk : out std_logic;
      sys_clk90 : out std_logic;
      sys_clk180 : out std_logic;
      sys_clk270 : out std_logic;
      sys_clk_lock : out std_logic;
      sys_clk2x : out std_logic;
      sys_clk2x90 : out std_logic;
      sys_clk2x180 : out std_logic;
      sys_clk2x270 : out std_logic;
      aux_clk : out std_logic;
      aux_clk90 : out std_logic;
      aux_clk180 : out std_logic;
      aux_clk270 : out std_logic;
      aux_clk2x : out std_logic;
      aux_clk2x90 : out std_logic;
      aux_clk2x180 : out std_logic;
      aux_clk2x270 : out std_logic;
      epb_clk : out std_logic;
      idelay_rst : in std_logic;
      idelay_rdy : out std_logic;
      op_power_on_rst : out std_logic;
      clk_200 : out std_logic;
      clk_100 : out std_logic
    );
  end component;

  component system_reset_block_inst_wrapper is
    port (
      clk : in std_logic;
      ip_async_reset_i : in std_logic;
      ip_reset_i : in std_logic;
      op_reset_o : out std_logic
    );
  end component;

  component system_opb0_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : out std_logic;
      SYS_Rst : in std_logic;
      Debug_SYS_Rst : in std_logic;
      WDT_Rst : in std_logic;
      M_ABus : in std_logic_vector(0 to 31);
      M_BE : in std_logic_vector(0 to 3);
      M_beXfer : in std_logic_vector(0 to 0);
      M_busLock : in std_logic_vector(0 to 0);
      M_DBus : in std_logic_vector(0 to 31);
      M_DBusEn : in std_logic_vector(0 to 0);
      M_DBusEn32_63 : in std_logic_vector(0 to 0);
      M_dwXfer : in std_logic_vector(0 to 0);
      M_fwXfer : in std_logic_vector(0 to 0);
      M_hwXfer : in std_logic_vector(0 to 0);
      M_request : in std_logic_vector(0 to 0);
      M_RNW : in std_logic_vector(0 to 0);
      M_select : in std_logic_vector(0 to 0);
      M_seqAddr : in std_logic_vector(0 to 0);
      Sl_beAck : in std_logic_vector(0 to 19);
      Sl_DBus : in std_logic_vector(0 to 639);
      Sl_DBusEn : in std_logic_vector(0 to 19);
      Sl_DBusEn32_63 : in std_logic_vector(0 to 19);
      Sl_errAck : in std_logic_vector(0 to 19);
      Sl_dwAck : in std_logic_vector(0 to 19);
      Sl_fwAck : in std_logic_vector(0 to 19);
      Sl_hwAck : in std_logic_vector(0 to 19);
      Sl_retry : in std_logic_vector(0 to 19);
      Sl_toutSup : in std_logic_vector(0 to 19);
      Sl_xferAck : in std_logic_vector(0 to 19);
      OPB_MRequest : out std_logic_vector(0 to 0);
      OPB_ABus : out std_logic_vector(0 to 31);
      OPB_BE : out std_logic_vector(0 to 3);
      OPB_beXfer : out std_logic;
      OPB_beAck : out std_logic;
      OPB_busLock : out std_logic;
      OPB_rdDBus : out std_logic_vector(0 to 31);
      OPB_wrDBus : out std_logic_vector(0 to 31);
      OPB_DBus : out std_logic_vector(0 to 31);
      OPB_errAck : out std_logic;
      OPB_dwAck : out std_logic;
      OPB_dwXfer : out std_logic;
      OPB_fwAck : out std_logic;
      OPB_fwXfer : out std_logic;
      OPB_hwAck : out std_logic;
      OPB_hwXfer : out std_logic;
      OPB_MGrant : out std_logic_vector(0 to 0);
      OPB_pendReq : out std_logic_vector(0 to 0);
      OPB_retry : out std_logic;
      OPB_RNW : out std_logic;
      OPB_select : out std_logic;
      OPB_seqAddr : out std_logic;
      OPB_timeout : out std_logic;
      OPB_toutSup : out std_logic;
      OPB_xferAck : out std_logic
    );
  end component;

  component system_epb_opb_bridge_inst_wrapper is
    port (
      epb_clk : in std_logic;
      epb_doe_n : out std_logic;
      epb_data_oe_n : out std_logic;
      epb_cs_n : in std_logic;
      epb_oe_n : in std_logic;
      epb_r_w_n : in std_logic;
      epb_be_n : in std_logic_vector(3 downto 0);
      epb_addr : in std_logic_vector(5 to 29);
      epb_data_i : in std_logic_vector(0 to 31);
      epb_data_o : out std_logic_vector(0 to 31);
      epb_rdy : out std_logic;
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      M_request : out std_logic;
      M_busLock : out std_logic;
      M_select : out std_logic;
      M_RNW : out std_logic;
      M_BE : out std_logic_vector(0 to 3);
      M_seqAddr : out std_logic;
      M_DBus : out std_logic_vector(0 to 31);
      M_ABus : out std_logic_vector(0 to 31);
      OPB_MGrant : in std_logic;
      OPB_xferAck : in std_logic;
      OPB_errAck : in std_logic;
      OPB_retry : in std_logic;
      OPB_timeout : in std_logic;
      OPB_DBus : in std_logic_vector(0 to 31)
    );
  end component;

  component system_epb_infrastructure_inst_wrapper is
    port (
      epb_data_buf : inout std_logic_vector(31 downto 0);
      epb_data_oe_n_i : in std_logic;
      epb_data_out_i : in std_logic_vector(31 downto 0);
      epb_data_in_o : out std_logic_vector(31 downto 0)
    );
  end component;

  component system_sys_block_inst_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      soft_reset : out std_logic;
      irq_n : out std_logic;
      app_irq : in std_logic_vector(15 downto 0);
      fab_clk : in std_logic
    );
  end component;

  component system_sfp_mdio_controller_inst_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      mgt_gpio : inout std_logic_vector(11 downto 0)
    );
  end component;

  component system_top_xsg_core_config_wrapper is
    port (
      clk : in std_logic;
      top_adc_adc_sync : in std_logic;
      top_adc_adc_user_data_i0 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i1 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i2 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i3 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i4 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i5 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i6 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i7 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q0 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q1 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q2 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q3 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q4 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q5 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q6 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q7 : in std_logic_vector(7 downto 0);
      top_adc_enable_user_data_out : in std_logic_vector(31 downto 0);
      top_data_bypass_topin_data_out : in std_logic_vector(127 downto 0);
      top_data_bypass_topin_data_out_vld : in std_logic;
      top_data_bypass_topin_data_in : out std_logic_vector(127 downto 0);
      top_data_bypass_topin_en : out std_logic;
      top_data_bypass_topin_rst_n : out std_logic;
      top_data_bypass_topout_data_out : in std_logic_vector(127 downto 0);
      top_data_bypass_topout_data_out_vld : in std_logic;
      top_data_bypass_topout_data_in : out std_logic_vector(127 downto 0);
      top_data_bypass_topout_en : out std_logic;
      top_data_bypass_topout_rst_n : out std_logic;
      top_ethout_dest_ip0_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_ip1_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_ip2_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_ip3_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_port0_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_port1_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_port2_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_port3_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_rst_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe0_led_rx : in std_logic;
      top_ethout_ten_Gbe0_led_tx : in std_logic;
      top_ethout_ten_Gbe0_led_up : in std_logic;
      top_ethout_ten_Gbe0_rx_bad_frame : in std_logic;
      top_ethout_ten_Gbe0_rx_data : in std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe0_rx_end_of_frame : in std_logic;
      top_ethout_ten_Gbe0_rx_overrun : in std_logic;
      top_ethout_ten_Gbe0_rx_source_ip : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe0_rx_source_port : in std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe0_rx_valid : in std_logic;
      top_ethout_ten_Gbe0_tx_afull : in std_logic;
      top_ethout_ten_Gbe0_tx_overflow : in std_logic;
      top_ethout_ten_Gbe0_rst : out std_logic;
      top_ethout_ten_Gbe0_rx_ack : out std_logic;
      top_ethout_ten_Gbe0_rx_overrun_ack : out std_logic;
      top_ethout_ten_Gbe0_tx_data : out std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe0_tx_dest_ip : out std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe0_tx_dest_port : out std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe0_tx_end_of_frame : out std_logic;
      top_ethout_ten_Gbe0_tx_valid : out std_logic;
      top_ethout_ten_Gbe1_led_rx : in std_logic;
      top_ethout_ten_Gbe1_led_tx : in std_logic;
      top_ethout_ten_Gbe1_led_up : in std_logic;
      top_ethout_ten_Gbe1_rx_bad_frame : in std_logic;
      top_ethout_ten_Gbe1_rx_data : in std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe1_rx_end_of_frame : in std_logic;
      top_ethout_ten_Gbe1_rx_overrun : in std_logic;
      top_ethout_ten_Gbe1_rx_source_ip : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe1_rx_source_port : in std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe1_rx_valid : in std_logic;
      top_ethout_ten_Gbe1_tx_afull : in std_logic;
      top_ethout_ten_Gbe1_tx_overflow : in std_logic;
      top_ethout_ten_Gbe1_rst : out std_logic;
      top_ethout_ten_Gbe1_rx_ack : out std_logic;
      top_ethout_ten_Gbe1_rx_overrun_ack : out std_logic;
      top_ethout_ten_Gbe1_tx_data : out std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe1_tx_dest_ip : out std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe1_tx_dest_port : out std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe1_tx_end_of_frame : out std_logic;
      top_ethout_ten_Gbe1_tx_valid : out std_logic;
      top_ethout_ten_Gbe2_led_rx : in std_logic;
      top_ethout_ten_Gbe2_led_tx : in std_logic;
      top_ethout_ten_Gbe2_led_up : in std_logic;
      top_ethout_ten_Gbe2_rx_bad_frame : in std_logic;
      top_ethout_ten_Gbe2_rx_data : in std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe2_rx_end_of_frame : in std_logic;
      top_ethout_ten_Gbe2_rx_overrun : in std_logic;
      top_ethout_ten_Gbe2_rx_source_ip : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe2_rx_source_port : in std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe2_rx_valid : in std_logic;
      top_ethout_ten_Gbe2_tx_afull : in std_logic;
      top_ethout_ten_Gbe2_tx_overflow : in std_logic;
      top_ethout_ten_Gbe2_rst : out std_logic;
      top_ethout_ten_Gbe2_rx_ack : out std_logic;
      top_ethout_ten_Gbe2_rx_overrun_ack : out std_logic;
      top_ethout_ten_Gbe2_tx_data : out std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe2_tx_dest_ip : out std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe2_tx_dest_port : out std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe2_tx_end_of_frame : out std_logic;
      top_ethout_ten_Gbe2_tx_valid : out std_logic;
      top_ethout_ten_Gbe3_led_rx : in std_logic;
      top_ethout_ten_Gbe3_led_tx : in std_logic;
      top_ethout_ten_Gbe3_led_up : in std_logic;
      top_ethout_ten_Gbe3_rx_bad_frame : in std_logic;
      top_ethout_ten_Gbe3_rx_data : in std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe3_rx_end_of_frame : in std_logic;
      top_ethout_ten_Gbe3_rx_overrun : in std_logic;
      top_ethout_ten_Gbe3_rx_source_ip : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe3_rx_source_port : in std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe3_rx_valid : in std_logic;
      top_ethout_ten_Gbe3_tx_afull : in std_logic;
      top_ethout_ten_Gbe3_tx_overflow : in std_logic;
      top_ethout_ten_Gbe3_rst : out std_logic;
      top_ethout_ten_Gbe3_rx_ack : out std_logic;
      top_ethout_ten_Gbe3_rx_overrun_ack : out std_logic;
      top_ethout_ten_Gbe3_tx_data : out std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe3_tx_dest_ip : out std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe3_tx_dest_port : out std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe3_tx_end_of_frame : out std_logic;
      top_ethout_ten_Gbe3_tx_valid : out std_logic;
      top_scope_raw_0_snap_bram_data_out : in std_logic_vector(127 downto 0);
      top_scope_raw_0_snap_bram_addr : out std_logic_vector(9 downto 0);
      top_scope_raw_0_snap_bram_data_in : out std_logic_vector(127 downto 0);
      top_scope_raw_0_snap_bram_we : out std_logic;
      top_scope_raw_0_snap_ctrl_user_data_out : in std_logic_vector(31 downto 0);
      top_scope_raw_0_snap_status_user_data_in : out std_logic_vector(31 downto 0)
    );
  end component;

  component system_top_adc_adc_wrapper is
    port (
      adc_clk_p_i : in std_logic;
      adc_clk_n_i : in std_logic;
      adc_data0_p_i : in std_logic_vector(7 downto 0);
      adc_data0_n_i : in std_logic_vector(7 downto 0);
      adc_data1_p_i : in std_logic_vector(7 downto 0);
      adc_data1_n_i : in std_logic_vector(7 downto 0);
      adc_data2_p_i : in std_logic_vector(7 downto 0);
      adc_data2_n_i : in std_logic_vector(7 downto 0);
      adc_data3_p_i : in std_logic_vector(7 downto 0);
      adc_data3_n_i : in std_logic_vector(7 downto 0);
      adc_reset_o : out std_logic;
      adc_sync_p : in std_logic;
      adc_sync_n : in std_logic;
      sync : out std_logic;
      user_data_i0 : out std_logic_vector(7 downto 0);
      user_data_i1 : out std_logic_vector(7 downto 0);
      user_data_i2 : out std_logic_vector(7 downto 0);
      user_data_i3 : out std_logic_vector(7 downto 0);
      user_data_i4 : out std_logic_vector(7 downto 0);
      user_data_i5 : out std_logic_vector(7 downto 0);
      user_data_i6 : out std_logic_vector(7 downto 0);
      user_data_i7 : out std_logic_vector(7 downto 0);
      user_data_q0 : out std_logic_vector(7 downto 0);
      user_data_q1 : out std_logic_vector(7 downto 0);
      user_data_q2 : out std_logic_vector(7 downto 0);
      user_data_q3 : out std_logic_vector(7 downto 0);
      user_data_q4 : out std_logic_vector(7 downto 0);
      user_data_q5 : out std_logic_vector(7 downto 0);
      user_data_q6 : out std_logic_vector(7 downto 0);
      user_data_q7 : out std_logic_vector(7 downto 0);
      dcm_reset : in std_logic;
      ctrl_reset : in std_logic;
      ctrl_clk_in : in std_logic;
      ctrl_clk_out : out std_logic;
      ctrl_clk90_out : out std_logic;
      ctrl_clk180_out : out std_logic;
      ctrl_clk270_out : out std_logic;
      ctrl_clk125m_out : out std_logic;
      ctrl_dcm_locked : out std_logic;
      fifo_full_cnt : out std_logic_vector(15 downto 0);
      fifo_empty_cnt : out std_logic_vector(15 downto 0);
      dcm_psclk : in std_logic;
      dcm_psen : in std_logic;
      dcm_psincdec : in std_logic;
      dcm_psdone : out std_logic;
      datain_pin : in std_logic_vector(4 downto 0);
      datain_tap : in std_logic_vector(4 downto 0);
      tap_rst : in std_logic
    );
  end component;

  component system_top_adc_enable_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_data_bypass_topin_wrapper is
    port (
      clk : in std_logic;
      rst_n : in std_logic;
      en : in std_logic;
      data_in : in std_logic_vector(127 downto 0);
      data_out : out std_logic_vector(127 downto 0);
      data_out_vld : out std_logic
    );
  end component;

  component system_top_data_bypass_topout_wrapper is
    port (
      clk : in std_logic;
      rst_n : in std_logic;
      en : in std_logic;
      data_in : in std_logic_vector(127 downto 0);
      data_out : out std_logic_vector(127 downto 0);
      data_out_vld : out std_logic
    );
  end component;

  component system_top_ethout_dest_ip0_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_ethout_dest_ip1_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_ethout_dest_ip2_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_ethout_dest_ip3_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_ethout_dest_port0_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_ethout_dest_port1_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_ethout_dest_port2_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_ethout_dest_port3_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_ethout_rst_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_ethout_ten_gbe0_wrapper is
    port (
      clk : in std_logic;
      rst : in std_logic;
      tx_valid : in std_logic;
      tx_afull : out std_logic;
      tx_overflow : out std_logic;
      tx_end_of_frame : in std_logic;
      tx_data : in std_logic_vector(63 downto 0);
      tx_dest_ip : in std_logic_vector(31 downto 0);
      tx_dest_port : in std_logic_vector(15 downto 0);
      rx_valid : out std_logic;
      rx_end_of_frame : out std_logic;
      rx_data : out std_logic_vector(63 downto 0);
      rx_source_ip : out std_logic_vector(31 downto 0);
      rx_source_port : out std_logic_vector(15 downto 0);
      rx_bad_frame : out std_logic;
      rx_overrun : out std_logic;
      rx_overrun_ack : in std_logic;
      rx_ack : in std_logic;
      led_up : out std_logic;
      led_rx : out std_logic;
      led_tx : out std_logic;
      xaui_clk : in std_logic;
      xgmii_txd : out std_logic_vector(63 downto 0);
      xgmii_txc : out std_logic_vector(7 downto 0);
      xgmii_rxd : in std_logic_vector(63 downto 0);
      xgmii_rxc : in std_logic_vector(7 downto 0);
      xaui_reset : in std_logic;
      mgt_txpostemphasis : out std_logic_vector(4 downto 0);
      mgt_txpreemphasis : out std_logic_vector(3 downto 0);
      mgt_txdiffctrl : out std_logic_vector(3 downto 0);
      mgt_rxeqmix : out std_logic_vector(2 downto 0);
      xaui_status : in std_logic_vector(7 downto 0);
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic
    );
  end component;

  component system_xaui_phy_0_wrapper is
    port (
      reset : in std_logic;
      xaui_clk : in std_logic;
      mgt_txdata : out std_logic_vector(63 downto 0);
      mgt_txcharisk : out std_logic_vector(7 downto 0);
      mgt_rxdata : in std_logic_vector(63 downto 0);
      mgt_rxcharisk : in std_logic_vector(7 downto 0);
      mgt_enable_align : out std_logic_vector(3 downto 0);
      mgt_code_valid : in std_logic_vector(7 downto 0);
      mgt_code_comma : in std_logic_vector(7 downto 0);
      mgt_rxlock : in std_logic_vector(3 downto 0);
      mgt_rxbufferr : in std_logic_vector(3 downto 0);
      mgt_syncok : in std_logic_vector(3 downto 0);
      mgt_en_chan_sync : out std_logic;
      mgt_rx_reset : out std_logic;
      mgt_tx_reset : out std_logic;
      xaui_status : out std_logic_vector(7 downto 0);
      xgmii_txd : in std_logic_vector(63 downto 0);
      xgmii_txc : in std_logic_vector(7 downto 0);
      xgmii_rxd : out std_logic_vector(63 downto 0);
      xgmii_rxc : out std_logic_vector(7 downto 0)
    );
  end component;

  component system_top_ethout_ten_gbe1_wrapper is
    port (
      clk : in std_logic;
      rst : in std_logic;
      tx_valid : in std_logic;
      tx_afull : out std_logic;
      tx_overflow : out std_logic;
      tx_end_of_frame : in std_logic;
      tx_data : in std_logic_vector(63 downto 0);
      tx_dest_ip : in std_logic_vector(31 downto 0);
      tx_dest_port : in std_logic_vector(15 downto 0);
      rx_valid : out std_logic;
      rx_end_of_frame : out std_logic;
      rx_data : out std_logic_vector(63 downto 0);
      rx_source_ip : out std_logic_vector(31 downto 0);
      rx_source_port : out std_logic_vector(15 downto 0);
      rx_bad_frame : out std_logic;
      rx_overrun : out std_logic;
      rx_overrun_ack : in std_logic;
      rx_ack : in std_logic;
      led_up : out std_logic;
      led_rx : out std_logic;
      led_tx : out std_logic;
      xaui_clk : in std_logic;
      xgmii_txd : out std_logic_vector(63 downto 0);
      xgmii_txc : out std_logic_vector(7 downto 0);
      xgmii_rxd : in std_logic_vector(63 downto 0);
      xgmii_rxc : in std_logic_vector(7 downto 0);
      xaui_reset : in std_logic;
      mgt_txpostemphasis : out std_logic_vector(4 downto 0);
      mgt_txpreemphasis : out std_logic_vector(3 downto 0);
      mgt_txdiffctrl : out std_logic_vector(3 downto 0);
      mgt_rxeqmix : out std_logic_vector(2 downto 0);
      xaui_status : in std_logic_vector(7 downto 0);
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic
    );
  end component;

  component system_xaui_phy_1_wrapper is
    port (
      reset : in std_logic;
      xaui_clk : in std_logic;
      mgt_txdata : out std_logic_vector(63 downto 0);
      mgt_txcharisk : out std_logic_vector(7 downto 0);
      mgt_rxdata : in std_logic_vector(63 downto 0);
      mgt_rxcharisk : in std_logic_vector(7 downto 0);
      mgt_enable_align : out std_logic_vector(3 downto 0);
      mgt_code_valid : in std_logic_vector(7 downto 0);
      mgt_code_comma : in std_logic_vector(7 downto 0);
      mgt_rxlock : in std_logic_vector(3 downto 0);
      mgt_rxbufferr : in std_logic_vector(3 downto 0);
      mgt_syncok : in std_logic_vector(3 downto 0);
      mgt_en_chan_sync : out std_logic;
      mgt_rx_reset : out std_logic;
      mgt_tx_reset : out std_logic;
      xaui_status : out std_logic_vector(7 downto 0);
      xgmii_txd : in std_logic_vector(63 downto 0);
      xgmii_txc : in std_logic_vector(7 downto 0);
      xgmii_rxd : out std_logic_vector(63 downto 0);
      xgmii_rxc : out std_logic_vector(7 downto 0)
    );
  end component;

  component system_top_ethout_ten_gbe2_wrapper is
    port (
      clk : in std_logic;
      rst : in std_logic;
      tx_valid : in std_logic;
      tx_afull : out std_logic;
      tx_overflow : out std_logic;
      tx_end_of_frame : in std_logic;
      tx_data : in std_logic_vector(63 downto 0);
      tx_dest_ip : in std_logic_vector(31 downto 0);
      tx_dest_port : in std_logic_vector(15 downto 0);
      rx_valid : out std_logic;
      rx_end_of_frame : out std_logic;
      rx_data : out std_logic_vector(63 downto 0);
      rx_source_ip : out std_logic_vector(31 downto 0);
      rx_source_port : out std_logic_vector(15 downto 0);
      rx_bad_frame : out std_logic;
      rx_overrun : out std_logic;
      rx_overrun_ack : in std_logic;
      rx_ack : in std_logic;
      led_up : out std_logic;
      led_rx : out std_logic;
      led_tx : out std_logic;
      xaui_clk : in std_logic;
      xgmii_txd : out std_logic_vector(63 downto 0);
      xgmii_txc : out std_logic_vector(7 downto 0);
      xgmii_rxd : in std_logic_vector(63 downto 0);
      xgmii_rxc : in std_logic_vector(7 downto 0);
      xaui_reset : in std_logic;
      mgt_txpostemphasis : out std_logic_vector(4 downto 0);
      mgt_txpreemphasis : out std_logic_vector(3 downto 0);
      mgt_txdiffctrl : out std_logic_vector(3 downto 0);
      mgt_rxeqmix : out std_logic_vector(2 downto 0);
      xaui_status : in std_logic_vector(7 downto 0);
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic
    );
  end component;

  component system_xaui_phy_2_wrapper is
    port (
      reset : in std_logic;
      xaui_clk : in std_logic;
      mgt_txdata : out std_logic_vector(63 downto 0);
      mgt_txcharisk : out std_logic_vector(7 downto 0);
      mgt_rxdata : in std_logic_vector(63 downto 0);
      mgt_rxcharisk : in std_logic_vector(7 downto 0);
      mgt_enable_align : out std_logic_vector(3 downto 0);
      mgt_code_valid : in std_logic_vector(7 downto 0);
      mgt_code_comma : in std_logic_vector(7 downto 0);
      mgt_rxlock : in std_logic_vector(3 downto 0);
      mgt_rxbufferr : in std_logic_vector(3 downto 0);
      mgt_syncok : in std_logic_vector(3 downto 0);
      mgt_en_chan_sync : out std_logic;
      mgt_rx_reset : out std_logic;
      mgt_tx_reset : out std_logic;
      xaui_status : out std_logic_vector(7 downto 0);
      xgmii_txd : in std_logic_vector(63 downto 0);
      xgmii_txc : in std_logic_vector(7 downto 0);
      xgmii_rxd : out std_logic_vector(63 downto 0);
      xgmii_rxc : out std_logic_vector(7 downto 0)
    );
  end component;

  component system_top_ethout_ten_gbe3_wrapper is
    port (
      clk : in std_logic;
      rst : in std_logic;
      tx_valid : in std_logic;
      tx_afull : out std_logic;
      tx_overflow : out std_logic;
      tx_end_of_frame : in std_logic;
      tx_data : in std_logic_vector(63 downto 0);
      tx_dest_ip : in std_logic_vector(31 downto 0);
      tx_dest_port : in std_logic_vector(15 downto 0);
      rx_valid : out std_logic;
      rx_end_of_frame : out std_logic;
      rx_data : out std_logic_vector(63 downto 0);
      rx_source_ip : out std_logic_vector(31 downto 0);
      rx_source_port : out std_logic_vector(15 downto 0);
      rx_bad_frame : out std_logic;
      rx_overrun : out std_logic;
      rx_overrun_ack : in std_logic;
      rx_ack : in std_logic;
      led_up : out std_logic;
      led_rx : out std_logic;
      led_tx : out std_logic;
      xaui_clk : in std_logic;
      xgmii_txd : out std_logic_vector(63 downto 0);
      xgmii_txc : out std_logic_vector(7 downto 0);
      xgmii_rxd : in std_logic_vector(63 downto 0);
      xgmii_rxc : in std_logic_vector(7 downto 0);
      xaui_reset : in std_logic;
      mgt_txpostemphasis : out std_logic_vector(4 downto 0);
      mgt_txpreemphasis : out std_logic_vector(3 downto 0);
      mgt_txdiffctrl : out std_logic_vector(3 downto 0);
      mgt_rxeqmix : out std_logic_vector(2 downto 0);
      xaui_status : in std_logic_vector(7 downto 0);
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic
    );
  end component;

  component system_xaui_phy_3_wrapper is
    port (
      reset : in std_logic;
      xaui_clk : in std_logic;
      mgt_txdata : out std_logic_vector(63 downto 0);
      mgt_txcharisk : out std_logic_vector(7 downto 0);
      mgt_rxdata : in std_logic_vector(63 downto 0);
      mgt_rxcharisk : in std_logic_vector(7 downto 0);
      mgt_enable_align : out std_logic_vector(3 downto 0);
      mgt_code_valid : in std_logic_vector(7 downto 0);
      mgt_code_comma : in std_logic_vector(7 downto 0);
      mgt_rxlock : in std_logic_vector(3 downto 0);
      mgt_rxbufferr : in std_logic_vector(3 downto 0);
      mgt_syncok : in std_logic_vector(3 downto 0);
      mgt_en_chan_sync : out std_logic;
      mgt_rx_reset : out std_logic;
      mgt_tx_reset : out std_logic;
      xaui_status : out std_logic_vector(7 downto 0);
      xgmii_txd : in std_logic_vector(63 downto 0);
      xgmii_txc : in std_logic_vector(7 downto 0);
      xgmii_rxd : out std_logic_vector(63 downto 0);
      xgmii_rxc : out std_logic_vector(7 downto 0)
    );
  end component;

  component system_top_scope_raw_0_snap_bram_ramblk_wrapper is
    port (
      clk : in std_logic;
      bram_we : in std_logic;
      bram_en_a : in std_logic;
      bram_addr : in std_logic_vector(9 downto 0);
      bram_rd_data : out std_logic_vector(127 downto 0);
      bram_wr_data : in std_logic_vector(127 downto 0);
      BRAM_Rst_B : in std_logic;
      BRAM_Clk_B : in std_logic;
      BRAM_EN_B : in std_logic;
      BRAM_WEN_B : in std_logic_vector(0 to 3);
      BRAM_Addr_B : in std_logic_vector(0 to 31);
      BRAM_Din_B : out std_logic_vector(0 to 31);
      BRAM_Dout_B : in std_logic_vector(0 to 31)
    );
  end component;

  component system_top_scope_raw_0_snap_bram_wrapper is
    port (
      opb_clk : in std_logic;
      opb_rst : in std_logic;
      opb_abus : in std_logic_vector(0 to 31);
      opb_dbus : in std_logic_vector(0 to 31);
      sln_dbus : out std_logic_vector(0 to 31);
      opb_select : in std_logic;
      opb_rnw : in std_logic;
      opb_seqaddr : in std_logic;
      opb_be : in std_logic_vector(0 to 3);
      sln_xferack : out std_logic;
      sln_errack : out std_logic;
      sln_toutsup : out std_logic;
      sln_retry : out std_logic;
      bram_rst : out std_logic;
      bram_clk : out std_logic;
      bram_en : out std_logic;
      bram_wen : out std_logic_vector(0 to 3);
      bram_addr : out std_logic_vector(0 to 31);
      bram_din : in std_logic_vector(0 to 31);
      bram_dout : out std_logic_vector(0 to 31)
    );
  end component;

  component system_top_scope_raw_0_snap_ctrl_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_out : out std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_top_scope_raw_0_snap_status_wrapper is
    port (
      OPB_Clk : in std_logic;
      OPB_Rst : in std_logic;
      Sl_DBus : out std_logic_vector(0 to 31);
      Sl_errAck : out std_logic;
      Sl_retry : out std_logic;
      Sl_toutSup : out std_logic;
      Sl_xferAck : out std_logic;
      OPB_ABus : in std_logic_vector(0 to 31);
      OPB_BE : in std_logic_vector(0 to 3);
      OPB_DBus : in std_logic_vector(0 to 31);
      OPB_RNW : in std_logic;
      OPB_select : in std_logic;
      OPB_seqAddr : in std_logic;
      user_data_in : in std_logic_vector(31 downto 0);
      user_clk : in std_logic
    );
  end component;

  component system_clk250_to_clk125_0_wrapper is
    port (
      clk_250 : in std_logic;
      clk_125 : in std_logic;
      data_in : in std_logic_vector(127 downto 0);
      data_in_vld : in std_logic;
      rst_n : in std_logic;
      data_out : out std_logic_vector(255 downto 0);
      data_out_vld : out std_logic
    );
  end component;

  component system_clk125_to_clk250_0_wrapper is
    port (
      clk_250 : in std_logic;
      clk_125 : in std_logic;
      data_in : in std_logic_vector(255 downto 0);
      data_in_vld : in std_logic;
      rst_n : in std_logic;
      data_out : out std_logic_vector(127 downto 0);
      data_out_vld : out std_logic
    );
  end component;

  component system_interleave_sampling_0_wrapper is
    port (
      clk : in std_logic;
      rst_n : in std_logic;
      en : in std_logic;
      data_in : in std_logic_vector(255 downto 0);
      data_out : out std_logic_vector(255 downto 0);
      wr_en : out std_logic
    );
  end component;

  -- Internal signals

  signal adc0_clk : std_logic;
  signal adc0_clk_lock : std_logic;
  signal adc0_datain_pin : std_logic_vector(4 downto 0);
  signal adc0_datain_tap : std_logic_vector(4 downto 0);
  signal adc0_dcm_reset : std_logic;
  signal adc0_fifo_empty_cnt : std_logic_vector(15 downto 0);
  signal adc0_fifo_full_cnt : std_logic_vector(15 downto 0);
  signal adc0_psclk : std_logic;
  signal adc0_psdone : std_logic;
  signal adc0_psen : std_logic;
  signal adc0_psincdec : std_logic;
  signal adc0_tap_rst : std_logic;
  signal clk125_to_clk250_0_data_out : std_logic_vector(127 downto 0);
  signal clk125_to_clk250_0_data_out_vld : std_logic;
  signal clk250_to_clk125_0_data_out : std_logic_vector(255 downto 0);
  signal clk250_to_clk125_0_data_out_vld : std_logic;
  signal epb_clk : std_logic;
  signal epb_data_i : std_logic_vector(0 to 31);
  signal epb_data_o : std_logic_vector(31 downto 0);
  signal epb_data_oe_n : std_logic;
  signal interleave_sampling_0_data_out : std_logic_vector(255 downto 0);
  signal interleave_sampling_0_wr_en : std_logic;
  signal net_gnd0 : std_logic;
  signal net_gnd1 : std_logic_vector(0 to 0);
  signal net_gnd3 : std_logic_vector(2 downto 0);
  signal net_gnd4 : std_logic_vector(3 downto 0);
  signal net_gnd5 : std_logic_vector(4 downto 0);
  signal net_gnd8 : std_logic_vector(7 downto 0);
  signal net_gnd16 : std_logic_vector(15 downto 0);
  signal net_gnd20 : std_logic_vector(0 to 19);
  signal net_gnd64 : std_logic_vector(63 downto 0);
  signal net_vcc1 : std_logic_vector(0 to 0);
  signal net_vcc20 : std_logic_vector(0 to 19);
  signal opb0_M_ABus : std_logic_vector(0 to 31);
  signal opb0_M_BE : std_logic_vector(0 to 3);
  signal opb0_M_DBus : std_logic_vector(0 to 31);
  signal opb0_M_RNW : std_logic_vector(0 to 0);
  signal opb0_M_busLock : std_logic_vector(0 to 0);
  signal opb0_M_request : std_logic_vector(0 to 0);
  signal opb0_M_select : std_logic_vector(0 to 0);
  signal opb0_M_seqAddr : std_logic_vector(0 to 0);
  signal opb0_OPB_ABus : std_logic_vector(0 to 31);
  signal opb0_OPB_BE : std_logic_vector(0 to 3);
  signal opb0_OPB_DBus : std_logic_vector(0 to 31);
  signal opb0_OPB_MGrant : std_logic_vector(0 to 0);
  signal opb0_OPB_RNW : std_logic;
  signal opb0_OPB_Rst : std_logic;
  signal opb0_OPB_errAck : std_logic;
  signal opb0_OPB_retry : std_logic;
  signal opb0_OPB_select : std_logic;
  signal opb0_OPB_seqAddr : std_logic;
  signal opb0_OPB_timeout : std_logic;
  signal opb0_OPB_xferAck : std_logic;
  signal opb0_Sl_DBus : std_logic_vector(0 to 639);
  signal opb0_Sl_errAck : std_logic_vector(0 to 19);
  signal opb0_Sl_retry : std_logic_vector(0 to 19);
  signal opb0_Sl_toutSup : std_logic_vector(0 to 19);
  signal opb0_Sl_xferAck : std_logic_vector(0 to 19);
  signal pgassign1 : std_logic_vector(15 downto 0);
  signal phy_conf0_mgt_rxeqmix : std_logic_vector(2 downto 0);
  signal phy_conf0_mgt_txdiffctrl : std_logic_vector(3 downto 0);
  signal phy_conf0_mgt_txpostemphasis : std_logic_vector(4 downto 0);
  signal phy_conf0_mgt_txpreemphasis : std_logic_vector(3 downto 0);
  signal phy_conf1_mgt_rxeqmix : std_logic_vector(2 downto 0);
  signal phy_conf1_mgt_txdiffctrl : std_logic_vector(3 downto 0);
  signal phy_conf1_mgt_txpostemphasis : std_logic_vector(4 downto 0);
  signal phy_conf1_mgt_txpreemphasis : std_logic_vector(3 downto 0);
  signal phy_conf2_mgt_rxeqmix : std_logic_vector(2 downto 0);
  signal phy_conf2_mgt_txdiffctrl : std_logic_vector(3 downto 0);
  signal phy_conf2_mgt_txpostemphasis : std_logic_vector(4 downto 0);
  signal phy_conf2_mgt_txpreemphasis : std_logic_vector(3 downto 0);
  signal phy_conf3_mgt_rxeqmix : std_logic_vector(2 downto 0);
  signal phy_conf3_mgt_txdiffctrl : std_logic_vector(3 downto 0);
  signal phy_conf3_mgt_txpostemphasis : std_logic_vector(4 downto 0);
  signal phy_conf3_mgt_txpreemphasis : std_logic_vector(3 downto 0);
  signal power_on_rst : std_logic;
  signal sys_clk : std_logic;
  signal sys_reset : std_logic;
  signal top_adc_adc_ctrl_clk125m_out : std_logic;
  signal top_adc_adc_sync : std_logic;
  signal top_adc_adc_user_data_i0 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i1 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i2 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i3 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i4 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i5 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i6 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_i7 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q0 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q1 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q2 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q3 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q4 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q5 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q6 : std_logic_vector(7 downto 0);
  signal top_adc_adc_user_data_q7 : std_logic_vector(7 downto 0);
  signal top_adc_enable_user_data_out : std_logic_vector(31 downto 0);
  signal top_data_bypass_topin_data_in : std_logic_vector(127 downto 0);
  signal top_data_bypass_topin_data_out : std_logic_vector(127 downto 0);
  signal top_data_bypass_topin_data_out_vld : std_logic;
  signal top_data_bypass_topin_en : std_logic;
  signal top_data_bypass_topin_rst_n : std_logic;
  signal top_data_bypass_topout_data_in : std_logic_vector(127 downto 0);
  signal top_data_bypass_topout_data_out : std_logic_vector(127 downto 0);
  signal top_data_bypass_topout_data_out_vld : std_logic;
  signal top_data_bypass_topout_en : std_logic;
  signal top_data_bypass_topout_rst_n : std_logic;
  signal top_ethout_dest_ip0_user_data_out : std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip1_user_data_out : std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip2_user_data_out : std_logic_vector(31 downto 0);
  signal top_ethout_dest_ip3_user_data_out : std_logic_vector(31 downto 0);
  signal top_ethout_dest_port0_user_data_out : std_logic_vector(31 downto 0);
  signal top_ethout_dest_port1_user_data_out : std_logic_vector(31 downto 0);
  signal top_ethout_dest_port2_user_data_out : std_logic_vector(31 downto 0);
  signal top_ethout_dest_port3_user_data_out : std_logic_vector(31 downto 0);
  signal top_ethout_rst_user_data_out : std_logic_vector(31 downto 0);
  signal top_ethout_ten_Gbe0_led_rx : std_logic;
  signal top_ethout_ten_Gbe0_led_tx : std_logic;
  signal top_ethout_ten_Gbe0_led_up : std_logic;
  signal top_ethout_ten_Gbe0_rst : std_logic;
  signal top_ethout_ten_Gbe0_rx_ack : std_logic;
  signal top_ethout_ten_Gbe0_rx_bad_frame : std_logic;
  signal top_ethout_ten_Gbe0_rx_data : std_logic_vector(63 downto 0);
  signal top_ethout_ten_Gbe0_rx_end_of_frame : std_logic;
  signal top_ethout_ten_Gbe0_rx_overrun : std_logic;
  signal top_ethout_ten_Gbe0_rx_overrun_ack : std_logic;
  signal top_ethout_ten_Gbe0_rx_source_ip : std_logic_vector(31 downto 0);
  signal top_ethout_ten_Gbe0_rx_source_port : std_logic_vector(15 downto 0);
  signal top_ethout_ten_Gbe0_rx_valid : std_logic;
  signal top_ethout_ten_Gbe0_tx_afull : std_logic;
  signal top_ethout_ten_Gbe0_tx_data : std_logic_vector(63 downto 0);
  signal top_ethout_ten_Gbe0_tx_dest_ip : std_logic_vector(31 downto 0);
  signal top_ethout_ten_Gbe0_tx_dest_port : std_logic_vector(15 downto 0);
  signal top_ethout_ten_Gbe0_tx_end_of_frame : std_logic;
  signal top_ethout_ten_Gbe0_tx_overflow : std_logic;
  signal top_ethout_ten_Gbe0_tx_valid : std_logic;
  signal top_ethout_ten_Gbe1_led_rx : std_logic;
  signal top_ethout_ten_Gbe1_led_tx : std_logic;
  signal top_ethout_ten_Gbe1_led_up : std_logic;
  signal top_ethout_ten_Gbe1_rst : std_logic;
  signal top_ethout_ten_Gbe1_rx_ack : std_logic;
  signal top_ethout_ten_Gbe1_rx_bad_frame : std_logic;
  signal top_ethout_ten_Gbe1_rx_data : std_logic_vector(63 downto 0);
  signal top_ethout_ten_Gbe1_rx_end_of_frame : std_logic;
  signal top_ethout_ten_Gbe1_rx_overrun : std_logic;
  signal top_ethout_ten_Gbe1_rx_overrun_ack : std_logic;
  signal top_ethout_ten_Gbe1_rx_source_ip : std_logic_vector(31 downto 0);
  signal top_ethout_ten_Gbe1_rx_source_port : std_logic_vector(15 downto 0);
  signal top_ethout_ten_Gbe1_rx_valid : std_logic;
  signal top_ethout_ten_Gbe1_tx_afull : std_logic;
  signal top_ethout_ten_Gbe1_tx_data : std_logic_vector(63 downto 0);
  signal top_ethout_ten_Gbe1_tx_dest_ip : std_logic_vector(31 downto 0);
  signal top_ethout_ten_Gbe1_tx_dest_port : std_logic_vector(15 downto 0);
  signal top_ethout_ten_Gbe1_tx_end_of_frame : std_logic;
  signal top_ethout_ten_Gbe1_tx_overflow : std_logic;
  signal top_ethout_ten_Gbe1_tx_valid : std_logic;
  signal top_ethout_ten_Gbe2_led_rx : std_logic;
  signal top_ethout_ten_Gbe2_led_tx : std_logic;
  signal top_ethout_ten_Gbe2_led_up : std_logic;
  signal top_ethout_ten_Gbe2_rst : std_logic;
  signal top_ethout_ten_Gbe2_rx_ack : std_logic;
  signal top_ethout_ten_Gbe2_rx_bad_frame : std_logic;
  signal top_ethout_ten_Gbe2_rx_data : std_logic_vector(63 downto 0);
  signal top_ethout_ten_Gbe2_rx_end_of_frame : std_logic;
  signal top_ethout_ten_Gbe2_rx_overrun : std_logic;
  signal top_ethout_ten_Gbe2_rx_overrun_ack : std_logic;
  signal top_ethout_ten_Gbe2_rx_source_ip : std_logic_vector(31 downto 0);
  signal top_ethout_ten_Gbe2_rx_source_port : std_logic_vector(15 downto 0);
  signal top_ethout_ten_Gbe2_rx_valid : std_logic;
  signal top_ethout_ten_Gbe2_tx_afull : std_logic;
  signal top_ethout_ten_Gbe2_tx_data : std_logic_vector(63 downto 0);
  signal top_ethout_ten_Gbe2_tx_dest_ip : std_logic_vector(31 downto 0);
  signal top_ethout_ten_Gbe2_tx_dest_port : std_logic_vector(15 downto 0);
  signal top_ethout_ten_Gbe2_tx_end_of_frame : std_logic;
  signal top_ethout_ten_Gbe2_tx_overflow : std_logic;
  signal top_ethout_ten_Gbe2_tx_valid : std_logic;
  signal top_ethout_ten_Gbe3_led_rx : std_logic;
  signal top_ethout_ten_Gbe3_led_tx : std_logic;
  signal top_ethout_ten_Gbe3_led_up : std_logic;
  signal top_ethout_ten_Gbe3_rst : std_logic;
  signal top_ethout_ten_Gbe3_rx_ack : std_logic;
  signal top_ethout_ten_Gbe3_rx_bad_frame : std_logic;
  signal top_ethout_ten_Gbe3_rx_data : std_logic_vector(63 downto 0);
  signal top_ethout_ten_Gbe3_rx_end_of_frame : std_logic;
  signal top_ethout_ten_Gbe3_rx_overrun : std_logic;
  signal top_ethout_ten_Gbe3_rx_overrun_ack : std_logic;
  signal top_ethout_ten_Gbe3_rx_source_ip : std_logic_vector(31 downto 0);
  signal top_ethout_ten_Gbe3_rx_source_port : std_logic_vector(15 downto 0);
  signal top_ethout_ten_Gbe3_rx_valid : std_logic;
  signal top_ethout_ten_Gbe3_tx_afull : std_logic;
  signal top_ethout_ten_Gbe3_tx_data : std_logic_vector(63 downto 0);
  signal top_ethout_ten_Gbe3_tx_dest_ip : std_logic_vector(31 downto 0);
  signal top_ethout_ten_Gbe3_tx_dest_port : std_logic_vector(15 downto 0);
  signal top_ethout_ten_Gbe3_tx_end_of_frame : std_logic;
  signal top_ethout_ten_Gbe3_tx_overflow : std_logic;
  signal top_ethout_ten_Gbe3_tx_valid : std_logic;
  signal top_scope_raw_0_snap_bram_addr : std_logic_vector(9 downto 0);
  signal top_scope_raw_0_snap_bram_data_in : std_logic_vector(127 downto 0);
  signal top_scope_raw_0_snap_bram_data_out : std_logic_vector(127 downto 0);
  signal top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Addr : std_logic_vector(0 to 31);
  signal top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Clk : std_logic;
  signal top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Din : std_logic_vector(0 to 31);
  signal top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Dout : std_logic_vector(0 to 31);
  signal top_scope_raw_0_snap_bram_ramblk_portb_BRAM_EN : std_logic;
  signal top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Rst : std_logic;
  signal top_scope_raw_0_snap_bram_ramblk_portb_BRAM_WEN : std_logic_vector(0 to 3);
  signal top_scope_raw_0_snap_bram_we : std_logic;
  signal top_scope_raw_0_snap_ctrl_user_data_out : std_logic_vector(31 downto 0);
  signal top_scope_raw_0_snap_status_user_data_in : std_logic_vector(31 downto 0);
  signal xaui_clk : std_logic;
  signal xaui_conf0_xaui_status : std_logic_vector(7 downto 0);
  signal xaui_conf1_xaui_status : std_logic_vector(7 downto 0);
  signal xaui_conf2_xaui_status : std_logic_vector(7 downto 0);
  signal xaui_conf3_xaui_status : std_logic_vector(7 downto 0);
  signal xaui_sys0_mgt_enchansync : std_logic_vector(0 to 0);
  signal xaui_sys0_mgt_encommaalign : std_logic_vector(3 downto 0);
  signal xaui_sys0_mgt_rx_reset : std_logic_vector(0 to 0);
  signal xaui_sys0_mgt_rxbufferr : std_logic_vector(3 downto 0);
  signal xaui_sys0_mgt_rxcharisk : std_logic_vector(7 downto 0);
  signal xaui_sys0_mgt_rxcodecomma : std_logic_vector(7 downto 0);
  signal xaui_sys0_mgt_rxcodevalid : std_logic_vector(7 downto 0);
  signal xaui_sys0_mgt_rxdata : std_logic_vector(63 downto 0);
  signal xaui_sys0_mgt_rxlock : std_logic_vector(3 downto 0);
  signal xaui_sys0_mgt_rxsyncok : std_logic_vector(3 downto 0);
  signal xaui_sys0_mgt_tx_reset : std_logic_vector(0 to 0);
  signal xaui_sys0_mgt_txcharisk : std_logic_vector(7 downto 0);
  signal xaui_sys0_mgt_txdata : std_logic_vector(63 downto 0);
  signal xaui_sys1_mgt_enchansync : std_logic_vector(0 to 0);
  signal xaui_sys1_mgt_encommaalign : std_logic_vector(3 downto 0);
  signal xaui_sys1_mgt_rx_reset : std_logic_vector(0 to 0);
  signal xaui_sys1_mgt_rxbufferr : std_logic_vector(3 downto 0);
  signal xaui_sys1_mgt_rxcharisk : std_logic_vector(7 downto 0);
  signal xaui_sys1_mgt_rxcodecomma : std_logic_vector(7 downto 0);
  signal xaui_sys1_mgt_rxcodevalid : std_logic_vector(7 downto 0);
  signal xaui_sys1_mgt_rxdata : std_logic_vector(63 downto 0);
  signal xaui_sys1_mgt_rxlock : std_logic_vector(3 downto 0);
  signal xaui_sys1_mgt_rxsyncok : std_logic_vector(3 downto 0);
  signal xaui_sys1_mgt_tx_reset : std_logic_vector(0 to 0);
  signal xaui_sys1_mgt_txcharisk : std_logic_vector(7 downto 0);
  signal xaui_sys1_mgt_txdata : std_logic_vector(63 downto 0);
  signal xaui_sys2_mgt_enchansync : std_logic_vector(0 to 0);
  signal xaui_sys2_mgt_encommaalign : std_logic_vector(3 downto 0);
  signal xaui_sys2_mgt_rx_reset : std_logic_vector(0 to 0);
  signal xaui_sys2_mgt_rxbufferr : std_logic_vector(3 downto 0);
  signal xaui_sys2_mgt_rxcharisk : std_logic_vector(7 downto 0);
  signal xaui_sys2_mgt_rxcodecomma : std_logic_vector(7 downto 0);
  signal xaui_sys2_mgt_rxcodevalid : std_logic_vector(7 downto 0);
  signal xaui_sys2_mgt_rxdata : std_logic_vector(63 downto 0);
  signal xaui_sys2_mgt_rxlock : std_logic_vector(3 downto 0);
  signal xaui_sys2_mgt_rxsyncok : std_logic_vector(3 downto 0);
  signal xaui_sys2_mgt_tx_reset : std_logic_vector(0 to 0);
  signal xaui_sys2_mgt_txcharisk : std_logic_vector(7 downto 0);
  signal xaui_sys2_mgt_txdata : std_logic_vector(63 downto 0);
  signal xaui_sys3_mgt_enchansync : std_logic_vector(0 to 0);
  signal xaui_sys3_mgt_encommaalign : std_logic_vector(3 downto 0);
  signal xaui_sys3_mgt_rx_reset : std_logic_vector(0 to 0);
  signal xaui_sys3_mgt_rxbufferr : std_logic_vector(3 downto 0);
  signal xaui_sys3_mgt_rxcharisk : std_logic_vector(7 downto 0);
  signal xaui_sys3_mgt_rxcodecomma : std_logic_vector(7 downto 0);
  signal xaui_sys3_mgt_rxcodevalid : std_logic_vector(7 downto 0);
  signal xaui_sys3_mgt_rxdata : std_logic_vector(63 downto 0);
  signal xaui_sys3_mgt_rxlock : std_logic_vector(3 downto 0);
  signal xaui_sys3_mgt_rxsyncok : std_logic_vector(3 downto 0);
  signal xaui_sys3_mgt_tx_reset : std_logic_vector(0 to 0);
  signal xaui_sys3_mgt_txcharisk : std_logic_vector(7 downto 0);
  signal xaui_sys3_mgt_txdata : std_logic_vector(63 downto 0);
  signal xgmii0_xgmii_rxc : std_logic_vector(7 downto 0);
  signal xgmii0_xgmii_rxd : std_logic_vector(63 downto 0);
  signal xgmii0_xgmii_txc : std_logic_vector(7 downto 0);
  signal xgmii0_xgmii_txd : std_logic_vector(63 downto 0);
  signal xgmii1_xgmii_rxc : std_logic_vector(7 downto 0);
  signal xgmii1_xgmii_rxd : std_logic_vector(63 downto 0);
  signal xgmii1_xgmii_txc : std_logic_vector(7 downto 0);
  signal xgmii1_xgmii_txd : std_logic_vector(63 downto 0);
  signal xgmii2_xgmii_rxc : std_logic_vector(7 downto 0);
  signal xgmii2_xgmii_rxd : std_logic_vector(63 downto 0);
  signal xgmii2_xgmii_txc : std_logic_vector(7 downto 0);
  signal xgmii2_xgmii_txd : std_logic_vector(63 downto 0);
  signal xgmii3_xgmii_rxc : std_logic_vector(7 downto 0);
  signal xgmii3_xgmii_rxd : std_logic_vector(63 downto 0);
  signal xgmii3_xgmii_txc : std_logic_vector(7 downto 0);
  signal xgmii3_xgmii_txd : std_logic_vector(63 downto 0);
  signal adc0_reset_1 : std_logic;

  attribute BOX_TYPE : STRING;
  attribute BOX_TYPE of system_xaui_infrastructure_inst_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_opb_adc5g_controller_0_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_infrastructure_inst_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_reset_block_inst_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_opb0_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_epb_opb_bridge_inst_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_epb_infrastructure_inst_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_sys_block_inst_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_sfp_mdio_controller_inst_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_xsg_core_config_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_adc_adc_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_adc_enable_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_data_bypass_topin_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_data_bypass_topout_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_dest_ip0_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_dest_ip1_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_dest_ip2_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_dest_ip3_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_dest_port0_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_dest_port1_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_dest_port2_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_dest_port3_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_rst_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_ten_gbe0_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_xaui_phy_0_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_ten_gbe1_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_xaui_phy_1_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_ten_gbe2_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_xaui_phy_2_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_ethout_ten_gbe3_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_xaui_phy_3_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_scope_raw_0_snap_bram_ramblk_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_scope_raw_0_snap_bram_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_scope_raw_0_snap_ctrl_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_top_scope_raw_0_snap_status_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_clk250_to_clk125_0_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_clk125_to_clk250_0_wrapper : component is "user_black_box";
  attribute BOX_TYPE of system_interleave_sampling_0_wrapper : component is "user_black_box";

begin

  -- Internal assignments

  pgassign1(15 downto 0) <= X"0000";
  net_gnd0 <= '0';
  net_gnd1(0 to 0) <= B"0";
  net_gnd16(15 downto 0) <= B"0000000000000000";
  net_gnd20(0 to 19) <= B"00000000000000000000";
  net_gnd3(2 downto 0) <= B"000";
  net_gnd4(3 downto 0) <= B"0000";
  net_gnd5(4 downto 0) <= B"00000";
  net_gnd64(63 downto 0) <= B"0000000000000000000000000000000000000000000000000000000000000000";
  net_gnd8(7 downto 0) <= B"00000000";
  net_vcc1(0 to 0) <= B"1";
  net_vcc20(0 to 19) <= B"11111111111111111111";
  adc0_reset <= adc0_reset_1;

  xaui_infrastructure_inst : system_xaui_infrastructure_inst_wrapper
    port map (
      reset => sys_reset,
      xaui_refclk_n => xaui_refclk_n,
      xaui_refclk_p => xaui_refclk_p,
      mgt_rx_n => mgt_rx_n,
      mgt_rx_p => mgt_rx_p,
      mgt_tx_n => mgt_tx_n,
      mgt_tx_p => mgt_tx_p,
      xaui_clk => xaui_clk,
      mgt_tx_rst0 => xaui_sys0_mgt_tx_reset(0 to 0),
      mgt_rx_rst0 => xaui_sys0_mgt_rx_reset(0 to 0),
      mgt_txdata0 => xaui_sys0_mgt_txdata,
      mgt_txcharisk0 => xaui_sys0_mgt_txcharisk,
      mgt_rxdata0 => xaui_sys0_mgt_rxdata,
      mgt_rxcharisk0 => xaui_sys0_mgt_rxcharisk,
      mgt_rxcodecomma0 => xaui_sys0_mgt_rxcodecomma,
      mgt_rxencommaalign0 => xaui_sys0_mgt_encommaalign,
      mgt_rxenchansync0 => xaui_sys0_mgt_enchansync(0 to 0),
      mgt_rxsyncok0 => xaui_sys0_mgt_rxsyncok,
      mgt_rxcodevalid0 => xaui_sys0_mgt_rxcodevalid,
      mgt_rxbufferr0 => xaui_sys0_mgt_rxbufferr,
      mgt_rxlock0 => xaui_sys0_mgt_rxlock,
      mgt_txpostemphasis0 => phy_conf0_mgt_txpostemphasis,
      mgt_txpreemphasis0 => phy_conf0_mgt_txpreemphasis,
      mgt_txdiffctrl0 => phy_conf0_mgt_txdiffctrl,
      mgt_rxeqmix0 => phy_conf0_mgt_rxeqmix,
      mgt_tx_rst1 => xaui_sys1_mgt_tx_reset(0 to 0),
      mgt_rx_rst1 => xaui_sys1_mgt_rx_reset(0 to 0),
      mgt_txdata1 => xaui_sys1_mgt_txdata,
      mgt_txcharisk1 => xaui_sys1_mgt_txcharisk,
      mgt_rxdata1 => xaui_sys1_mgt_rxdata,
      mgt_rxcharisk1 => xaui_sys1_mgt_rxcharisk,
      mgt_rxcodecomma1 => xaui_sys1_mgt_rxcodecomma,
      mgt_rxencommaalign1 => xaui_sys1_mgt_encommaalign,
      mgt_rxenchansync1 => xaui_sys1_mgt_enchansync(0 to 0),
      mgt_rxsyncok1 => xaui_sys1_mgt_rxsyncok,
      mgt_rxcodevalid1 => xaui_sys1_mgt_rxcodevalid,
      mgt_rxbufferr1 => xaui_sys1_mgt_rxbufferr,
      mgt_rxlock1 => xaui_sys1_mgt_rxlock,
      mgt_txpostemphasis1 => phy_conf1_mgt_txpostemphasis,
      mgt_txpreemphasis1 => phy_conf1_mgt_txpreemphasis,
      mgt_txdiffctrl1 => phy_conf1_mgt_txdiffctrl,
      mgt_rxeqmix1 => phy_conf1_mgt_rxeqmix,
      mgt_tx_rst2 => xaui_sys2_mgt_tx_reset(0 to 0),
      mgt_rx_rst2 => xaui_sys2_mgt_rx_reset(0 to 0),
      mgt_txdata2 => xaui_sys2_mgt_txdata,
      mgt_txcharisk2 => xaui_sys2_mgt_txcharisk,
      mgt_rxdata2 => xaui_sys2_mgt_rxdata,
      mgt_rxcharisk2 => xaui_sys2_mgt_rxcharisk,
      mgt_rxcodecomma2 => xaui_sys2_mgt_rxcodecomma,
      mgt_rxencommaalign2 => xaui_sys2_mgt_encommaalign,
      mgt_rxenchansync2 => xaui_sys2_mgt_enchansync(0 to 0),
      mgt_rxsyncok2 => xaui_sys2_mgt_rxsyncok,
      mgt_rxcodevalid2 => xaui_sys2_mgt_rxcodevalid,
      mgt_rxbufferr2 => xaui_sys2_mgt_rxbufferr,
      mgt_rxlock2 => xaui_sys2_mgt_rxlock,
      mgt_txpostemphasis2 => phy_conf2_mgt_txpostemphasis,
      mgt_txpreemphasis2 => phy_conf2_mgt_txpreemphasis,
      mgt_txdiffctrl2 => phy_conf2_mgt_txdiffctrl,
      mgt_rxeqmix2 => phy_conf2_mgt_rxeqmix,
      mgt_tx_rst3 => xaui_sys3_mgt_tx_reset(0 to 0),
      mgt_rx_rst3 => xaui_sys3_mgt_rx_reset(0 to 0),
      mgt_txdata3 => xaui_sys3_mgt_txdata,
      mgt_txcharisk3 => xaui_sys3_mgt_txcharisk,
      mgt_rxdata3 => xaui_sys3_mgt_rxdata,
      mgt_rxcharisk3 => xaui_sys3_mgt_rxcharisk,
      mgt_rxcodecomma3 => xaui_sys3_mgt_rxcodecomma,
      mgt_rxencommaalign3 => xaui_sys3_mgt_encommaalign,
      mgt_rxenchansync3 => xaui_sys3_mgt_enchansync(0 to 0),
      mgt_rxsyncok3 => xaui_sys3_mgt_rxsyncok,
      mgt_rxcodevalid3 => xaui_sys3_mgt_rxcodevalid,
      mgt_rxbufferr3 => xaui_sys3_mgt_rxbufferr,
      mgt_rxlock3 => xaui_sys3_mgt_rxlock,
      mgt_txpostemphasis3 => phy_conf3_mgt_txpostemphasis,
      mgt_txpreemphasis3 => phy_conf3_mgt_txpreemphasis,
      mgt_txdiffctrl3 => phy_conf3_mgt_txdiffctrl,
      mgt_rxeqmix3 => phy_conf3_mgt_rxeqmix,
      mgt_tx_rst4 => net_gnd1(0 to 0),
      mgt_rx_rst4 => net_gnd1(0 to 0),
      mgt_txdata4 => net_gnd64,
      mgt_txcharisk4 => net_gnd8,
      mgt_rxdata4 => open,
      mgt_rxcharisk4 => open,
      mgt_rxcodecomma4 => open,
      mgt_rxencommaalign4 => net_gnd4,
      mgt_rxenchansync4 => net_gnd1(0 to 0),
      mgt_rxsyncok4 => open,
      mgt_rxcodevalid4 => open,
      mgt_rxbufferr4 => open,
      mgt_rxlock4 => open,
      mgt_txpostemphasis4 => net_gnd5,
      mgt_txpreemphasis4 => net_gnd4,
      mgt_txdiffctrl4 => net_gnd4,
      mgt_rxeqmix4 => net_gnd3,
      mgt_tx_rst5 => net_gnd1(0 to 0),
      mgt_rx_rst5 => net_gnd1(0 to 0),
      mgt_txdata5 => net_gnd64,
      mgt_txcharisk5 => net_gnd8,
      mgt_rxdata5 => open,
      mgt_rxcharisk5 => open,
      mgt_rxcodecomma5 => open,
      mgt_rxencommaalign5 => net_gnd4,
      mgt_rxenchansync5 => net_gnd1(0 to 0),
      mgt_rxsyncok5 => open,
      mgt_rxcodevalid5 => open,
      mgt_rxbufferr5 => open,
      mgt_rxlock5 => open,
      mgt_txpostemphasis5 => net_gnd5,
      mgt_txpreemphasis5 => net_gnd4,
      mgt_txdiffctrl5 => net_gnd4,
      mgt_rxeqmix5 => net_gnd3,
      mgt_tx_rst6 => net_gnd1(0 to 0),
      mgt_rx_rst6 => net_gnd1(0 to 0),
      mgt_txdata6 => net_gnd64,
      mgt_txcharisk6 => net_gnd8,
      mgt_rxdata6 => open,
      mgt_rxcharisk6 => open,
      mgt_rxcodecomma6 => open,
      mgt_rxencommaalign6 => net_gnd4,
      mgt_rxenchansync6 => net_gnd1(0 to 0),
      mgt_rxsyncok6 => open,
      mgt_rxcodevalid6 => open,
      mgt_rxbufferr6 => open,
      mgt_rxlock6 => open,
      mgt_txpostemphasis6 => net_gnd5,
      mgt_txpreemphasis6 => net_gnd4,
      mgt_txdiffctrl6 => net_gnd4,
      mgt_rxeqmix6 => net_gnd3,
      mgt_tx_rst7 => net_gnd1(0 to 0),
      mgt_rx_rst7 => net_gnd1(0 to 0),
      mgt_txdata7 => net_gnd64,
      mgt_txcharisk7 => net_gnd8,
      mgt_rxdata7 => open,
      mgt_rxcharisk7 => open,
      mgt_rxcodecomma7 => open,
      mgt_rxencommaalign7 => net_gnd4,
      mgt_rxenchansync7 => net_gnd1(0 to 0),
      mgt_rxsyncok7 => open,
      mgt_rxcodevalid7 => open,
      mgt_rxbufferr7 => open,
      mgt_rxlock7 => open,
      mgt_txpostemphasis7 => net_gnd5,
      mgt_txpreemphasis7 => net_gnd4,
      mgt_txdiffctrl7 => net_gnd4,
      mgt_rxeqmix7 => net_gnd3
    );

  opb_adc5g_controller_0 : system_opb_adc5g_controller_0_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(0 to 31),
      Sl_errAck => opb0_Sl_errAck(0),
      Sl_retry => opb0_Sl_retry(0),
      Sl_toutSup => opb0_Sl_toutSup(0),
      Sl_xferAck => opb0_Sl_xferAck(0),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      adc0_adc3wire_clk => adc0_adc3wire_clk,
      adc0_adc3wire_data => adc0_adc3wire_data,
      adc0_adc3wire_data_o => adc0_adc3wire_data_o,
      adc0_adc3wire_spi_rst => adc0_adc3wire_spi_rst,
      adc0_modepin => adc0_modepin,
      adc0_reset => adc0_reset_1,
      adc0_dcm_reset => adc0_dcm_reset,
      adc0_dcm_locked => adc0_clk_lock,
      adc0_fifo_full_cnt => adc0_fifo_full_cnt,
      adc0_fifo_empty_cnt => adc0_fifo_empty_cnt,
      adc0_psclk => adc0_psclk,
      adc0_psen => adc0_psen,
      adc0_psincdec => adc0_psincdec,
      adc0_psdone => adc0_psdone,
      adc0_clk => adc0_clk,
      adc0_datain_pin => adc0_datain_pin,
      adc0_datain_tap => adc0_datain_tap,
      adc0_tap_rst => adc0_tap_rst,
      adc1_adc3wire_clk => open,
      adc1_adc3wire_data => open,
      adc1_adc3wire_data_o => net_gnd0,
      adc1_adc3wire_spi_rst => open,
      adc1_modepin => open,
      adc1_reset => open,
      adc1_dcm_reset => open,
      adc1_dcm_locked => net_gnd0,
      adc1_fifo_full_cnt => net_gnd16,
      adc1_fifo_empty_cnt => net_gnd16,
      adc1_psclk => open,
      adc1_psen => open,
      adc1_psincdec => open,
      adc1_psdone => net_gnd0,
      adc1_clk => net_gnd0,
      adc1_datain_pin => open,
      adc1_datain_tap => open,
      adc1_tap_rst => open
    );

  infrastructure_inst : system_infrastructure_inst_wrapper
    port map (
      sys_clk_n => sys_clk_n,
      sys_clk_p => sys_clk_p,
      aux_clk_n => aux_clk_n,
      aux_clk_p => aux_clk_p,
      epb_clk_in => epb_clk_in,
      sys_clk => sys_clk,
      sys_clk90 => open,
      sys_clk180 => open,
      sys_clk270 => open,
      sys_clk_lock => open,
      sys_clk2x => open,
      sys_clk2x90 => open,
      sys_clk2x180 => open,
      sys_clk2x270 => open,
      aux_clk => open,
      aux_clk90 => open,
      aux_clk180 => open,
      aux_clk270 => open,
      aux_clk2x => open,
      aux_clk2x90 => open,
      aux_clk2x180 => open,
      aux_clk2x270 => open,
      epb_clk => epb_clk,
      idelay_rst => power_on_rst,
      idelay_rdy => open,
      op_power_on_rst => power_on_rst,
      clk_200 => open,
      clk_100 => open
    );

  reset_block_inst : system_reset_block_inst_wrapper
    port map (
      clk => sys_clk,
      ip_async_reset_i => power_on_rst,
      ip_reset_i => power_on_rst,
      op_reset_o => sys_reset
    );

  opb0 : system_opb0_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      SYS_Rst => power_on_rst,
      Debug_SYS_Rst => net_gnd0,
      WDT_Rst => net_gnd0,
      M_ABus => opb0_M_ABus,
      M_BE => opb0_M_BE,
      M_beXfer => net_gnd1(0 to 0),
      M_busLock => opb0_M_busLock(0 to 0),
      M_DBus => opb0_M_DBus,
      M_DBusEn => net_gnd1(0 to 0),
      M_DBusEn32_63 => net_vcc1(0 to 0),
      M_dwXfer => net_gnd1(0 to 0),
      M_fwXfer => net_gnd1(0 to 0),
      M_hwXfer => net_gnd1(0 to 0),
      M_request => opb0_M_request(0 to 0),
      M_RNW => opb0_M_RNW(0 to 0),
      M_select => opb0_M_select(0 to 0),
      M_seqAddr => opb0_M_seqAddr(0 to 0),
      Sl_beAck => net_gnd20,
      Sl_DBus => opb0_Sl_DBus,
      Sl_DBusEn => net_vcc20,
      Sl_DBusEn32_63 => net_vcc20,
      Sl_errAck => opb0_Sl_errAck,
      Sl_dwAck => net_gnd20,
      Sl_fwAck => net_gnd20,
      Sl_hwAck => net_gnd20,
      Sl_retry => opb0_Sl_retry,
      Sl_toutSup => opb0_Sl_toutSup,
      Sl_xferAck => opb0_Sl_xferAck,
      OPB_MRequest => open,
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_beXfer => open,
      OPB_beAck => open,
      OPB_busLock => open,
      OPB_rdDBus => open,
      OPB_wrDBus => open,
      OPB_DBus => opb0_OPB_DBus,
      OPB_errAck => opb0_OPB_errAck,
      OPB_dwAck => open,
      OPB_dwXfer => open,
      OPB_fwAck => open,
      OPB_fwXfer => open,
      OPB_hwAck => open,
      OPB_hwXfer => open,
      OPB_MGrant => opb0_OPB_MGrant(0 to 0),
      OPB_pendReq => open,
      OPB_retry => opb0_OPB_retry,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      OPB_timeout => opb0_OPB_timeout,
      OPB_toutSup => open,
      OPB_xferAck => opb0_OPB_xferAck
    );

  epb_opb_bridge_inst : system_epb_opb_bridge_inst_wrapper
    port map (
      epb_clk => epb_clk,
      epb_doe_n => epb_doe_n,
      epb_data_oe_n => epb_data_oe_n,
      epb_cs_n => epb_cs_n,
      epb_oe_n => epb_oe_n,
      epb_r_w_n => epb_r_w_n,
      epb_be_n => epb_be_n(0 to 3),
      epb_addr => epb_addr,
      epb_data_i => epb_data_i,
      epb_data_o => epb_data_o(31 downto 0),
      epb_rdy => epb_rdy,
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      M_request => opb0_M_request(0),
      M_busLock => opb0_M_busLock(0),
      M_select => opb0_M_select(0),
      M_RNW => opb0_M_RNW(0),
      M_BE => opb0_M_BE,
      M_seqAddr => opb0_M_seqAddr(0),
      M_DBus => opb0_M_DBus,
      M_ABus => opb0_M_ABus,
      OPB_MGrant => opb0_OPB_MGrant(0),
      OPB_xferAck => opb0_OPB_xferAck,
      OPB_errAck => opb0_OPB_errAck,
      OPB_retry => opb0_OPB_retry,
      OPB_timeout => opb0_OPB_timeout,
      OPB_DBus => opb0_OPB_DBus
    );

  epb_infrastructure_inst : system_epb_infrastructure_inst_wrapper
    port map (
      epb_data_buf => epb_data(0 to 31),
      epb_data_oe_n_i => epb_data_oe_n,
      epb_data_out_i => epb_data_o,
      epb_data_in_o => epb_data_i(0 to 31)
    );

  sys_block_inst : system_sys_block_inst_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(32 to 63),
      Sl_errAck => opb0_Sl_errAck(1),
      Sl_retry => opb0_Sl_retry(1),
      Sl_toutSup => opb0_Sl_toutSup(1),
      Sl_xferAck => opb0_Sl_xferAck(1),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      soft_reset => open,
      irq_n => ppc_irq_n,
      app_irq => pgassign1,
      fab_clk => adc0_clk
    );

  sfp_mdio_controller_inst : system_sfp_mdio_controller_inst_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(64 to 95),
      Sl_errAck => opb0_Sl_errAck(2),
      Sl_retry => opb0_Sl_retry(2),
      Sl_toutSup => opb0_Sl_toutSup(2),
      Sl_xferAck => opb0_Sl_xferAck(2),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      mgt_gpio => mgt_gpio
    );

  top_XSG_core_config : system_top_xsg_core_config_wrapper
    port map (
      clk => adc0_clk,
      top_adc_adc_sync => top_adc_adc_sync,
      top_adc_adc_user_data_i0 => top_adc_adc_user_data_i0,
      top_adc_adc_user_data_i1 => top_adc_adc_user_data_i1,
      top_adc_adc_user_data_i2 => top_adc_adc_user_data_i2,
      top_adc_adc_user_data_i3 => top_adc_adc_user_data_i3,
      top_adc_adc_user_data_i4 => top_adc_adc_user_data_i4,
      top_adc_adc_user_data_i5 => top_adc_adc_user_data_i5,
      top_adc_adc_user_data_i6 => top_adc_adc_user_data_i6,
      top_adc_adc_user_data_i7 => top_adc_adc_user_data_i7,
      top_adc_adc_user_data_q0 => top_adc_adc_user_data_q0,
      top_adc_adc_user_data_q1 => top_adc_adc_user_data_q1,
      top_adc_adc_user_data_q2 => top_adc_adc_user_data_q2,
      top_adc_adc_user_data_q3 => top_adc_adc_user_data_q3,
      top_adc_adc_user_data_q4 => top_adc_adc_user_data_q4,
      top_adc_adc_user_data_q5 => top_adc_adc_user_data_q5,
      top_adc_adc_user_data_q6 => top_adc_adc_user_data_q6,
      top_adc_adc_user_data_q7 => top_adc_adc_user_data_q7,
      top_adc_enable_user_data_out => top_adc_enable_user_data_out,
      top_data_bypass_topin_data_out => top_data_bypass_topin_data_out,
      top_data_bypass_topin_data_out_vld => top_data_bypass_topin_data_out_vld,
      top_data_bypass_topin_data_in => top_data_bypass_topin_data_in,
      top_data_bypass_topin_en => top_data_bypass_topin_en,
      top_data_bypass_topin_rst_n => top_data_bypass_topin_rst_n,
      top_data_bypass_topout_data_out => clk125_to_clk250_0_data_out,
      top_data_bypass_topout_data_out_vld => clk125_to_clk250_0_data_out_vld,
      top_data_bypass_topout_data_in => top_data_bypass_topout_data_in,
      top_data_bypass_topout_en => top_data_bypass_topout_en,
      top_data_bypass_topout_rst_n => top_data_bypass_topout_rst_n,
      top_ethout_dest_ip0_user_data_out => top_ethout_dest_ip0_user_data_out,
      top_ethout_dest_ip1_user_data_out => top_ethout_dest_ip1_user_data_out,
      top_ethout_dest_ip2_user_data_out => top_ethout_dest_ip2_user_data_out,
      top_ethout_dest_ip3_user_data_out => top_ethout_dest_ip3_user_data_out,
      top_ethout_dest_port0_user_data_out => top_ethout_dest_port0_user_data_out,
      top_ethout_dest_port1_user_data_out => top_ethout_dest_port1_user_data_out,
      top_ethout_dest_port2_user_data_out => top_ethout_dest_port2_user_data_out,
      top_ethout_dest_port3_user_data_out => top_ethout_dest_port3_user_data_out,
      top_ethout_rst_user_data_out => top_ethout_rst_user_data_out,
      top_ethout_ten_Gbe0_led_rx => top_ethout_ten_Gbe0_led_rx,
      top_ethout_ten_Gbe0_led_tx => top_ethout_ten_Gbe0_led_tx,
      top_ethout_ten_Gbe0_led_up => top_ethout_ten_Gbe0_led_up,
      top_ethout_ten_Gbe0_rx_bad_frame => top_ethout_ten_Gbe0_rx_bad_frame,
      top_ethout_ten_Gbe0_rx_data => top_ethout_ten_Gbe0_rx_data,
      top_ethout_ten_Gbe0_rx_end_of_frame => top_ethout_ten_Gbe0_rx_end_of_frame,
      top_ethout_ten_Gbe0_rx_overrun => top_ethout_ten_Gbe0_rx_overrun,
      top_ethout_ten_Gbe0_rx_source_ip => top_ethout_ten_Gbe0_rx_source_ip,
      top_ethout_ten_Gbe0_rx_source_port => top_ethout_ten_Gbe0_rx_source_port,
      top_ethout_ten_Gbe0_rx_valid => top_ethout_ten_Gbe0_rx_valid,
      top_ethout_ten_Gbe0_tx_afull => top_ethout_ten_Gbe0_tx_afull,
      top_ethout_ten_Gbe0_tx_overflow => top_ethout_ten_Gbe0_tx_overflow,
      top_ethout_ten_Gbe0_rst => top_ethout_ten_Gbe0_rst,
      top_ethout_ten_Gbe0_rx_ack => top_ethout_ten_Gbe0_rx_ack,
      top_ethout_ten_Gbe0_rx_overrun_ack => top_ethout_ten_Gbe0_rx_overrun_ack,
      top_ethout_ten_Gbe0_tx_data => top_ethout_ten_Gbe0_tx_data,
      top_ethout_ten_Gbe0_tx_dest_ip => top_ethout_ten_Gbe0_tx_dest_ip,
      top_ethout_ten_Gbe0_tx_dest_port => top_ethout_ten_Gbe0_tx_dest_port,
      top_ethout_ten_Gbe0_tx_end_of_frame => top_ethout_ten_Gbe0_tx_end_of_frame,
      top_ethout_ten_Gbe0_tx_valid => top_ethout_ten_Gbe0_tx_valid,
      top_ethout_ten_Gbe1_led_rx => top_ethout_ten_Gbe1_led_rx,
      top_ethout_ten_Gbe1_led_tx => top_ethout_ten_Gbe1_led_tx,
      top_ethout_ten_Gbe1_led_up => top_ethout_ten_Gbe1_led_up,
      top_ethout_ten_Gbe1_rx_bad_frame => top_ethout_ten_Gbe1_rx_bad_frame,
      top_ethout_ten_Gbe1_rx_data => top_ethout_ten_Gbe1_rx_data,
      top_ethout_ten_Gbe1_rx_end_of_frame => top_ethout_ten_Gbe1_rx_end_of_frame,
      top_ethout_ten_Gbe1_rx_overrun => top_ethout_ten_Gbe1_rx_overrun,
      top_ethout_ten_Gbe1_rx_source_ip => top_ethout_ten_Gbe1_rx_source_ip,
      top_ethout_ten_Gbe1_rx_source_port => top_ethout_ten_Gbe1_rx_source_port,
      top_ethout_ten_Gbe1_rx_valid => top_ethout_ten_Gbe1_rx_valid,
      top_ethout_ten_Gbe1_tx_afull => top_ethout_ten_Gbe1_tx_afull,
      top_ethout_ten_Gbe1_tx_overflow => top_ethout_ten_Gbe1_tx_overflow,
      top_ethout_ten_Gbe1_rst => top_ethout_ten_Gbe1_rst,
      top_ethout_ten_Gbe1_rx_ack => top_ethout_ten_Gbe1_rx_ack,
      top_ethout_ten_Gbe1_rx_overrun_ack => top_ethout_ten_Gbe1_rx_overrun_ack,
      top_ethout_ten_Gbe1_tx_data => top_ethout_ten_Gbe1_tx_data,
      top_ethout_ten_Gbe1_tx_dest_ip => top_ethout_ten_Gbe1_tx_dest_ip,
      top_ethout_ten_Gbe1_tx_dest_port => top_ethout_ten_Gbe1_tx_dest_port,
      top_ethout_ten_Gbe1_tx_end_of_frame => top_ethout_ten_Gbe1_tx_end_of_frame,
      top_ethout_ten_Gbe1_tx_valid => top_ethout_ten_Gbe1_tx_valid,
      top_ethout_ten_Gbe2_led_rx => top_ethout_ten_Gbe2_led_rx,
      top_ethout_ten_Gbe2_led_tx => top_ethout_ten_Gbe2_led_tx,
      top_ethout_ten_Gbe2_led_up => top_ethout_ten_Gbe2_led_up,
      top_ethout_ten_Gbe2_rx_bad_frame => top_ethout_ten_Gbe2_rx_bad_frame,
      top_ethout_ten_Gbe2_rx_data => top_ethout_ten_Gbe2_rx_data,
      top_ethout_ten_Gbe2_rx_end_of_frame => top_ethout_ten_Gbe2_rx_end_of_frame,
      top_ethout_ten_Gbe2_rx_overrun => top_ethout_ten_Gbe2_rx_overrun,
      top_ethout_ten_Gbe2_rx_source_ip => top_ethout_ten_Gbe2_rx_source_ip,
      top_ethout_ten_Gbe2_rx_source_port => top_ethout_ten_Gbe2_rx_source_port,
      top_ethout_ten_Gbe2_rx_valid => top_ethout_ten_Gbe2_rx_valid,
      top_ethout_ten_Gbe2_tx_afull => top_ethout_ten_Gbe2_tx_afull,
      top_ethout_ten_Gbe2_tx_overflow => top_ethout_ten_Gbe2_tx_overflow,
      top_ethout_ten_Gbe2_rst => top_ethout_ten_Gbe2_rst,
      top_ethout_ten_Gbe2_rx_ack => top_ethout_ten_Gbe2_rx_ack,
      top_ethout_ten_Gbe2_rx_overrun_ack => top_ethout_ten_Gbe2_rx_overrun_ack,
      top_ethout_ten_Gbe2_tx_data => top_ethout_ten_Gbe2_tx_data,
      top_ethout_ten_Gbe2_tx_dest_ip => top_ethout_ten_Gbe2_tx_dest_ip,
      top_ethout_ten_Gbe2_tx_dest_port => top_ethout_ten_Gbe2_tx_dest_port,
      top_ethout_ten_Gbe2_tx_end_of_frame => top_ethout_ten_Gbe2_tx_end_of_frame,
      top_ethout_ten_Gbe2_tx_valid => top_ethout_ten_Gbe2_tx_valid,
      top_ethout_ten_Gbe3_led_rx => top_ethout_ten_Gbe3_led_rx,
      top_ethout_ten_Gbe3_led_tx => top_ethout_ten_Gbe3_led_tx,
      top_ethout_ten_Gbe3_led_up => top_ethout_ten_Gbe3_led_up,
      top_ethout_ten_Gbe3_rx_bad_frame => top_ethout_ten_Gbe3_rx_bad_frame,
      top_ethout_ten_Gbe3_rx_data => top_ethout_ten_Gbe3_rx_data,
      top_ethout_ten_Gbe3_rx_end_of_frame => top_ethout_ten_Gbe3_rx_end_of_frame,
      top_ethout_ten_Gbe3_rx_overrun => top_ethout_ten_Gbe3_rx_overrun,
      top_ethout_ten_Gbe3_rx_source_ip => top_ethout_ten_Gbe3_rx_source_ip,
      top_ethout_ten_Gbe3_rx_source_port => top_ethout_ten_Gbe3_rx_source_port,
      top_ethout_ten_Gbe3_rx_valid => top_ethout_ten_Gbe3_rx_valid,
      top_ethout_ten_Gbe3_tx_afull => top_ethout_ten_Gbe3_tx_afull,
      top_ethout_ten_Gbe3_tx_overflow => top_ethout_ten_Gbe3_tx_overflow,
      top_ethout_ten_Gbe3_rst => top_ethout_ten_Gbe3_rst,
      top_ethout_ten_Gbe3_rx_ack => top_ethout_ten_Gbe3_rx_ack,
      top_ethout_ten_Gbe3_rx_overrun_ack => top_ethout_ten_Gbe3_rx_overrun_ack,
      top_ethout_ten_Gbe3_tx_data => top_ethout_ten_Gbe3_tx_data,
      top_ethout_ten_Gbe3_tx_dest_ip => top_ethout_ten_Gbe3_tx_dest_ip,
      top_ethout_ten_Gbe3_tx_dest_port => top_ethout_ten_Gbe3_tx_dest_port,
      top_ethout_ten_Gbe3_tx_end_of_frame => top_ethout_ten_Gbe3_tx_end_of_frame,
      top_ethout_ten_Gbe3_tx_valid => top_ethout_ten_Gbe3_tx_valid,
      top_scope_raw_0_snap_bram_data_out => top_scope_raw_0_snap_bram_data_out,
      top_scope_raw_0_snap_bram_addr => top_scope_raw_0_snap_bram_addr,
      top_scope_raw_0_snap_bram_data_in => top_scope_raw_0_snap_bram_data_in,
      top_scope_raw_0_snap_bram_we => top_scope_raw_0_snap_bram_we,
      top_scope_raw_0_snap_ctrl_user_data_out => top_scope_raw_0_snap_ctrl_user_data_out,
      top_scope_raw_0_snap_status_user_data_in => top_scope_raw_0_snap_status_user_data_in
    );

  top_adc_adc : system_top_adc_adc_wrapper
    port map (
      adc_clk_p_i => adc0clk_p,
      adc_clk_n_i => adc0clk_n,
      adc_data0_p_i => adc0data0_p_i,
      adc_data0_n_i => adc0data0_n_i,
      adc_data1_p_i => adc0data1_p_i,
      adc_data1_n_i => adc0data1_n_i,
      adc_data2_p_i => adc0data2_p_i,
      adc_data2_n_i => adc0data2_n_i,
      adc_data3_p_i => adc0data3_p_i,
      adc_data3_n_i => adc0data3_n_i,
      adc_reset_o => open,
      adc_sync_p => adc0sync_p,
      adc_sync_n => adc0sync_n,
      sync => top_adc_adc_sync,
      user_data_i0 => top_adc_adc_user_data_i0,
      user_data_i1 => top_adc_adc_user_data_i1,
      user_data_i2 => top_adc_adc_user_data_i2,
      user_data_i3 => top_adc_adc_user_data_i3,
      user_data_i4 => top_adc_adc_user_data_i4,
      user_data_i5 => top_adc_adc_user_data_i5,
      user_data_i6 => top_adc_adc_user_data_i6,
      user_data_i7 => top_adc_adc_user_data_i7,
      user_data_q0 => top_adc_adc_user_data_q0,
      user_data_q1 => top_adc_adc_user_data_q1,
      user_data_q2 => top_adc_adc_user_data_q2,
      user_data_q3 => top_adc_adc_user_data_q3,
      user_data_q4 => top_adc_adc_user_data_q4,
      user_data_q5 => top_adc_adc_user_data_q5,
      user_data_q6 => top_adc_adc_user_data_q6,
      user_data_q7 => top_adc_adc_user_data_q7,
      dcm_reset => adc0_dcm_reset,
      ctrl_reset => adc0_reset_1,
      ctrl_clk_in => adc0_clk,
      ctrl_clk_out => adc0_clk,
      ctrl_clk90_out => open,
      ctrl_clk180_out => open,
      ctrl_clk270_out => open,
      ctrl_clk125m_out => top_adc_adc_ctrl_clk125m_out,
      ctrl_dcm_locked => adc0_clk_lock,
      fifo_full_cnt => adc0_fifo_full_cnt,
      fifo_empty_cnt => adc0_fifo_empty_cnt,
      dcm_psclk => adc0_psclk,
      dcm_psen => adc0_psen,
      dcm_psincdec => adc0_psincdec,
      dcm_psdone => adc0_psdone,
      datain_pin => adc0_datain_pin,
      datain_tap => adc0_datain_tap,
      tap_rst => adc0_tap_rst
    );

  top_adc_enable : system_top_adc_enable_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(96 to 127),
      Sl_errAck => opb0_Sl_errAck(3),
      Sl_retry => opb0_Sl_retry(3),
      Sl_toutSup => opb0_Sl_toutSup(3),
      Sl_xferAck => opb0_Sl_xferAck(3),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_adc_enable_user_data_out,
      user_clk => adc0_clk
    );

  top_data_bypass_topin : system_top_data_bypass_topin_wrapper
    port map (
      clk => adc0_clk,
      rst_n => top_data_bypass_topin_rst_n,
      en => top_data_bypass_topin_en,
      data_in => top_data_bypass_topin_data_in,
      data_out => top_data_bypass_topin_data_out,
      data_out_vld => top_data_bypass_topin_data_out_vld
    );

  top_data_bypass_topout : system_top_data_bypass_topout_wrapper
    port map (
      clk => adc0_clk,
      rst_n => top_data_bypass_topout_rst_n,
      en => top_data_bypass_topout_en,
      data_in => top_data_bypass_topout_data_in,
      data_out => top_data_bypass_topout_data_out,
      data_out_vld => top_data_bypass_topout_data_out_vld
    );

  top_ethout_dest_ip0 : system_top_ethout_dest_ip0_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(128 to 159),
      Sl_errAck => opb0_Sl_errAck(4),
      Sl_retry => opb0_Sl_retry(4),
      Sl_toutSup => opb0_Sl_toutSup(4),
      Sl_xferAck => opb0_Sl_xferAck(4),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_ethout_dest_ip0_user_data_out,
      user_clk => adc0_clk
    );

  top_ethout_dest_ip1 : system_top_ethout_dest_ip1_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(160 to 191),
      Sl_errAck => opb0_Sl_errAck(5),
      Sl_retry => opb0_Sl_retry(5),
      Sl_toutSup => opb0_Sl_toutSup(5),
      Sl_xferAck => opb0_Sl_xferAck(5),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_ethout_dest_ip1_user_data_out,
      user_clk => adc0_clk
    );

  top_ethout_dest_ip2 : system_top_ethout_dest_ip2_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(192 to 223),
      Sl_errAck => opb0_Sl_errAck(6),
      Sl_retry => opb0_Sl_retry(6),
      Sl_toutSup => opb0_Sl_toutSup(6),
      Sl_xferAck => opb0_Sl_xferAck(6),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_ethout_dest_ip2_user_data_out,
      user_clk => adc0_clk
    );

  top_ethout_dest_ip3 : system_top_ethout_dest_ip3_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(224 to 255),
      Sl_errAck => opb0_Sl_errAck(7),
      Sl_retry => opb0_Sl_retry(7),
      Sl_toutSup => opb0_Sl_toutSup(7),
      Sl_xferAck => opb0_Sl_xferAck(7),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_ethout_dest_ip3_user_data_out,
      user_clk => adc0_clk
    );

  top_ethout_dest_port0 : system_top_ethout_dest_port0_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(256 to 287),
      Sl_errAck => opb0_Sl_errAck(8),
      Sl_retry => opb0_Sl_retry(8),
      Sl_toutSup => opb0_Sl_toutSup(8),
      Sl_xferAck => opb0_Sl_xferAck(8),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_ethout_dest_port0_user_data_out,
      user_clk => adc0_clk
    );

  top_ethout_dest_port1 : system_top_ethout_dest_port1_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(288 to 319),
      Sl_errAck => opb0_Sl_errAck(9),
      Sl_retry => opb0_Sl_retry(9),
      Sl_toutSup => opb0_Sl_toutSup(9),
      Sl_xferAck => opb0_Sl_xferAck(9),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_ethout_dest_port1_user_data_out,
      user_clk => adc0_clk
    );

  top_ethout_dest_port2 : system_top_ethout_dest_port2_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(320 to 351),
      Sl_errAck => opb0_Sl_errAck(10),
      Sl_retry => opb0_Sl_retry(10),
      Sl_toutSup => opb0_Sl_toutSup(10),
      Sl_xferAck => opb0_Sl_xferAck(10),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_ethout_dest_port2_user_data_out,
      user_clk => adc0_clk
    );

  top_ethout_dest_port3 : system_top_ethout_dest_port3_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(352 to 383),
      Sl_errAck => opb0_Sl_errAck(11),
      Sl_retry => opb0_Sl_retry(11),
      Sl_toutSup => opb0_Sl_toutSup(11),
      Sl_xferAck => opb0_Sl_xferAck(11),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_ethout_dest_port3_user_data_out,
      user_clk => adc0_clk
    );

  top_ethout_rst : system_top_ethout_rst_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(384 to 415),
      Sl_errAck => opb0_Sl_errAck(12),
      Sl_retry => opb0_Sl_retry(12),
      Sl_toutSup => opb0_Sl_toutSup(12),
      Sl_xferAck => opb0_Sl_xferAck(12),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_ethout_rst_user_data_out,
      user_clk => adc0_clk
    );

  top_ethout_ten_Gbe0 : system_top_ethout_ten_gbe0_wrapper
    port map (
      clk => adc0_clk,
      rst => top_ethout_ten_Gbe0_rst,
      tx_valid => top_ethout_ten_Gbe0_tx_valid,
      tx_afull => top_ethout_ten_Gbe0_tx_afull,
      tx_overflow => top_ethout_ten_Gbe0_tx_overflow,
      tx_end_of_frame => top_ethout_ten_Gbe0_tx_end_of_frame,
      tx_data => top_ethout_ten_Gbe0_tx_data,
      tx_dest_ip => top_ethout_ten_Gbe0_tx_dest_ip,
      tx_dest_port => top_ethout_ten_Gbe0_tx_dest_port,
      rx_valid => top_ethout_ten_Gbe0_rx_valid,
      rx_end_of_frame => top_ethout_ten_Gbe0_rx_end_of_frame,
      rx_data => top_ethout_ten_Gbe0_rx_data,
      rx_source_ip => top_ethout_ten_Gbe0_rx_source_ip,
      rx_source_port => top_ethout_ten_Gbe0_rx_source_port,
      rx_bad_frame => top_ethout_ten_Gbe0_rx_bad_frame,
      rx_overrun => top_ethout_ten_Gbe0_rx_overrun,
      rx_overrun_ack => top_ethout_ten_Gbe0_rx_overrun_ack,
      rx_ack => top_ethout_ten_Gbe0_rx_ack,
      led_up => top_ethout_ten_Gbe0_led_up,
      led_rx => top_ethout_ten_Gbe0_led_rx,
      led_tx => top_ethout_ten_Gbe0_led_tx,
      xaui_clk => xaui_clk,
      xgmii_txd => xgmii0_xgmii_txd,
      xgmii_txc => xgmii0_xgmii_txc,
      xgmii_rxd => xgmii0_xgmii_rxd,
      xgmii_rxc => xgmii0_xgmii_rxc,
      xaui_reset => sys_reset,
      mgt_txpostemphasis => phy_conf0_mgt_txpostemphasis,
      mgt_txpreemphasis => phy_conf0_mgt_txpreemphasis,
      mgt_txdiffctrl => phy_conf0_mgt_txdiffctrl,
      mgt_rxeqmix => phy_conf0_mgt_rxeqmix,
      xaui_status => xaui_conf0_xaui_status,
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(416 to 447),
      Sl_errAck => opb0_Sl_errAck(13),
      Sl_retry => opb0_Sl_retry(13),
      Sl_toutSup => opb0_Sl_toutSup(13),
      Sl_xferAck => opb0_Sl_xferAck(13),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr
    );

  xaui_phy_0 : system_xaui_phy_0_wrapper
    port map (
      reset => sys_reset,
      xaui_clk => xaui_clk,
      mgt_txdata => xaui_sys0_mgt_txdata,
      mgt_txcharisk => xaui_sys0_mgt_txcharisk,
      mgt_rxdata => xaui_sys0_mgt_rxdata,
      mgt_rxcharisk => xaui_sys0_mgt_rxcharisk,
      mgt_enable_align => xaui_sys0_mgt_encommaalign,
      mgt_code_valid => xaui_sys0_mgt_rxcodevalid,
      mgt_code_comma => xaui_sys0_mgt_rxcodecomma,
      mgt_rxlock => xaui_sys0_mgt_rxlock,
      mgt_rxbufferr => xaui_sys0_mgt_rxbufferr,
      mgt_syncok => xaui_sys0_mgt_rxsyncok,
      mgt_en_chan_sync => xaui_sys0_mgt_enchansync(0),
      mgt_rx_reset => xaui_sys0_mgt_rx_reset(0),
      mgt_tx_reset => xaui_sys0_mgt_tx_reset(0),
      xaui_status => xaui_conf0_xaui_status,
      xgmii_txd => xgmii0_xgmii_txd,
      xgmii_txc => xgmii0_xgmii_txc,
      xgmii_rxd => xgmii0_xgmii_rxd,
      xgmii_rxc => xgmii0_xgmii_rxc
    );

  top_ethout_ten_Gbe1 : system_top_ethout_ten_gbe1_wrapper
    port map (
      clk => adc0_clk,
      rst => top_ethout_ten_Gbe1_rst,
      tx_valid => top_ethout_ten_Gbe1_tx_valid,
      tx_afull => top_ethout_ten_Gbe1_tx_afull,
      tx_overflow => top_ethout_ten_Gbe1_tx_overflow,
      tx_end_of_frame => top_ethout_ten_Gbe1_tx_end_of_frame,
      tx_data => top_ethout_ten_Gbe1_tx_data,
      tx_dest_ip => top_ethout_ten_Gbe1_tx_dest_ip,
      tx_dest_port => top_ethout_ten_Gbe1_tx_dest_port,
      rx_valid => top_ethout_ten_Gbe1_rx_valid,
      rx_end_of_frame => top_ethout_ten_Gbe1_rx_end_of_frame,
      rx_data => top_ethout_ten_Gbe1_rx_data,
      rx_source_ip => top_ethout_ten_Gbe1_rx_source_ip,
      rx_source_port => top_ethout_ten_Gbe1_rx_source_port,
      rx_bad_frame => top_ethout_ten_Gbe1_rx_bad_frame,
      rx_overrun => top_ethout_ten_Gbe1_rx_overrun,
      rx_overrun_ack => top_ethout_ten_Gbe1_rx_overrun_ack,
      rx_ack => top_ethout_ten_Gbe1_rx_ack,
      led_up => top_ethout_ten_Gbe1_led_up,
      led_rx => top_ethout_ten_Gbe1_led_rx,
      led_tx => top_ethout_ten_Gbe1_led_tx,
      xaui_clk => xaui_clk,
      xgmii_txd => xgmii1_xgmii_txd,
      xgmii_txc => xgmii1_xgmii_txc,
      xgmii_rxd => xgmii1_xgmii_rxd,
      xgmii_rxc => xgmii1_xgmii_rxc,
      xaui_reset => sys_reset,
      mgt_txpostemphasis => phy_conf1_mgt_txpostemphasis,
      mgt_txpreemphasis => phy_conf1_mgt_txpreemphasis,
      mgt_txdiffctrl => phy_conf1_mgt_txdiffctrl,
      mgt_rxeqmix => phy_conf1_mgt_rxeqmix,
      xaui_status => xaui_conf1_xaui_status,
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(448 to 479),
      Sl_errAck => opb0_Sl_errAck(14),
      Sl_retry => opb0_Sl_retry(14),
      Sl_toutSup => opb0_Sl_toutSup(14),
      Sl_xferAck => opb0_Sl_xferAck(14),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr
    );

  xaui_phy_1 : system_xaui_phy_1_wrapper
    port map (
      reset => sys_reset,
      xaui_clk => xaui_clk,
      mgt_txdata => xaui_sys1_mgt_txdata,
      mgt_txcharisk => xaui_sys1_mgt_txcharisk,
      mgt_rxdata => xaui_sys1_mgt_rxdata,
      mgt_rxcharisk => xaui_sys1_mgt_rxcharisk,
      mgt_enable_align => xaui_sys1_mgt_encommaalign,
      mgt_code_valid => xaui_sys1_mgt_rxcodevalid,
      mgt_code_comma => xaui_sys1_mgt_rxcodecomma,
      mgt_rxlock => xaui_sys1_mgt_rxlock,
      mgt_rxbufferr => xaui_sys1_mgt_rxbufferr,
      mgt_syncok => xaui_sys1_mgt_rxsyncok,
      mgt_en_chan_sync => xaui_sys1_mgt_enchansync(0),
      mgt_rx_reset => xaui_sys1_mgt_rx_reset(0),
      mgt_tx_reset => xaui_sys1_mgt_tx_reset(0),
      xaui_status => xaui_conf1_xaui_status,
      xgmii_txd => xgmii1_xgmii_txd,
      xgmii_txc => xgmii1_xgmii_txc,
      xgmii_rxd => xgmii1_xgmii_rxd,
      xgmii_rxc => xgmii1_xgmii_rxc
    );

  top_ethout_ten_Gbe2 : system_top_ethout_ten_gbe2_wrapper
    port map (
      clk => adc0_clk,
      rst => top_ethout_ten_Gbe2_rst,
      tx_valid => top_ethout_ten_Gbe2_tx_valid,
      tx_afull => top_ethout_ten_Gbe2_tx_afull,
      tx_overflow => top_ethout_ten_Gbe2_tx_overflow,
      tx_end_of_frame => top_ethout_ten_Gbe2_tx_end_of_frame,
      tx_data => top_ethout_ten_Gbe2_tx_data,
      tx_dest_ip => top_ethout_ten_Gbe2_tx_dest_ip,
      tx_dest_port => top_ethout_ten_Gbe2_tx_dest_port,
      rx_valid => top_ethout_ten_Gbe2_rx_valid,
      rx_end_of_frame => top_ethout_ten_Gbe2_rx_end_of_frame,
      rx_data => top_ethout_ten_Gbe2_rx_data,
      rx_source_ip => top_ethout_ten_Gbe2_rx_source_ip,
      rx_source_port => top_ethout_ten_Gbe2_rx_source_port,
      rx_bad_frame => top_ethout_ten_Gbe2_rx_bad_frame,
      rx_overrun => top_ethout_ten_Gbe2_rx_overrun,
      rx_overrun_ack => top_ethout_ten_Gbe2_rx_overrun_ack,
      rx_ack => top_ethout_ten_Gbe2_rx_ack,
      led_up => top_ethout_ten_Gbe2_led_up,
      led_rx => top_ethout_ten_Gbe2_led_rx,
      led_tx => top_ethout_ten_Gbe2_led_tx,
      xaui_clk => xaui_clk,
      xgmii_txd => xgmii2_xgmii_txd,
      xgmii_txc => xgmii2_xgmii_txc,
      xgmii_rxd => xgmii2_xgmii_rxd,
      xgmii_rxc => xgmii2_xgmii_rxc,
      xaui_reset => sys_reset,
      mgt_txpostemphasis => phy_conf2_mgt_txpostemphasis,
      mgt_txpreemphasis => phy_conf2_mgt_txpreemphasis,
      mgt_txdiffctrl => phy_conf2_mgt_txdiffctrl,
      mgt_rxeqmix => phy_conf2_mgt_rxeqmix,
      xaui_status => xaui_conf2_xaui_status,
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(480 to 511),
      Sl_errAck => opb0_Sl_errAck(15),
      Sl_retry => opb0_Sl_retry(15),
      Sl_toutSup => opb0_Sl_toutSup(15),
      Sl_xferAck => opb0_Sl_xferAck(15),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr
    );

  xaui_phy_2 : system_xaui_phy_2_wrapper
    port map (
      reset => sys_reset,
      xaui_clk => xaui_clk,
      mgt_txdata => xaui_sys2_mgt_txdata,
      mgt_txcharisk => xaui_sys2_mgt_txcharisk,
      mgt_rxdata => xaui_sys2_mgt_rxdata,
      mgt_rxcharisk => xaui_sys2_mgt_rxcharisk,
      mgt_enable_align => xaui_sys2_mgt_encommaalign,
      mgt_code_valid => xaui_sys2_mgt_rxcodevalid,
      mgt_code_comma => xaui_sys2_mgt_rxcodecomma,
      mgt_rxlock => xaui_sys2_mgt_rxlock,
      mgt_rxbufferr => xaui_sys2_mgt_rxbufferr,
      mgt_syncok => xaui_sys2_mgt_rxsyncok,
      mgt_en_chan_sync => xaui_sys2_mgt_enchansync(0),
      mgt_rx_reset => xaui_sys2_mgt_rx_reset(0),
      mgt_tx_reset => xaui_sys2_mgt_tx_reset(0),
      xaui_status => xaui_conf2_xaui_status,
      xgmii_txd => xgmii2_xgmii_txd,
      xgmii_txc => xgmii2_xgmii_txc,
      xgmii_rxd => xgmii2_xgmii_rxd,
      xgmii_rxc => xgmii2_xgmii_rxc
    );

  top_ethout_ten_Gbe3 : system_top_ethout_ten_gbe3_wrapper
    port map (
      clk => adc0_clk,
      rst => top_ethout_ten_Gbe3_rst,
      tx_valid => top_ethout_ten_Gbe3_tx_valid,
      tx_afull => top_ethout_ten_Gbe3_tx_afull,
      tx_overflow => top_ethout_ten_Gbe3_tx_overflow,
      tx_end_of_frame => top_ethout_ten_Gbe3_tx_end_of_frame,
      tx_data => top_ethout_ten_Gbe3_tx_data,
      tx_dest_ip => top_ethout_ten_Gbe3_tx_dest_ip,
      tx_dest_port => top_ethout_ten_Gbe3_tx_dest_port,
      rx_valid => top_ethout_ten_Gbe3_rx_valid,
      rx_end_of_frame => top_ethout_ten_Gbe3_rx_end_of_frame,
      rx_data => top_ethout_ten_Gbe3_rx_data,
      rx_source_ip => top_ethout_ten_Gbe3_rx_source_ip,
      rx_source_port => top_ethout_ten_Gbe3_rx_source_port,
      rx_bad_frame => top_ethout_ten_Gbe3_rx_bad_frame,
      rx_overrun => top_ethout_ten_Gbe3_rx_overrun,
      rx_overrun_ack => top_ethout_ten_Gbe3_rx_overrun_ack,
      rx_ack => top_ethout_ten_Gbe3_rx_ack,
      led_up => top_ethout_ten_Gbe3_led_up,
      led_rx => top_ethout_ten_Gbe3_led_rx,
      led_tx => top_ethout_ten_Gbe3_led_tx,
      xaui_clk => xaui_clk,
      xgmii_txd => xgmii3_xgmii_txd,
      xgmii_txc => xgmii3_xgmii_txc,
      xgmii_rxd => xgmii3_xgmii_rxd,
      xgmii_rxc => xgmii3_xgmii_rxc,
      xaui_reset => sys_reset,
      mgt_txpostemphasis => phy_conf3_mgt_txpostemphasis,
      mgt_txpreemphasis => phy_conf3_mgt_txpreemphasis,
      mgt_txdiffctrl => phy_conf3_mgt_txdiffctrl,
      mgt_rxeqmix => phy_conf3_mgt_rxeqmix,
      xaui_status => xaui_conf3_xaui_status,
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(512 to 543),
      Sl_errAck => opb0_Sl_errAck(16),
      Sl_retry => opb0_Sl_retry(16),
      Sl_toutSup => opb0_Sl_toutSup(16),
      Sl_xferAck => opb0_Sl_xferAck(16),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr
    );

  xaui_phy_3 : system_xaui_phy_3_wrapper
    port map (
      reset => sys_reset,
      xaui_clk => xaui_clk,
      mgt_txdata => xaui_sys3_mgt_txdata,
      mgt_txcharisk => xaui_sys3_mgt_txcharisk,
      mgt_rxdata => xaui_sys3_mgt_rxdata,
      mgt_rxcharisk => xaui_sys3_mgt_rxcharisk,
      mgt_enable_align => xaui_sys3_mgt_encommaalign,
      mgt_code_valid => xaui_sys3_mgt_rxcodevalid,
      mgt_code_comma => xaui_sys3_mgt_rxcodecomma,
      mgt_rxlock => xaui_sys3_mgt_rxlock,
      mgt_rxbufferr => xaui_sys3_mgt_rxbufferr,
      mgt_syncok => xaui_sys3_mgt_rxsyncok,
      mgt_en_chan_sync => xaui_sys3_mgt_enchansync(0),
      mgt_rx_reset => xaui_sys3_mgt_rx_reset(0),
      mgt_tx_reset => xaui_sys3_mgt_tx_reset(0),
      xaui_status => xaui_conf3_xaui_status,
      xgmii_txd => xgmii3_xgmii_txd,
      xgmii_txc => xgmii3_xgmii_txc,
      xgmii_rxd => xgmii3_xgmii_rxd,
      xgmii_rxc => xgmii3_xgmii_rxc
    );

  top_scope_raw_0_snap_bram_ramblk : system_top_scope_raw_0_snap_bram_ramblk_wrapper
    port map (
      clk => adc0_clk,
      bram_we => top_scope_raw_0_snap_bram_we,
      bram_en_a => net_gnd0,
      bram_addr => top_scope_raw_0_snap_bram_addr,
      bram_rd_data => top_scope_raw_0_snap_bram_data_out,
      bram_wr_data => top_scope_raw_0_snap_bram_data_in,
      BRAM_Rst_B => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Rst,
      BRAM_Clk_B => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Clk,
      BRAM_EN_B => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_EN,
      BRAM_WEN_B => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_WEN,
      BRAM_Addr_B => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Addr,
      BRAM_Din_B => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Din,
      BRAM_Dout_B => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Dout
    );

  top_scope_raw_0_snap_bram : system_top_scope_raw_0_snap_bram_wrapper
    port map (
      opb_clk => epb_clk,
      opb_rst => opb0_OPB_Rst,
      opb_abus => opb0_OPB_ABus,
      opb_dbus => opb0_OPB_DBus,
      sln_dbus => opb0_Sl_DBus(544 to 575),
      opb_select => opb0_OPB_select,
      opb_rnw => opb0_OPB_RNW,
      opb_seqaddr => opb0_OPB_seqAddr,
      opb_be => opb0_OPB_BE,
      sln_xferack => opb0_Sl_xferAck(17),
      sln_errack => opb0_Sl_errAck(17),
      sln_toutsup => opb0_Sl_toutSup(17),
      sln_retry => opb0_Sl_retry(17),
      bram_rst => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Rst,
      bram_clk => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Clk,
      bram_en => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_EN,
      bram_wen => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_WEN,
      bram_addr => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Addr,
      bram_din => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Din,
      bram_dout => top_scope_raw_0_snap_bram_ramblk_portb_BRAM_Dout
    );

  top_scope_raw_0_snap_ctrl : system_top_scope_raw_0_snap_ctrl_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(576 to 607),
      Sl_errAck => opb0_Sl_errAck(18),
      Sl_retry => opb0_Sl_retry(18),
      Sl_toutSup => opb0_Sl_toutSup(18),
      Sl_xferAck => opb0_Sl_xferAck(18),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_out => top_scope_raw_0_snap_ctrl_user_data_out,
      user_clk => adc0_clk
    );

  top_scope_raw_0_snap_status : system_top_scope_raw_0_snap_status_wrapper
    port map (
      OPB_Clk => epb_clk,
      OPB_Rst => opb0_OPB_Rst,
      Sl_DBus => opb0_Sl_DBus(608 to 639),
      Sl_errAck => opb0_Sl_errAck(19),
      Sl_retry => opb0_Sl_retry(19),
      Sl_toutSup => opb0_Sl_toutSup(19),
      Sl_xferAck => opb0_Sl_xferAck(19),
      OPB_ABus => opb0_OPB_ABus,
      OPB_BE => opb0_OPB_BE,
      OPB_DBus => opb0_OPB_DBus,
      OPB_RNW => opb0_OPB_RNW,
      OPB_select => opb0_OPB_select,
      OPB_seqAddr => opb0_OPB_seqAddr,
      user_data_in => top_scope_raw_0_snap_status_user_data_in,
      user_clk => adc0_clk
    );

  clk250_to_clk125_0 : system_clk250_to_clk125_0_wrapper
    port map (
      clk_250 => adc0_clk,
      clk_125 => top_adc_adc_ctrl_clk125m_out,
      data_in => top_data_bypass_topout_data_out,
      data_in_vld => top_data_bypass_topout_data_out_vld,
      rst_n => top_data_bypass_topout_rst_n,
      data_out => clk250_to_clk125_0_data_out,
      data_out_vld => clk250_to_clk125_0_data_out_vld
    );

  clk125_to_clk250_0 : system_clk125_to_clk250_0_wrapper
    port map (
      clk_250 => adc0_clk,
      clk_125 => top_adc_adc_ctrl_clk125m_out,
      data_in => interleave_sampling_0_data_out,
      data_in_vld => interleave_sampling_0_wr_en,
      rst_n => top_data_bypass_topout_rst_n,
      data_out => clk125_to_clk250_0_data_out,
      data_out_vld => clk125_to_clk250_0_data_out_vld
    );

  interleave_sampling_0 : system_interleave_sampling_0_wrapper
    port map (
      clk => top_adc_adc_ctrl_clk125m_out,
      rst_n => top_data_bypass_topout_rst_n,
      en => clk250_to_clk125_0_data_out_vld,
      data_in => clk250_to_clk125_0_data_out,
      data_out => interleave_sampling_0_data_out,
      wr_en => interleave_sampling_0_wr_en
    );

end architecture STRUCTURE;

