-------------------------------------------------------------------------------
-- system_top_xsg_core_config_wrapper.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity system_top_xsg_core_config_wrapper is
  port (
    clk : in std_logic;
    top_adc_adc_sync : in std_logic;
    top_adc_adc_user_data_i0 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_i1 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_i2 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_i3 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_i4 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_i5 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_i6 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_i7 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_q0 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_q1 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_q2 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_q3 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_q4 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_q5 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_q6 : in std_logic_vector(7 downto 0);
    top_adc_adc_user_data_q7 : in std_logic_vector(7 downto 0);
    top_adc_enable_user_data_out : in std_logic_vector(31 downto 0);
    top_data_bypass_topin_data_out : in std_logic_vector(127 downto 0);
    top_data_bypass_topin_data_out_vld : in std_logic;
    top_data_bypass_topin_data_in : out std_logic_vector(127 downto 0);
    top_data_bypass_topin_en : out std_logic;
    top_data_bypass_topin_rst_n : out std_logic;
    top_data_bypass_topout_data_out : in std_logic_vector(127 downto 0);
    top_data_bypass_topout_data_out_vld : in std_logic;
    top_data_bypass_topout_data_in : out std_logic_vector(127 downto 0);
    top_data_bypass_topout_en : out std_logic;
    top_data_bypass_topout_rst_n : out std_logic;
    top_ethout_dest_ip0_user_data_out : in std_logic_vector(31 downto 0);
    top_ethout_dest_ip1_user_data_out : in std_logic_vector(31 downto 0);
    top_ethout_dest_ip2_user_data_out : in std_logic_vector(31 downto 0);
    top_ethout_dest_ip3_user_data_out : in std_logic_vector(31 downto 0);
    top_ethout_dest_port0_user_data_out : in std_logic_vector(31 downto 0);
    top_ethout_dest_port1_user_data_out : in std_logic_vector(31 downto 0);
    top_ethout_dest_port2_user_data_out : in std_logic_vector(31 downto 0);
    top_ethout_dest_port3_user_data_out : in std_logic_vector(31 downto 0);
    top_ethout_rst_user_data_out : in std_logic_vector(31 downto 0);
    top_ethout_ten_Gbe0_led_rx : in std_logic;
    top_ethout_ten_Gbe0_led_tx : in std_logic;
    top_ethout_ten_Gbe0_led_up : in std_logic;
    top_ethout_ten_Gbe0_rx_bad_frame : in std_logic;
    top_ethout_ten_Gbe0_rx_data : in std_logic_vector(63 downto 0);
    top_ethout_ten_Gbe0_rx_end_of_frame : in std_logic;
    top_ethout_ten_Gbe0_rx_overrun : in std_logic;
    top_ethout_ten_Gbe0_rx_source_ip : in std_logic_vector(31 downto 0);
    top_ethout_ten_Gbe0_rx_source_port : in std_logic_vector(15 downto 0);
    top_ethout_ten_Gbe0_rx_valid : in std_logic;
    top_ethout_ten_Gbe0_tx_afull : in std_logic;
    top_ethout_ten_Gbe0_tx_overflow : in std_logic;
    top_ethout_ten_Gbe0_rst : out std_logic;
    top_ethout_ten_Gbe0_rx_ack : out std_logic;
    top_ethout_ten_Gbe0_rx_overrun_ack : out std_logic;
    top_ethout_ten_Gbe0_tx_data : out std_logic_vector(63 downto 0);
    top_ethout_ten_Gbe0_tx_dest_ip : out std_logic_vector(31 downto 0);
    top_ethout_ten_Gbe0_tx_dest_port : out std_logic_vector(15 downto 0);
    top_ethout_ten_Gbe0_tx_end_of_frame : out std_logic;
    top_ethout_ten_Gbe0_tx_valid : out std_logic;
    top_ethout_ten_Gbe1_led_rx : in std_logic;
    top_ethout_ten_Gbe1_led_tx : in std_logic;
    top_ethout_ten_Gbe1_led_up : in std_logic;
    top_ethout_ten_Gbe1_rx_bad_frame : in std_logic;
    top_ethout_ten_Gbe1_rx_data : in std_logic_vector(63 downto 0);
    top_ethout_ten_Gbe1_rx_end_of_frame : in std_logic;
    top_ethout_ten_Gbe1_rx_overrun : in std_logic;
    top_ethout_ten_Gbe1_rx_source_ip : in std_logic_vector(31 downto 0);
    top_ethout_ten_Gbe1_rx_source_port : in std_logic_vector(15 downto 0);
    top_ethout_ten_Gbe1_rx_valid : in std_logic;
    top_ethout_ten_Gbe1_tx_afull : in std_logic;
    top_ethout_ten_Gbe1_tx_overflow : in std_logic;
    top_ethout_ten_Gbe1_rst : out std_logic;
    top_ethout_ten_Gbe1_rx_ack : out std_logic;
    top_ethout_ten_Gbe1_rx_overrun_ack : out std_logic;
    top_ethout_ten_Gbe1_tx_data : out std_logic_vector(63 downto 0);
    top_ethout_ten_Gbe1_tx_dest_ip : out std_logic_vector(31 downto 0);
    top_ethout_ten_Gbe1_tx_dest_port : out std_logic_vector(15 downto 0);
    top_ethout_ten_Gbe1_tx_end_of_frame : out std_logic;
    top_ethout_ten_Gbe1_tx_valid : out std_logic;
    top_ethout_ten_Gbe2_led_rx : in std_logic;
    top_ethout_ten_Gbe2_led_tx : in std_logic;
    top_ethout_ten_Gbe2_led_up : in std_logic;
    top_ethout_ten_Gbe2_rx_bad_frame : in std_logic;
    top_ethout_ten_Gbe2_rx_data : in std_logic_vector(63 downto 0);
    top_ethout_ten_Gbe2_rx_end_of_frame : in std_logic;
    top_ethout_ten_Gbe2_rx_overrun : in std_logic;
    top_ethout_ten_Gbe2_rx_source_ip : in std_logic_vector(31 downto 0);
    top_ethout_ten_Gbe2_rx_source_port : in std_logic_vector(15 downto 0);
    top_ethout_ten_Gbe2_rx_valid : in std_logic;
    top_ethout_ten_Gbe2_tx_afull : in std_logic;
    top_ethout_ten_Gbe2_tx_overflow : in std_logic;
    top_ethout_ten_Gbe2_rst : out std_logic;
    top_ethout_ten_Gbe2_rx_ack : out std_logic;
    top_ethout_ten_Gbe2_rx_overrun_ack : out std_logic;
    top_ethout_ten_Gbe2_tx_data : out std_logic_vector(63 downto 0);
    top_ethout_ten_Gbe2_tx_dest_ip : out std_logic_vector(31 downto 0);
    top_ethout_ten_Gbe2_tx_dest_port : out std_logic_vector(15 downto 0);
    top_ethout_ten_Gbe2_tx_end_of_frame : out std_logic;
    top_ethout_ten_Gbe2_tx_valid : out std_logic;
    top_ethout_ten_Gbe3_led_rx : in std_logic;
    top_ethout_ten_Gbe3_led_tx : in std_logic;
    top_ethout_ten_Gbe3_led_up : in std_logic;
    top_ethout_ten_Gbe3_rx_bad_frame : in std_logic;
    top_ethout_ten_Gbe3_rx_data : in std_logic_vector(63 downto 0);
    top_ethout_ten_Gbe3_rx_end_of_frame : in std_logic;
    top_ethout_ten_Gbe3_rx_overrun : in std_logic;
    top_ethout_ten_Gbe3_rx_source_ip : in std_logic_vector(31 downto 0);
    top_ethout_ten_Gbe3_rx_source_port : in std_logic_vector(15 downto 0);
    top_ethout_ten_Gbe3_rx_valid : in std_logic;
    top_ethout_ten_Gbe3_tx_afull : in std_logic;
    top_ethout_ten_Gbe3_tx_overflow : in std_logic;
    top_ethout_ten_Gbe3_rst : out std_logic;
    top_ethout_ten_Gbe3_rx_ack : out std_logic;
    top_ethout_ten_Gbe3_rx_overrun_ack : out std_logic;
    top_ethout_ten_Gbe3_tx_data : out std_logic_vector(63 downto 0);
    top_ethout_ten_Gbe3_tx_dest_ip : out std_logic_vector(31 downto 0);
    top_ethout_ten_Gbe3_tx_dest_port : out std_logic_vector(15 downto 0);
    top_ethout_ten_Gbe3_tx_end_of_frame : out std_logic;
    top_ethout_ten_Gbe3_tx_valid : out std_logic;
    top_scope_raw_0_snap_bram_data_out : in std_logic_vector(127 downto 0);
    top_scope_raw_0_snap_bram_addr : out std_logic_vector(9 downto 0);
    top_scope_raw_0_snap_bram_data_in : out std_logic_vector(127 downto 0);
    top_scope_raw_0_snap_bram_we : out std_logic;
    top_scope_raw_0_snap_ctrl_user_data_out : in std_logic_vector(31 downto 0);
    top_scope_raw_0_snap_status_user_data_in : out std_logic_vector(31 downto 0)
  );
end system_top_xsg_core_config_wrapper;

architecture STRUCTURE of system_top_xsg_core_config_wrapper is

  component top is
    port (
      clk : in std_logic;
      top_adc_adc_sync : in std_logic;
      top_adc_adc_user_data_i0 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i1 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i2 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i3 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i4 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i5 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i6 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_i7 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q0 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q1 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q2 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q3 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q4 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q5 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q6 : in std_logic_vector(7 downto 0);
      top_adc_adc_user_data_q7 : in std_logic_vector(7 downto 0);
      top_adc_enable_user_data_out : in std_logic_vector(31 downto 0);
      top_data_bypass_topin_data_out : in std_logic_vector(127 downto 0);
      top_data_bypass_topin_data_out_vld : in std_logic;
      top_data_bypass_topin_data_in : out std_logic_vector(127 downto 0);
      top_data_bypass_topin_en : out std_logic;
      top_data_bypass_topin_rst_n : out std_logic;
      top_data_bypass_topout_data_out : in std_logic_vector(127 downto 0);
      top_data_bypass_topout_data_out_vld : in std_logic;
      top_data_bypass_topout_data_in : out std_logic_vector(127 downto 0);
      top_data_bypass_topout_en : out std_logic;
      top_data_bypass_topout_rst_n : out std_logic;
      top_ethout_dest_ip0_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_ip1_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_ip2_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_ip3_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_port0_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_port1_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_port2_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_dest_port3_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_rst_user_data_out : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe0_led_rx : in std_logic;
      top_ethout_ten_Gbe0_led_tx : in std_logic;
      top_ethout_ten_Gbe0_led_up : in std_logic;
      top_ethout_ten_Gbe0_rx_bad_frame : in std_logic;
      top_ethout_ten_Gbe0_rx_data : in std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe0_rx_end_of_frame : in std_logic;
      top_ethout_ten_Gbe0_rx_overrun : in std_logic;
      top_ethout_ten_Gbe0_rx_source_ip : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe0_rx_source_port : in std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe0_rx_valid : in std_logic;
      top_ethout_ten_Gbe0_tx_afull : in std_logic;
      top_ethout_ten_Gbe0_tx_overflow : in std_logic;
      top_ethout_ten_Gbe0_rst : out std_logic;
      top_ethout_ten_Gbe0_rx_ack : out std_logic;
      top_ethout_ten_Gbe0_rx_overrun_ack : out std_logic;
      top_ethout_ten_Gbe0_tx_data : out std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe0_tx_dest_ip : out std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe0_tx_dest_port : out std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe0_tx_end_of_frame : out std_logic;
      top_ethout_ten_Gbe0_tx_valid : out std_logic;
      top_ethout_ten_Gbe1_led_rx : in std_logic;
      top_ethout_ten_Gbe1_led_tx : in std_logic;
      top_ethout_ten_Gbe1_led_up : in std_logic;
      top_ethout_ten_Gbe1_rx_bad_frame : in std_logic;
      top_ethout_ten_Gbe1_rx_data : in std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe1_rx_end_of_frame : in std_logic;
      top_ethout_ten_Gbe1_rx_overrun : in std_logic;
      top_ethout_ten_Gbe1_rx_source_ip : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe1_rx_source_port : in std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe1_rx_valid : in std_logic;
      top_ethout_ten_Gbe1_tx_afull : in std_logic;
      top_ethout_ten_Gbe1_tx_overflow : in std_logic;
      top_ethout_ten_Gbe1_rst : out std_logic;
      top_ethout_ten_Gbe1_rx_ack : out std_logic;
      top_ethout_ten_Gbe1_rx_overrun_ack : out std_logic;
      top_ethout_ten_Gbe1_tx_data : out std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe1_tx_dest_ip : out std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe1_tx_dest_port : out std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe1_tx_end_of_frame : out std_logic;
      top_ethout_ten_Gbe1_tx_valid : out std_logic;
      top_ethout_ten_Gbe2_led_rx : in std_logic;
      top_ethout_ten_Gbe2_led_tx : in std_logic;
      top_ethout_ten_Gbe2_led_up : in std_logic;
      top_ethout_ten_Gbe2_rx_bad_frame : in std_logic;
      top_ethout_ten_Gbe2_rx_data : in std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe2_rx_end_of_frame : in std_logic;
      top_ethout_ten_Gbe2_rx_overrun : in std_logic;
      top_ethout_ten_Gbe2_rx_source_ip : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe2_rx_source_port : in std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe2_rx_valid : in std_logic;
      top_ethout_ten_Gbe2_tx_afull : in std_logic;
      top_ethout_ten_Gbe2_tx_overflow : in std_logic;
      top_ethout_ten_Gbe2_rst : out std_logic;
      top_ethout_ten_Gbe2_rx_ack : out std_logic;
      top_ethout_ten_Gbe2_rx_overrun_ack : out std_logic;
      top_ethout_ten_Gbe2_tx_data : out std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe2_tx_dest_ip : out std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe2_tx_dest_port : out std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe2_tx_end_of_frame : out std_logic;
      top_ethout_ten_Gbe2_tx_valid : out std_logic;
      top_ethout_ten_Gbe3_led_rx : in std_logic;
      top_ethout_ten_Gbe3_led_tx : in std_logic;
      top_ethout_ten_Gbe3_led_up : in std_logic;
      top_ethout_ten_Gbe3_rx_bad_frame : in std_logic;
      top_ethout_ten_Gbe3_rx_data : in std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe3_rx_end_of_frame : in std_logic;
      top_ethout_ten_Gbe3_rx_overrun : in std_logic;
      top_ethout_ten_Gbe3_rx_source_ip : in std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe3_rx_source_port : in std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe3_rx_valid : in std_logic;
      top_ethout_ten_Gbe3_tx_afull : in std_logic;
      top_ethout_ten_Gbe3_tx_overflow : in std_logic;
      top_ethout_ten_Gbe3_rst : out std_logic;
      top_ethout_ten_Gbe3_rx_ack : out std_logic;
      top_ethout_ten_Gbe3_rx_overrun_ack : out std_logic;
      top_ethout_ten_Gbe3_tx_data : out std_logic_vector(63 downto 0);
      top_ethout_ten_Gbe3_tx_dest_ip : out std_logic_vector(31 downto 0);
      top_ethout_ten_Gbe3_tx_dest_port : out std_logic_vector(15 downto 0);
      top_ethout_ten_Gbe3_tx_end_of_frame : out std_logic;
      top_ethout_ten_Gbe3_tx_valid : out std_logic;
      top_scope_raw_0_snap_bram_data_out : in std_logic_vector(127 downto 0);
      top_scope_raw_0_snap_bram_addr : out std_logic_vector(9 downto 0);
      top_scope_raw_0_snap_bram_data_in : out std_logic_vector(127 downto 0);
      top_scope_raw_0_snap_bram_we : out std_logic;
      top_scope_raw_0_snap_ctrl_user_data_out : in std_logic_vector(31 downto 0);
      top_scope_raw_0_snap_status_user_data_in : out std_logic_vector(31 downto 0)
    );
  end component;

  attribute BOX_TYPE : STRING;
  attribute BOX_TYPE of top : component is "user_black_box";

begin

  top_XSG_core_config : top
    port map (
      clk => clk,
      top_adc_adc_sync => top_adc_adc_sync,
      top_adc_adc_user_data_i0 => top_adc_adc_user_data_i0,
      top_adc_adc_user_data_i1 => top_adc_adc_user_data_i1,
      top_adc_adc_user_data_i2 => top_adc_adc_user_data_i2,
      top_adc_adc_user_data_i3 => top_adc_adc_user_data_i3,
      top_adc_adc_user_data_i4 => top_adc_adc_user_data_i4,
      top_adc_adc_user_data_i5 => top_adc_adc_user_data_i5,
      top_adc_adc_user_data_i6 => top_adc_adc_user_data_i6,
      top_adc_adc_user_data_i7 => top_adc_adc_user_data_i7,
      top_adc_adc_user_data_q0 => top_adc_adc_user_data_q0,
      top_adc_adc_user_data_q1 => top_adc_adc_user_data_q1,
      top_adc_adc_user_data_q2 => top_adc_adc_user_data_q2,
      top_adc_adc_user_data_q3 => top_adc_adc_user_data_q3,
      top_adc_adc_user_data_q4 => top_adc_adc_user_data_q4,
      top_adc_adc_user_data_q5 => top_adc_adc_user_data_q5,
      top_adc_adc_user_data_q6 => top_adc_adc_user_data_q6,
      top_adc_adc_user_data_q7 => top_adc_adc_user_data_q7,
      top_adc_enable_user_data_out => top_adc_enable_user_data_out,
      top_data_bypass_topin_data_out => top_data_bypass_topin_data_out,
      top_data_bypass_topin_data_out_vld => top_data_bypass_topin_data_out_vld,
      top_data_bypass_topin_data_in => top_data_bypass_topin_data_in,
      top_data_bypass_topin_en => top_data_bypass_topin_en,
      top_data_bypass_topin_rst_n => top_data_bypass_topin_rst_n,
      top_data_bypass_topout_data_out => top_data_bypass_topout_data_out,
      top_data_bypass_topout_data_out_vld => top_data_bypass_topout_data_out_vld,
      top_data_bypass_topout_data_in => top_data_bypass_topout_data_in,
      top_data_bypass_topout_en => top_data_bypass_topout_en,
      top_data_bypass_topout_rst_n => top_data_bypass_topout_rst_n,
      top_ethout_dest_ip0_user_data_out => top_ethout_dest_ip0_user_data_out,
      top_ethout_dest_ip1_user_data_out => top_ethout_dest_ip1_user_data_out,
      top_ethout_dest_ip2_user_data_out => top_ethout_dest_ip2_user_data_out,
      top_ethout_dest_ip3_user_data_out => top_ethout_dest_ip3_user_data_out,
      top_ethout_dest_port0_user_data_out => top_ethout_dest_port0_user_data_out,
      top_ethout_dest_port1_user_data_out => top_ethout_dest_port1_user_data_out,
      top_ethout_dest_port2_user_data_out => top_ethout_dest_port2_user_data_out,
      top_ethout_dest_port3_user_data_out => top_ethout_dest_port3_user_data_out,
      top_ethout_rst_user_data_out => top_ethout_rst_user_data_out,
      top_ethout_ten_Gbe0_led_rx => top_ethout_ten_Gbe0_led_rx,
      top_ethout_ten_Gbe0_led_tx => top_ethout_ten_Gbe0_led_tx,
      top_ethout_ten_Gbe0_led_up => top_ethout_ten_Gbe0_led_up,
      top_ethout_ten_Gbe0_rx_bad_frame => top_ethout_ten_Gbe0_rx_bad_frame,
      top_ethout_ten_Gbe0_rx_data => top_ethout_ten_Gbe0_rx_data,
      top_ethout_ten_Gbe0_rx_end_of_frame => top_ethout_ten_Gbe0_rx_end_of_frame,
      top_ethout_ten_Gbe0_rx_overrun => top_ethout_ten_Gbe0_rx_overrun,
      top_ethout_ten_Gbe0_rx_source_ip => top_ethout_ten_Gbe0_rx_source_ip,
      top_ethout_ten_Gbe0_rx_source_port => top_ethout_ten_Gbe0_rx_source_port,
      top_ethout_ten_Gbe0_rx_valid => top_ethout_ten_Gbe0_rx_valid,
      top_ethout_ten_Gbe0_tx_afull => top_ethout_ten_Gbe0_tx_afull,
      top_ethout_ten_Gbe0_tx_overflow => top_ethout_ten_Gbe0_tx_overflow,
      top_ethout_ten_Gbe0_rst => top_ethout_ten_Gbe0_rst,
      top_ethout_ten_Gbe0_rx_ack => top_ethout_ten_Gbe0_rx_ack,
      top_ethout_ten_Gbe0_rx_overrun_ack => top_ethout_ten_Gbe0_rx_overrun_ack,
      top_ethout_ten_Gbe0_tx_data => top_ethout_ten_Gbe0_tx_data,
      top_ethout_ten_Gbe0_tx_dest_ip => top_ethout_ten_Gbe0_tx_dest_ip,
      top_ethout_ten_Gbe0_tx_dest_port => top_ethout_ten_Gbe0_tx_dest_port,
      top_ethout_ten_Gbe0_tx_end_of_frame => top_ethout_ten_Gbe0_tx_end_of_frame,
      top_ethout_ten_Gbe0_tx_valid => top_ethout_ten_Gbe0_tx_valid,
      top_ethout_ten_Gbe1_led_rx => top_ethout_ten_Gbe1_led_rx,
      top_ethout_ten_Gbe1_led_tx => top_ethout_ten_Gbe1_led_tx,
      top_ethout_ten_Gbe1_led_up => top_ethout_ten_Gbe1_led_up,
      top_ethout_ten_Gbe1_rx_bad_frame => top_ethout_ten_Gbe1_rx_bad_frame,
      top_ethout_ten_Gbe1_rx_data => top_ethout_ten_Gbe1_rx_data,
      top_ethout_ten_Gbe1_rx_end_of_frame => top_ethout_ten_Gbe1_rx_end_of_frame,
      top_ethout_ten_Gbe1_rx_overrun => top_ethout_ten_Gbe1_rx_overrun,
      top_ethout_ten_Gbe1_rx_source_ip => top_ethout_ten_Gbe1_rx_source_ip,
      top_ethout_ten_Gbe1_rx_source_port => top_ethout_ten_Gbe1_rx_source_port,
      top_ethout_ten_Gbe1_rx_valid => top_ethout_ten_Gbe1_rx_valid,
      top_ethout_ten_Gbe1_tx_afull => top_ethout_ten_Gbe1_tx_afull,
      top_ethout_ten_Gbe1_tx_overflow => top_ethout_ten_Gbe1_tx_overflow,
      top_ethout_ten_Gbe1_rst => top_ethout_ten_Gbe1_rst,
      top_ethout_ten_Gbe1_rx_ack => top_ethout_ten_Gbe1_rx_ack,
      top_ethout_ten_Gbe1_rx_overrun_ack => top_ethout_ten_Gbe1_rx_overrun_ack,
      top_ethout_ten_Gbe1_tx_data => top_ethout_ten_Gbe1_tx_data,
      top_ethout_ten_Gbe1_tx_dest_ip => top_ethout_ten_Gbe1_tx_dest_ip,
      top_ethout_ten_Gbe1_tx_dest_port => top_ethout_ten_Gbe1_tx_dest_port,
      top_ethout_ten_Gbe1_tx_end_of_frame => top_ethout_ten_Gbe1_tx_end_of_frame,
      top_ethout_ten_Gbe1_tx_valid => top_ethout_ten_Gbe1_tx_valid,
      top_ethout_ten_Gbe2_led_rx => top_ethout_ten_Gbe2_led_rx,
      top_ethout_ten_Gbe2_led_tx => top_ethout_ten_Gbe2_led_tx,
      top_ethout_ten_Gbe2_led_up => top_ethout_ten_Gbe2_led_up,
      top_ethout_ten_Gbe2_rx_bad_frame => top_ethout_ten_Gbe2_rx_bad_frame,
      top_ethout_ten_Gbe2_rx_data => top_ethout_ten_Gbe2_rx_data,
      top_ethout_ten_Gbe2_rx_end_of_frame => top_ethout_ten_Gbe2_rx_end_of_frame,
      top_ethout_ten_Gbe2_rx_overrun => top_ethout_ten_Gbe2_rx_overrun,
      top_ethout_ten_Gbe2_rx_source_ip => top_ethout_ten_Gbe2_rx_source_ip,
      top_ethout_ten_Gbe2_rx_source_port => top_ethout_ten_Gbe2_rx_source_port,
      top_ethout_ten_Gbe2_rx_valid => top_ethout_ten_Gbe2_rx_valid,
      top_ethout_ten_Gbe2_tx_afull => top_ethout_ten_Gbe2_tx_afull,
      top_ethout_ten_Gbe2_tx_overflow => top_ethout_ten_Gbe2_tx_overflow,
      top_ethout_ten_Gbe2_rst => top_ethout_ten_Gbe2_rst,
      top_ethout_ten_Gbe2_rx_ack => top_ethout_ten_Gbe2_rx_ack,
      top_ethout_ten_Gbe2_rx_overrun_ack => top_ethout_ten_Gbe2_rx_overrun_ack,
      top_ethout_ten_Gbe2_tx_data => top_ethout_ten_Gbe2_tx_data,
      top_ethout_ten_Gbe2_tx_dest_ip => top_ethout_ten_Gbe2_tx_dest_ip,
      top_ethout_ten_Gbe2_tx_dest_port => top_ethout_ten_Gbe2_tx_dest_port,
      top_ethout_ten_Gbe2_tx_end_of_frame => top_ethout_ten_Gbe2_tx_end_of_frame,
      top_ethout_ten_Gbe2_tx_valid => top_ethout_ten_Gbe2_tx_valid,
      top_ethout_ten_Gbe3_led_rx => top_ethout_ten_Gbe3_led_rx,
      top_ethout_ten_Gbe3_led_tx => top_ethout_ten_Gbe3_led_tx,
      top_ethout_ten_Gbe3_led_up => top_ethout_ten_Gbe3_led_up,
      top_ethout_ten_Gbe3_rx_bad_frame => top_ethout_ten_Gbe3_rx_bad_frame,
      top_ethout_ten_Gbe3_rx_data => top_ethout_ten_Gbe3_rx_data,
      top_ethout_ten_Gbe3_rx_end_of_frame => top_ethout_ten_Gbe3_rx_end_of_frame,
      top_ethout_ten_Gbe3_rx_overrun => top_ethout_ten_Gbe3_rx_overrun,
      top_ethout_ten_Gbe3_rx_source_ip => top_ethout_ten_Gbe3_rx_source_ip,
      top_ethout_ten_Gbe3_rx_source_port => top_ethout_ten_Gbe3_rx_source_port,
      top_ethout_ten_Gbe3_rx_valid => top_ethout_ten_Gbe3_rx_valid,
      top_ethout_ten_Gbe3_tx_afull => top_ethout_ten_Gbe3_tx_afull,
      top_ethout_ten_Gbe3_tx_overflow => top_ethout_ten_Gbe3_tx_overflow,
      top_ethout_ten_Gbe3_rst => top_ethout_ten_Gbe3_rst,
      top_ethout_ten_Gbe3_rx_ack => top_ethout_ten_Gbe3_rx_ack,
      top_ethout_ten_Gbe3_rx_overrun_ack => top_ethout_ten_Gbe3_rx_overrun_ack,
      top_ethout_ten_Gbe3_tx_data => top_ethout_ten_Gbe3_tx_data,
      top_ethout_ten_Gbe3_tx_dest_ip => top_ethout_ten_Gbe3_tx_dest_ip,
      top_ethout_ten_Gbe3_tx_dest_port => top_ethout_ten_Gbe3_tx_dest_port,
      top_ethout_ten_Gbe3_tx_end_of_frame => top_ethout_ten_Gbe3_tx_end_of_frame,
      top_ethout_ten_Gbe3_tx_valid => top_ethout_ten_Gbe3_tx_valid,
      top_scope_raw_0_snap_bram_data_out => top_scope_raw_0_snap_bram_data_out,
      top_scope_raw_0_snap_bram_addr => top_scope_raw_0_snap_bram_addr,
      top_scope_raw_0_snap_bram_data_in => top_scope_raw_0_snap_bram_data_in,
      top_scope_raw_0_snap_bram_we => top_scope_raw_0_snap_bram_we,
      top_scope_raw_0_snap_ctrl_user_data_out => top_scope_raw_0_snap_ctrl_user_data_out,
      top_scope_raw_0_snap_status_user_data_in => top_scope_raw_0_snap_status_user_data_in
    );

end architecture STRUCTURE;

