% top/XSG_core_config
top_XSG_core_config_type         = 'xps_xsg';
top_XSG_core_config_param        = '';

% top/adc/adc
top_adc_adc_type         = 'xps_adc5g';
top_adc_adc_param        = '';
top_adc_adc_ip_name      = 'adc5g_dmux1_interface';

% top/adc/enable
top_adc_enable_type         = 'xps_sw_reg';
top_adc_enable_param        = 'in';
top_adc_enable_ip_name      = 'opb_register_ppc2simulink';
top_adc_enable_addr_start   = hex2dec('01000000');
top_adc_enable_addr_end     = hex2dec('010000FF');

% top/data_bypass_topin
top_data_bypass_topin_type         = 'xps_data_bypass_topin';
top_data_bypass_topin_param        = '';
top_data_bypass_topin_ip_name      = 'data_bypass_topin';

% top/data_bypass_topout
top_data_bypass_topout_type         = 'xps_data_bypass_topout';
top_data_bypass_topout_param        = '';
top_data_bypass_topout_ip_name      = 'data_bypass_topout';

% top/ethout/dest_ip0
top_ethout_dest_ip0_type         = 'xps_sw_reg';
top_ethout_dest_ip0_param        = 'in';
top_ethout_dest_ip0_ip_name      = 'opb_register_ppc2simulink';
top_ethout_dest_ip0_addr_start   = hex2dec('01000100');
top_ethout_dest_ip0_addr_end     = hex2dec('010001FF');

% top/ethout/dest_ip1
top_ethout_dest_ip1_type         = 'xps_sw_reg';
top_ethout_dest_ip1_param        = 'in';
top_ethout_dest_ip1_ip_name      = 'opb_register_ppc2simulink';
top_ethout_dest_ip1_addr_start   = hex2dec('01000200');
top_ethout_dest_ip1_addr_end     = hex2dec('010002FF');

% top/ethout/dest_ip2
top_ethout_dest_ip2_type         = 'xps_sw_reg';
top_ethout_dest_ip2_param        = 'in';
top_ethout_dest_ip2_ip_name      = 'opb_register_ppc2simulink';
top_ethout_dest_ip2_addr_start   = hex2dec('01000300');
top_ethout_dest_ip2_addr_end     = hex2dec('010003FF');

% top/ethout/dest_ip3
top_ethout_dest_ip3_type         = 'xps_sw_reg';
top_ethout_dest_ip3_param        = 'in';
top_ethout_dest_ip3_ip_name      = 'opb_register_ppc2simulink';
top_ethout_dest_ip3_addr_start   = hex2dec('01000400');
top_ethout_dest_ip3_addr_end     = hex2dec('010004FF');

% top/ethout/dest_port0
top_ethout_dest_port0_type         = 'xps_sw_reg';
top_ethout_dest_port0_param        = 'in';
top_ethout_dest_port0_ip_name      = 'opb_register_ppc2simulink';
top_ethout_dest_port0_addr_start   = hex2dec('01000500');
top_ethout_dest_port0_addr_end     = hex2dec('010005FF');

% top/ethout/dest_port1
top_ethout_dest_port1_type         = 'xps_sw_reg';
top_ethout_dest_port1_param        = 'in';
top_ethout_dest_port1_ip_name      = 'opb_register_ppc2simulink';
top_ethout_dest_port1_addr_start   = hex2dec('01000600');
top_ethout_dest_port1_addr_end     = hex2dec('010006FF');

% top/ethout/dest_port2
top_ethout_dest_port2_type         = 'xps_sw_reg';
top_ethout_dest_port2_param        = 'in';
top_ethout_dest_port2_ip_name      = 'opb_register_ppc2simulink';
top_ethout_dest_port2_addr_start   = hex2dec('01000700');
top_ethout_dest_port2_addr_end     = hex2dec('010007FF');

% top/ethout/dest_port3
top_ethout_dest_port3_type         = 'xps_sw_reg';
top_ethout_dest_port3_param        = 'in';
top_ethout_dest_port3_ip_name      = 'opb_register_ppc2simulink';
top_ethout_dest_port3_addr_start   = hex2dec('01000800');
top_ethout_dest_port3_addr_end     = hex2dec('010008FF');

% top/ethout/rst
top_ethout_rst_type         = 'xps_sw_reg';
top_ethout_rst_param        = 'in';
top_ethout_rst_ip_name      = 'opb_register_ppc2simulink';
top_ethout_rst_addr_start   = hex2dec('01000900');
top_ethout_rst_addr_end     = hex2dec('010009FF');

% top/ethout/ten_Gbe0
top_ethout_ten_Gbe0_type         = 'xps_tengbe_v2';
top_ethout_ten_Gbe0_param        = '';
top_ethout_ten_Gbe0_ip_name      = 'kat_ten_gb_eth';
top_ethout_ten_Gbe0_addr_start   = hex2dec('01004000');
top_ethout_ten_Gbe0_addr_end     = hex2dec('01007FFF');

% top/ethout/ten_Gbe1
top_ethout_ten_Gbe1_type         = 'xps_tengbe_v2';
top_ethout_ten_Gbe1_param        = '';
top_ethout_ten_Gbe1_ip_name      = 'kat_ten_gb_eth';
top_ethout_ten_Gbe1_addr_start   = hex2dec('01008000');
top_ethout_ten_Gbe1_addr_end     = hex2dec('0100BFFF');

% top/ethout/ten_Gbe2
top_ethout_ten_Gbe2_type         = 'xps_tengbe_v2';
top_ethout_ten_Gbe2_param        = '';
top_ethout_ten_Gbe2_ip_name      = 'kat_ten_gb_eth';
top_ethout_ten_Gbe2_addr_start   = hex2dec('0100C000');
top_ethout_ten_Gbe2_addr_end     = hex2dec('0100FFFF');

% top/ethout/ten_Gbe3
top_ethout_ten_Gbe3_type         = 'xps_tengbe_v2';
top_ethout_ten_Gbe3_param        = '';
top_ethout_ten_Gbe3_ip_name      = 'kat_ten_gb_eth';
top_ethout_ten_Gbe3_addr_start   = hex2dec('01010000');
top_ethout_ten_Gbe3_addr_end     = hex2dec('01013FFF');

% top/scope_raw_0/snap/bram
top_scope_raw_0_snap_bram_type         = 'xps_bram';
top_scope_raw_0_snap_bram_param        = '4096';
top_scope_raw_0_snap_bram_ip_name      = 'bram_if';
top_scope_raw_0_snap_bram_addr_start   = hex2dec('01014000');
top_scope_raw_0_snap_bram_addr_end     = hex2dec('01017FFF');

% top/scope_raw_0/snap/ctrl
top_scope_raw_0_snap_ctrl_type         = 'xps_sw_reg';
top_scope_raw_0_snap_ctrl_param        = 'in';
top_scope_raw_0_snap_ctrl_ip_name      = 'opb_register_ppc2simulink';
top_scope_raw_0_snap_ctrl_addr_start   = hex2dec('01018000');
top_scope_raw_0_snap_ctrl_addr_end     = hex2dec('010180FF');

% top/scope_raw_0/snap/status
top_scope_raw_0_snap_status_type         = 'xps_sw_reg';
top_scope_raw_0_snap_status_param        = 'out';
top_scope_raw_0_snap_status_ip_name      = 'opb_register_simulink2ppc';
top_scope_raw_0_snap_status_addr_start   = hex2dec('01018100');
top_scope_raw_0_snap_status_addr_end     = hex2dec('010181FF');

